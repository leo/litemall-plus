package org.ysling.litemall.wx.web;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ysling.litemall.db.dtomain.GoodsAllinone;
import org.ysling.litemall.core.service.GoodsCoreService;
import org.ysling.litemall.core.utils.ResponseUtil;
import org.ysling.litemall.core.validator.Order;
import org.ysling.litemall.core.validator.Sort;
import org.ysling.litemall.wx.annotation.LoginUser;
import org.ysling.litemall.wx.service.WxBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.ysling.litemall.db.domain.*;
import org.ysling.litemall.db.service.*;

import javax.validation.constraints.NotNull;

/**
 * 店铺
 */
@RestController
@RequestMapping("/wx/brand")
@Validated
public class WxBrandController {
    private final Log logger = LogFactory.getLog(WxBrandController.class);

    @Autowired
    private LitemallBrandService brandService;
    @Autowired
    private WxBrandService wxBrandService;
    @Autowired
    private GoodsCoreService goodsCoreService;


    /**
     * 品牌列表
     *
     * @param page 分页页数
     * @param limit 分页大小
     * @return 品牌列表
     */
    @GetMapping("list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        return ResponseUtil.okList(brandService.query(page, limit, sort, order));
    }

    /**
     * 品牌详情
     *
     * @param brandId 品牌ID
     * @return 品牌详情
     */
    @GetMapping("detail")
    public Object brandDetail(@NotNull Integer brandId) {
        return wxBrandService.brandDetail(brandId);
    }


    /**
     * 添加或修改店铺
     * @param userId
     * @param brand
     * @return
     */
    @PostMapping("/save")
    public Object brandSave(@LoginUser Integer userId , @RequestBody LitemallBrand brand) {
        return wxBrandService.brandSave(userId, brand);
    }

    /**
     * 店铺订单列表
     *
     * @param page     分页页数
     * @param limit     分页大小
     * @return 订单列表
     */
    @GetMapping("/order")
    public Object orderList(@LoginUser Integer userId ,
                            @RequestParam(defaultValue = "0") Integer showType,
                            @RequestParam(defaultValue = "1") Integer page,
                            @RequestParam(defaultValue = "10") Integer limit,
                            @Sort @RequestParam(defaultValue = "update_time") String sort,
                            @Order @RequestParam(defaultValue = "desc") String order) {
        return wxBrandService.orderList(userId, showType, page, limit ,sort ,order);
    }


    /**
     * 分类列表
     * @return
     */
    @GetMapping("/goods/category")
    public Object catList() {
        return wxBrandService.catList();
    }

    /**
     * 店铺商品列表
     * @param page 分页页数
     * @param limit 分页大小
     * @return 店铺商品列表
     */
    @GetMapping("goods/list")
    public Object goodsList(Integer brandId, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        return wxBrandService.goodsList(brandId, page, limit);
    }

    /**
     * 编辑店铺商品
     *
     * NOTE：
     * 由于商品涉及到四个表，特别是litemall_goods_product表依赖litemall_goods_specification表，
     * 这导致允许所有字段都是可编辑会带来一些问题，因此这里商品编辑功能是受限制：
     * （1）litemall_goods表可以编辑字段；
     * （2）litemall_goods_specification表只能编辑pic_url字段，其他操作不支持；
     * （3）litemall_goods_product表只能编辑price, number和url字段，其他操作不支持；
     * （4）litemall_goods_attribute表支持编辑、添加和删除操作。
     *
     *
     * NOTE3:
     * （1）购物车缓存了一些商品信息，因此需要及时更新。
     * 目前这些字段是goods_sn, goods_name, price, pic_url。
     * （2）但是订单里面的商品信息则是不会更新。
     * 如果订单是未支付订单，此时仍然以旧的价格支付。
     */
    @PostMapping("/goods/update")
    public Object goodsUpdate(@RequestBody GoodsAllinone goodsAllinone) {
        return goodsCoreService.goodsUpdate(goodsAllinone);
    }

    /**
     * 删除店铺商品
     * @param goods
     * @return
     */
    @PostMapping("/goods/delete")
    public Object goodsDelete(@RequestBody LitemallGoods goods) {
        return goodsCoreService.goodsDelete(goods);
    }

    /**
     * 添加店铺商品
     * @param goodsAllinone
     * @return
     */
    @PostMapping("/goods/create")
    public Object goodsCreate(@RequestBody GoodsAllinone goodsAllinone) {
        return goodsCoreService.goodsCreate(goodsAllinone);
    }

    /**
     * 店铺商品详情
     * @param id
     * @return
     */
    @GetMapping("/goods/detail")
    public Object goodsDetail(@NotNull Integer id) {
        return wxBrandService.goodsDetail(id);
    }

}