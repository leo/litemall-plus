package org.ysling.litemall.wx.web;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.ysling.litemall.core.utils.JacksonUtil;
import org.ysling.litemall.core.utils.ResponseUtil;
import org.ysling.litemall.db.vomain.TimeLineVo;
import org.ysling.litemall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.ysling.litemall.db.domain.LitemallTimeline;
import org.ysling.litemall.db.domain.LitemallUser;
import org.ysling.litemall.db.service.LitemallTimelineService;
import org.ysling.litemall.db.service.LitemallUserService;

import java.util.*;

/**
 * 时间轴信息发布
 */
@RestController
@RequestMapping("/wx/timeline")
@Validated
public class WxTimelineController {

    private final Log logger = LogFactory.getLog(WxSearchController.class);

    @Autowired
    private LitemallTimelineService timelineService;
    @Autowired
    private LitemallUserService userService;

    /**
     * 发布列表
     *
     * @param page     分页页数
     * @param limit     分页大小
     * @return 订单列表
     */
    @GetMapping("timelinelist")
    public Object timelineList(@RequestParam(defaultValue = "1") Integer page,
                               @RequestParam(defaultValue = "10") Integer limit) {

        List<LitemallTimeline> timelineList = timelineService.queryList(page, limit);
        ArrayList<Object> timeLineVoList = new ArrayList<>();

        //根据日期将LitemallTimeline添加进Map的value
        for (LitemallTimeline timeline : timelineList) {
            //给查寻出来的时间加上浏览量
            timeline.setLookNumber(timeline.getLookNumber()+1);
            timelineService.updateById(timeline);

            LitemallUser user = userService.findById(timeline.getUserId());
            if (user == null) continue;

            TimeLineVo timeLineVo = new TimeLineVo();
            timeLineVo.setAddTime(timeline.getAddTime());
            timeLineVo.setContent(timeline.getContent());
            timeLineVo.setAdmin(timeline.getIsAdmin());
            timeLineVo.setLookNumber(timeline.getLookNumber());
            timeLineVo.setPicUrls(timeline.getPicUrls());
            timeLineVo.setThumbUp(timeline.getThumbUp());
            timeLineVo.setUserId(timeline.getUserId());
            timeLineVo.setId(timeline.getId());
            timeLineVo.setAvatar(user.getAvatar());
            timeLineVo.setNickname(user.getNickname());
            timeLineVoList.add(timeLineVo);
        }

        return ResponseUtil.okList(timeLineVoList,timelineList);
    }


    /**
     * 发布日常
     *
     * @param userId   用户ID
     * @param timeline 时间轴发布信息
     * @return 操作结果
     */
    @PostMapping("submit")
    public Object submit(@LoginUser Integer userId, @RequestBody LitemallTimeline timeline) {
        if (Objects.isNull(userId)) {
            return ResponseUtil.unlogin();
        }

        String content = timeline.getContent();
        if (Objects.isNull(content)) {
            return ResponseUtil.badArgument();
        }

        timeline.setId(null);
        timeline.setUserId(userId);
        timelineService.add(timeline);
        return ResponseUtil.ok();
    }

    /**
     * 删除日常
     *
     * @param userId   用户ID
     * @param body 时间轴发布信息的id
     * @return 操作结果
     */
    @PostMapping("delete")
    public Object submit(@LoginUser Integer userId, @RequestBody String body) {
        if (Objects.isNull(userId)) {
            return ResponseUtil.unlogin();
        }
        Integer timeVoId = JacksonUtil.parseInteger(body, "timeVoId");

        if (timeVoId == null){
            return ResponseUtil.badArgument();
        }
        LitemallTimeline timeline = timelineService.findById(timeVoId);
        if (timeline == null){
            return ResponseUtil.badArgument();
        }

        if (SecurityUtils.getSubject().getPrincipal() != null){
            timelineService.deleteById(timeVoId);
        }else if(userId.equals(timeline.getUserId())){
            timelineService.deleteById(timeVoId);
        }else {
            return ResponseUtil.fail(600,"无权限");
        }
        return ResponseUtil.ok();
    }


}
