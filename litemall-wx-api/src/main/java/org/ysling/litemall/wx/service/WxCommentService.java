package org.ysling.litemall.wx.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ysling.litemall.db.domain.LitemallComment;
import org.ysling.litemall.db.domain.LitemallUser;
import org.ysling.litemall.db.service.LitemallCommentService;
import org.ysling.litemall.db.service.LitemallUserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WxCommentService {

    @Autowired
    private LitemallUserService userService;
    @Autowired
    private LitemallCommentService commentService;

    /**
     * 获取评论信息
     * @param valueId 商品或专题id
     * @param type 0=商品 1=专题
     * @param offset 页数
     * @param limit  条数
     * @return
     */
    public Map<String, Object> getComments(Integer valueId , Byte type , int offset, int limit){
        // 评论
        List<LitemallComment> comments = commentService.queryCommentList(valueId, type, offset, limit);
        List<Map<String, Object>> commentsVo = new ArrayList<>(comments.size());
        int commentCount = commentService.count(type, valueId, 0);
        for (LitemallComment comment : comments) {
            Map<String, Object> c = new HashMap<>();
            c.put("goodId", comment.getId());
            c.put("addTime", comment.getAddTime());
            c.put("content", comment.getContent());
            c.put("adminContent", comment.getAdminContent());
            LitemallUser user = userService.findById(comment.getUserId());
            c.put("nickname", user == null ? "" : user.getNickname());
            c.put("avatar", user == null ? "" : user.getAvatar());
            c.put("picList", comment.getPicUrls());
            commentsVo.add(c);
        }
        Map<String, Object> commentList = new HashMap<>();
        commentList.put("count", commentCount);
        commentList.put("data", commentsVo);
        return commentList;
    }
}
