package org.ysling.litemall.wx.socket;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import org.ysling.litemall.db.domain.LitemallMessage;

import javax.websocket.Session;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * webSocket消息发送工具类
 */
public class WebSocketUtil {

    /**用于存所有的连接服务的客户端，这个对象存储是安全的*/
    public static Map<Integer, WebSocketService> socketMap = new ConcurrentHashMap<>();


    /**
     * 向指定用户发送信息提示
     * @param session 用户会话
     * @param content 提示信息
     */
    public static synchronized void sendHintTo(Session session, String content) {
        LitemallMessage message = new LitemallMessage();
        message.setContent(content);
        //将消息对象转为list然后转为json
        String string = JSONObject.toJSONString(Lists.newArrayList(message));
        session.getAsyncRemote().sendText(string);
    }

    /**
     * 向自己和指定用户发送信息
     * @param message 消息对象
     * @param sendUserId 发送用户
     * @param receiveUserId 接收用户
     */
    public static synchronized void sendMessageTo(LitemallMessage message ,Integer sendUserId, Integer receiveUserId) {
        //将消息对象转为list然后转为json
        String content = JSONObject.toJSONString(Lists.newArrayList(message));
        //给自己和接收方发送消息
        for (WebSocketService item : socketMap.values()) {
            if (item.userId.equals(sendUserId) ){
                item.session.getAsyncRemote().sendText(content);
            }
            if (item.userId.equals(receiveUserId) ){
                item.session.getAsyncRemote().sendText(content);
            }
        }
    }

    /**
     * 给所有用户发送信息
     * @param content 信息
     */
    public static synchronized void sendMessageAll(String content) {
        for (WebSocketService item : socketMap.values()) {
            item.session.getAsyncRemote().sendText(content);
        }
    }

}
