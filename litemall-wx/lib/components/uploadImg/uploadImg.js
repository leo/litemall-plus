// lib/components/uploadImg/uploadImg.js
var api = require('../../../config/api.js');
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    imgSite: { //图片张数
      type: Number, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
      value: 4 // 属性初始值（可选），如果未指定则会根据类型选择一个
    },
    pics: { //初始数据
      type: Array, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
      value: [],// 属性初始值（可选），如果未指定则会根据类型选择一个
      observer: function(newVal, oldVal) {
        // console.log("newVal",newVal);
        // console.log("oldVal",oldVal);
        if(newVal == null) return;
        this.setData({
          picUrls: newVal,
          files: newVal,
        })
      }
    },
    pic: { //初始数据
      type: String, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
      value: "" ,// 属性初始值（可选），如果未指定则会根据类型选择一个
      observer: function(newVal, oldVal) {
        let that = this;
        if(that.data.files.length > 0){
          that.data.files.splice(0, 1);
        }
        if(newVal != null && newVal != "") {
          that.data.files.push(newVal);
        }
        this.setData({
          files: that.data.files,
        })
      }
    },
    title: { //标题
      type: String, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
      value: "图片上传" // 属性初始值（可选），如果未指定则会根据类型选择一个
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    picUrls: [],
    files: [],
  },

  /**
   * 组件的方法列表
   */
  methods: {
    chooseImage: function(e) {
      var that = this;
      wx.chooseImage({
        count: 1,
        sizeType: ['original', 'compressed'],
        sourceType: ['album', 'camera'],
        success: function(res) {
          that.upload(res);
        }
      })
    },

    upload: function(res) {
      var that = this;
      wx.showLoading({
        title: '加载中',
      });
      const uploadTask = wx.uploadFile({
        url: api.StorageUpload,
        filePath: res.tempFilePaths[0],
        name: 'file',
        success: function(res) {
          var _res = JSON.parse(res.data);
          if (_res.errno === 0) {
            var url = _res.data.url
            that.data.picUrls.push(url);
            that.setData({
              picUrls: that.data.picUrls,
            })
            //将值返回给getImage，getImage为组件调用页的js方法
            that.triggerEvent('getImage',{
              picUrls: that.data.picUrls
            })
          }
        },
        fail: function(e) {
          wx.hideLoading();
          wx.showModal({
            title: '错误',
            content: '上传失败',
            showCancel: false
          })
        },
      })

      uploadTask.onProgressUpdate((res) => {
        console.log('上传进度', res.progress)
        console.log('已经上传的数据长度', res.totalBytesSent)
        console.log('预期需要上传的数据总长度', res.totalBytesExpectedToSend)
        if(res.progress == 100){
          wx.hideLoading();
        }
      })
    },

    previewImage: function(e) {
      wx.previewImage({
        current: e.currentTarget.id, // 当前显示图片的http链接
        urls: this.data.files // 需要预览的图片http链接列表
      })
    },
  
    DelImg(e) {
      let that = this;
      wx.showModal({
        title: '图片删除',
        content: '确定要删除这张图片吗？',
        cancelText: '再看看',
        confirmText: '再见',
        success: (res) => {
          if (res.confirm) {
            that.data.files.splice(e.currentTarget.dataset.index, 1);
            that.data.picUrls.splice(e.currentTarget.dataset.index, 1)
            that.setData({
              files: that.data.files,
              picUrls: that.data.picUrls
            })
            that.triggerEvent('getImage',{
              picUrls: that.data.picUrls
            })
          }
        },
      })
    },
  }
})
