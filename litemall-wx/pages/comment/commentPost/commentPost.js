// 上传组件 基于https://github.com/Tencent/weui-wxss/tree/master/src/example/uploader
var app = getApp();
var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
Page({
  data: {
    type: 0,//判断是专题判断还是商品评论
    valueId: 0,//待评论商品id或专题id
    topic: {},//专题详情
    orderGoods: {}, //订单商品详情
    content: '',
    stars: [0, 1, 2, 3, 4],
    star: 5,
    starText: '十分满意',
    hasPicture: false,
    picUrls: [],
    files: []
  },
  
  chooseImage: function(e) {
    if (this.data.files.length >= 4) {
      util.showErrorModal('只能上传四张图片')
      return false;
    }
    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {
        that.setData({
          files: that.data.files.concat(res.tempFilePaths)
        });
        that.upload(res);
      }
    })
  },

  upload: function(res) {
    var that = this;
    const uploadTask = wx.uploadFile({
      url: api.StorageUpload,
      filePath: res.tempFilePaths[0],
      name: 'file',
      success: function(res) {
        var data = JSON.parse(res.data);
        if (data.errno === 0) {
          var url = data.data.url
          that.data.picUrls.push(url)
          that.setData({
            hasPicture: true,
            picUrls: that.data.picUrls
          })
        }
      },
      fail: function(e) {
        wx.showModal({
          title: '错误',
          content: '上传失败',
          showCancel: false
        })
      },
    })

    uploadTask.onProgressUpdate((res) => {
      console.log('上传进度', res.progress)
      console.log('已经上传的数据长度', res.totalBytesSent)
      console.log('预期需要上传的数据总长度', res.totalBytesExpectedToSend)
    })

  },
  previewImage: function(e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: this.data.files // 需要预览的图片http链接列表
    })
  },

  DelImg(e) {
    let that = this;
    wx.showModal({
      title: '图片删除',
      content: '确定要删除这张图片吗？',
      cancelText: '再看看',
      confirmText: '再见',
      success: res => {
        if (res.confirm) {
          that.data.files.splice(e.currentTarget.dataset.index, 1);
          that.data.picUrls.splice(e.currentTarget.dataset.index, 1)
          that.setData({
            files: that.data.files,
            picUrls: that.data.picUrls
          })
        }
      }
    })
  },

  selectRater: function(e) {
    var star = e.currentTarget.dataset.star + 1;
    var starText;
    if (star == 1) {
      starText = '很差';
    } else if (star == 2) {
      starText = '不太满意';
    } else if (star == 3) {
      starText = '满意';
    } else if (star == 4) {
      starText = '比较满意';
    } else {
      starText = '十分满意'
    }
    this.setData({
      star: star,
      starText: starText
    })
  },

  onLoad: function(options) {
    var that = this;
    that.setData({
      type : options.type,
      valueId: options.valueId
    });
    if(options.type == 0){
      that.getOrderGoods();
    }else{
      that.getTopic();
    }
  },

  getOrderGoods: function() {
    let that = this;
    util.request(api.OrderGoods, {
      ogId: that.data.valueId
    }).then(function(res) {
      console.log(res);
      if (res.errno === 0) {
        that.setData({
          orderGoods: res.data,
        });
      }
    });
  },

  getTopic: function() {
    let that = this;
    util.request(api.TopicDetail, {
      id: that.data.valueId
    }).then(function(res) {
      console.log(res);
      if (res.errno === 0) {
        that.setData({
          topic: res.data.topic
        });
      }
    });
  },

  onClose: function() {
    wx.navigateBack();
  },

  onPost: function() {
    let that = this;
    if (!this.data.content) {
      util.showErrorModal('请填写评论')
      return false;
    }
    let requestUrl = ''; 
    if(that.data.type == 0){
      requestUrl = api.OrderComment;
    }else{
      requestUrl = api.TopicComment;
    }
    console.log(requestUrl)
    util.request(requestUrl, {
      valueId: that.data.valueId,
      content: that.data.content,
      star: that.data.star,
      hasPicture: that.data.hasPicture,
      picUrls: that.data.picUrls
    }, 'POST').then(function(res) {
      if (res.errno === 0) {
        wx.showToast({
          title: '评论成功',
          complete: function() {
            wx.navigateBack();
          }
        })
      }
    });
    
  },
  bindInputValue(event) {
    let value = event.detail.value;
    //判断是否超过140个字符
    if (value && value.length > 140) {
      return false;
    }
    this.setData({
      content: event.detail.value,
    })
  },
  onReady: function() {

  },
  onShow: function() {
    // 页面显示

  },
  onHide: function() {
    // 页面隐藏

  },
  onUnload: function() {
    // 页面关闭

  }
})