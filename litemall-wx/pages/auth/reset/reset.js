var api = require('../../../config/api.js');
var check = require('../../../utils/check.js');
var util = require('../../../utils/util.js');

var app = getApp();
Page({
  data: {
    mobile: '',
    code: '',
    password: '',
    confirmPassword: ''
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    // 页面渲染完成
  },
  onReady: function() {

  },
  onShow: function() {
    // 页面显示
  },
  onHide: function() {
    // 页面隐藏
  },
  onUnload: function() {
    // 页面关闭
  },

  /**
   * 发送验证码
   */
  sendCode: function() {
    let that = this;
    util.request(api.AuthMobileCaptcha, {
      mobile: that.data.mobile
    },'POST').then(function(res) {
      if (res.errno === 0) {
        util.showOkToast("发送成功");
      }
    });
  },

  startReset: function() {
    var that = this;
    if (this.data.mobile.length == 0 || this.data.code.length == 0) {
      util.showErrorModal('手机号和验证码不能为空');
      return false;
    }
    if (!check.isValidPhone(this.data.mobile)) {
      util.showErrorModal('手机号输入不正确');
      return false;
    }
    if (this.data.password.length < 6) {
      util.showErrorModal('密码不得少于6位');
      return false;
    }
    if (this.data.password != this.data.confirmPassword) {
      util.showErrorModal('确认密码不一致');
      return false;
    }
    util.request(api.AuthReset, {
      mobile: that.data.mobile,
      code: that.data.code,
      password: that.data.password
    },'POST').then(function(res) {
      if (res.errno === 0) {
        wx.navigateBack();
      }else{
        util.showErrorModal(res.errmsg)
      }
    });
  },

  bindPasswordInput: function(e) {
    this.setData({
      password: e.detail.value
    });
  },
  bindConfirmPasswordInput: function(e) {
    this.setData({
      confirmPassword: e.detail.value
    });
  },
  bindMobileInput: function(e) {
    this.setData({
      mobile: e.detail.value
    });
  },
  bindCodeInput: function(e) {
    this.setData({
      code: e.detail.value
    });
  },
  clearInput: function(e) {
    switch (e.currentTarget.id) {
      case 'clear-password':
        this.setData({
          password: ''
        });
        break;
      case 'clear-confirm-password':
        this.setData({
          confirmPassword: ''
        });
        break;
      case 'clear-mobile':
        this.setData({
          mobile: ''
        });
        break;
      case 'clear-code':
        this.setData({
          code: ''
        });
        break;
    }
  }
})