var api = require('../../../config/api.js');
var check = require('../../../utils/check.js');
var util = require('../../../utils/util.js');

var app = getApp();
Page({
  data: {
    username: '',
    password: '',
    confirmPassword: '',
    mobile: '',
    code: ''
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    // 页面渲染完成

  },
  onReady: function() {

  },
  onShow: function() {
    // 页面显示

  },
  onHide: function() {
    // 页面隐藏

  },
  onUnload: function() {
    // 页面关闭

  },

  /**
   * 发送验证码
   */
  sendCode: function() {
    let that = this;
    if (that.data.mobile.length == 0) {
      util.showErrorModal('手机号不能为空');
      return false;
    }
    if (!check.isValidPhone(that.data.mobile)) {
      util.showErrorModal('手机号输入不正确');
      return false;
    }
    util.request(api.AuthMobileCaptcha, {
      mobile: that.data.mobile
    },'POST').then(function(res) {
      if (res.errno === 0) {
        util.showOkToast("发送成功");
      }
    });
  },

  /**
   * 注册账号
   * @param {*} wxCode 
   */
  requestRegister: function(wxCode) {
    let that = this;
    util.request(api.AuthRegister, {
      username: that.data.username,
      password: that.data.password,
      mobile: that.data.mobile,
      code: that.data.code,
      wxCode: wxCode
    },'POST').then(function(res) {
      console.log(res)
      if (res.errno === 0) {
        app.globalData.hasLogin = true;
        wx.setStorageSync('userInfo', res.data.data.userInfo);
        wx.setStorage({
          key: "token",
          data: res.data.data.token,
          success: function() {
            wx.switchTab({
              url: '/pages/ucenter/index/index'
            });
          }
        });
      }else{
        util.showErrorModal(res.errmsg)
      }
    });
  },

  startRegister: function() {
    var that = this;
    if (this.data.password.length < 6 || this.data.username.length < 6) {
      util.showErrorModal('用户名和密码不得少于6位');
      return false;
    }

    if (this.data.password != this.data.confirmPassword) {
      util.showErrorModal('确认密码不一致');
      return false;
    }

    if (this.data.mobile.length == 0 || this.data.code.length == 0) {
      util.showErrorModal('手机号和验证码不能为空');
      return false;
    }

    if (!check.isValidPhone(this.data.mobile)) {
      util.showErrorModal('手机号输入不正确');
      return false;
    }

    wx.login({
      success: function(res) {
        if (!res.code) {
          util.showErrorModal('注册失败');
        }
        that.requestRegister(res.code);
      }
    });
  },
  bindUsernameInput: function(e) {
    this.setData({
      username: e.detail.value
    });
  },
  bindPasswordInput: function(e) {
    this.setData({
      password: e.detail.value
    });
  },
  bindConfirmPasswordInput: function(e) {
    this.setData({
      confirmPassword: e.detail.value
    });
  },
  bindMobileInput: function(e) {
    this.setData({
      mobile: e.detail.value
    });
  },
  bindCodeInput: function(e) {
    this.setData({
      code: e.detail.value
    });
  },
  clearInput: function(e) {
    switch (e.currentTarget.id) {
      case 'clear-username':
        this.setData({
          username: ''
        });
        break;
      case 'clear-password':
        this.setData({
          password: ''
        });
        break;
      case 'clear-confirm-password':
        this.setData({
          confirmPassword: ''
        });
        break;
      case 'clear-mobile':
        this.setData({
          mobile: ''
        });
        break;
      case 'clear-code':
        this.setData({
          code: ''
        });
        break;
    }
  }
})