var api = require('../../../config/api.js');
var util = require('../../../utils/util.js');
var user = require('../../../utils/user.js');

var app = getApp();
Page({
  data: {
    code: '',
    username: '',
    password: '',
    loginErrorCount: 0,
    canIUseGetUserProfile: false,
    iv : '',
    userInfo: {},
    encryptedData : '',
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  onReady: function() {

  },
  onShow: function() {
    // 页面显示
  },
  onHide: function() {
    // 页面隐藏
  },
  onUnload: function() {
    // 页面关闭
  },


  bindPhoneNumber: function(e) {
    if (e.detail.errMsg === "getPhoneNumber:ok") {
      this.setData({
        iv: e.detail.iv,
        encryptedData: e.detail.encryptedData
      })
    }
    if(this.data.userInfo.nickName){
      this.doLogin();
    }
  },

  wxLogin: function(e) {
    let that = this;
    if (this.data.canIUseGetUserProfile) {
      wx.getUserProfile({
        desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
        success: (res) => {
          that.setData({
            userInfo : res.userInfo,
          })
        },
        fail: () => {
          util.showErrorModal('微信登录失败');
        }
      })
    }else {
      that.setData({
        userInfo : e.detail.userInfo,
      });
    }
    if(that.data.iv && that.data.encryptedData){
      this.doLogin();
    }
  },

  //微信登陆
  doLogin: function() {
    let iv = this.data.iv;
    let userInfo = this.data.userInfo;
    let encryptedData = this.data.encryptedData;

    if(!iv || !userInfo.nickName || !encryptedData){
      app.globalData.hasLogin = false;
      util.showErrorToast('微信登录失败');
      return;
    }
    let that = this;
    user.checkLogin().catch(() => {
      user.loginByWeixin(userInfo ,encryptedData, iv).then(res => {
        app.globalData.hasLogin = true;
        that.accountLogin();
        return;
      }).catch((err) => {
        app.globalData.hasLogin = false;
        console.log(err)
      });
    });
  },

  //管理员登陆
  accountLogin: function() {
    var that = this;
    let code = that.data.code;
    let username = that.data.username;
    let password = that.data.password;
    
    if (password.length < 1 || username.length < 1) {
      util.showErrorModal('请输入用户名和密码');
      return false;
    }
    if (code.length < 1) {
      util.showErrorModal('请输入验证码');
      return false;
    }

    user.loginByAccount(username,password,code).then(res => {
      app.globalData.Administrator = true;
      wx.switchTab({
        url: '/pages/index/index',
      })
      return;
    }).catch((err) => {
      app.globalData.Administrator = false;
      util.showErrorModal('登陆失败');
    });
  },

  bindUsernameInput: function(e) {
    this.setData({
      username: e.detail.value
    });
  },

  bindPasswordInput: function(e) {
    this.setData({
      password: e.detail.value
    });
  },

  bindCodeInput: function(e) {
    this.setData({
      code: e.detail.value
    });
  },

  sendCode: function() {
    let that = this;
    if (this.data.password.length < 1 || this.data.username.length < 1) {
      util.showErrorModal('请输入用户名和密码');
      return false;
    }
    
    util.request(api.AuthMailCaptcha, {
      username: that.data.username
    },'POST').then(function(res) {
      if (res.errno === 0) {
        util.showOkToast("发送成功");
      }
    });
  },

  clearInput: function(e) {
    switch (e.currentTarget.id) {
      case 'clear-username':
        this.setData({
          username: ''
        });
        break;
      case 'clear-password':
        this.setData({
          password: ''
        });
        break;
      case 'clear-code':
        this.setData({
          code: ''
        });
        break;
    }
  }
})