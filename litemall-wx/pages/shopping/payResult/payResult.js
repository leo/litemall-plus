var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');

var app = getApp();
Page({
  data: {
    status: false,
    hideModal: true,
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    this.setData({
      status: options.status === '1' ? true : false,
    })
  },

  //打开或关闭拟态窗
  hideModal(e) {
    let that = this;
    that.setData({
      hideModal: !that.data.hideModal
    })
  },

  //查看订单
  payResultConfirm:function(){
    wx.switchTab({
      url: '/pages/ucenter/index/index',
    })
    this.hideModal();
  },

  //继续逛
  payResultCancel:function(){
    wx.switchTab({
      url: '/pages/index/index',
    })
    this.hideModal();
  },

  //去支付
  payResultPayOrder:function(){
    let that = this;
    //关闭拟态窗
    that.hideModal();
    //调用支付
    util.request(api.OrderPrepay, {
      orderIds: wx.getStorageSync('orderIds')
    }, 'POST').then(function(res) {
      if (res.errno === 0) {
        console.log("支付过程开始")
        wx.requestPayment({
          'timeStamp': res.data.timeStamp,
          'nonceStr': res.data.nonceStr,
          'package': res.data.packageValue,
          'signType': res.data.signType,
          'paySign': res.data.paySign,
          'success': function(res) {
            that.setData({
              status: true
            });
            console.log("支付过程成功");
          },
          'fail': function(res) {
            that.setData({
              status: false
            });
            console.log("支付过程失败");
          },
          'complete': function(res) {
            console.log("支付过程结束");
            that.hideModal();
          }
        });
      }else{
        console.log("支付调用失败");
      }
    });
  },

  onReady: function() {

  },
  onShow: function() {
    // 页面显示
  },
  onHide: function() {
    // 页面隐藏
  },
  onUnload: function() {
    // 页面关闭
  },
})