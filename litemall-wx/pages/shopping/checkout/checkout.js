const util = require('../../../utils/util.js');
const api = require('../../../config/api.js');

const app = getApp();

Page({
  data: {
    message: '',
    checkedGoodsList: [],
    checkedAddress: {},
    checkedCoupon: [],
    goodsTotalPrice: 0.00, //商品总价
    freightPrice: 0.00, //快递费
    couponPrice: 0.00, //优惠券的价格
    grouponPrice: 0.00, //团购优惠价格
    orderTotalPrice: 0.00, //订单总价
    integralPrice: 0.00,//积分优惠
    actualPrice: 0.00, //实际需要支付的总价
    cartId: 0,
    couponId: 0,
    addressId: 0,
    rewardLinkId: 0,
    userCouponId: 0,
    grouponLinkId: 0, //参与的团购，如果是发起则为0
    grouponRulesId: 0, //团购规则ID
    availableCouponLength: 0, // 可用的优惠券数量
  },


  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    
  },
  
  //获取checkou信息
  getCheckoutInfo: function() {
    let that = this;
    util.request(api.CartCheckout, {
      cartId: that.data.cartId,
      addressId: that.data.addressId,
      userCouponId: that.data.userCouponId,
      couponId: that.data.couponId,
      grouponRulesId: that.data.grouponRulesId
    }).then(function(res) {
      if (res.errno === 0) {
        console.log(res)
        that.setData({
          checkedGoodsList: res.data.checkedGoodsList,
          checkedAddress: res.data.checkedAddress,
          actualPrice: res.data.actualPrice,
          checkedCoupon: res.data.checkedCoupon,
          availableCouponLength: res.data.availableCouponLength,
          couponPrice: res.data.couponPrice,
          grouponPrice: res.data.grouponPrice,
          freightPrice: res.data.freightPrice,
          goodsTotalPrice: res.data.goodsTotalPrice,
          orderTotalPrice: res.data.orderTotalPrice,
          addressId: res.data.addressId,
          couponId: res.data.couponId,
          userCouponId: res.data.userCouponId,
          grouponRulesId: res.data.grouponRulesId,
          integralPrice: res.data.integralPrice,
        });
      }
      wx.hideLoading();
    });
  },
  selectAddress() {
    wx.navigateTo({
      url: '/pages/ucenter/address/address',
    })
  },
  selectCoupon() {
    wx.navigateTo({
      url: '/pages/ucenter/couponSelect/couponSelect',
    })
  },
  bindMessageInput: function(e) {
    this.setData({
      message: e.detail.value
    });
  },

  onReady: function() {
    // 页面渲染完成
  },

  onShow: function() {
    // 页面显示
    wx.showLoading({
      title: '加载中...',
    });
    try {
      var cartId = wx.getStorageSync('cartId');
      if (cartId === "") {
        cartId = 0;
      }
      var couponId = wx.getStorageSync('couponId');
      if (couponId === "") {
        couponId = 0;
      }
      var addressId = wx.getStorageSync('addressId');
      if (addressId === "") {
        addressId = 0;
      }
      var userCouponId = wx.getStorageSync('userCouponId');
      if (userCouponId === "") {
        userCouponId = 0;
      }
      var rewardLinkId = wx.getStorageSync('rewardLinkId');
      if (rewardLinkId === "") {
        rewardLinkId = 0;
      }
      var grouponLinkId = wx.getStorageSync('grouponLinkId');
      if (grouponLinkId === "") {
        grouponLinkId = 0;
      }
      var grouponRulesId = wx.getStorageSync('grouponRulesId');
      if (grouponRulesId === "") {
        grouponRulesId = 0;
      }
      
      this.setData({
        cartId: cartId,
        couponId: couponId,
        addressId: addressId,
        rewardLinkId: rewardLinkId,
        userCouponId: userCouponId,
        grouponLinkId: grouponLinkId,
        grouponRulesId: grouponRulesId,
      });
    } catch (e) {
      console.log(e);
    }
    this.getCheckoutInfo();
  },

  onHide: function() {
    // 页面隐藏
  },

  onUnload: function() {
    // 页面关闭
  },
  
  submitOrder: function() {
    var that = this;
    if(that.data.addressId <= 0){
      util.showErrorModal('请选择收货地址');
      return false;
    }
    //提交订单
    util.request(api.OrderSubmit, {
      cartId: that.data.cartId,
      message: that.data.message,
      couponId: that.data.couponId,
      addressId: that.data.addressId,
      userCouponId: that.data.userCouponId,
      rewardLinkId: that.data.rewardLinkId,
      grouponLinkId: that.data.grouponLinkId,
      grouponRulesId: that.data.grouponRulesId,
    }, 'POST').then(res => {
      if (res.errno === 0) {
        // 下单成功，重置couponId
        try {
          wx.setStorageSync('couponId', 0);
          wx.setStorageSync('rewardLinkId', 0);
          wx.setStorageSync('grouponLinkId', 0);
          wx.setStorageSync('grouponRulesId', 0);
        } catch (error) {
          console.log(error)
        }
        //判断是否需要调用微信支付
        if (res.data.isPay) {
          wx.redirectTo({
            url: '/pages/shopping/payResult/payResult?status=1'
          });
          return
        }
        //调用微信支付
        wx.setStorageSync('orderIds', res.data.orderIds);
        util.request(api.OrderPrepay, {
          orderIds: res.data.orderIds
        }, 'POST').then(function(res) {
          if (res.errno === 0) {
            console.log("支付过程开始");
            wx.requestPayment({
              'timeStamp': res.data.timeStamp,
              'nonceStr': res.data.nonceStr,
              'package': res.data.packageValue,
              'signType': res.data.signType,
              'paySign': res.data.paySign,
              'success': function(res) {
                console.log("支付过程成功");
                //发起发货消息订阅
                util.requestSubscribe(api.ShipTmplIds);
                wx.redirectTo({
                  url: '/pages/shopping/payResult/payResult?status=1'
                });
              },
              'fail': function(res) {
                console.log("支付过程失败");
                wx.redirectTo({
                  url: '/pages/shopping/payResult/payResult?status=0'
                });
              },
              'complete': function(res) {
                console.log("支付过程结束")
              }
            });
          } else {
            wx.redirectTo({
              url: '/pages/shopping/payResult/payResult?status=0'
            });
          }
        });
      }
    });
  },
});