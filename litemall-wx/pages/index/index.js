const util = require('../../utils/util.js');
const api = require('../../config/api.js');
const user = require('../../utils/user.js');
//获取应用实例
const app = getApp();

Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    hasLogin: false,
    Administrator: false,
    userInfo: {},
    banner : [],
    newGoods: [],
    hotGoods: [],
    allGoods: [],
    coupons: [],
    channel: [],
    elements: [
      { title: '店铺', name: 'brand', color: 'red', icon: 'shop',url:'/pages/brand/brandList/brandList'},
      { title: '专题', name: 'topics', color: 'orange', icon: 'selection',url:'/pages/topic/topicList/topicList' },
      { title: '团购', name: 'groupon', color: 'green', icon: 'friendadd',url:'/pages/grouponList/grouponList'},
      { title: '赏金', name: 'reward', color: 'olive', icon: 'sponsor' ,url:'/pages/rewards/rewards'},
    ],
    page: 1,
    limit: 20,
    totalPages: 1,
  },
  
  onShareAppMessage: function() {
    return {
      title: 'litemall-plus-wx',
      desc: '唯爱与生活不可辜负',
      path: '/pages/index/index'
    }
  },

  onPullDownRefresh() {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    this.setData({page : 1});
    this.getIndexData();
    //在当前页面显示导航条加载动画
    wx.hideNavigationBarLoading() //完成停止加载
    wx.stopPullDownRefresh() //停止下拉刷新
  },

  switchCate: function(event) {
    let id = event.currentTarget.dataset.id;
    util.navigate("/pages/category/category?id="+id)
  },

  getIndexData: function() {
    wx.showLoading({
      title: '加载中',
    });

    let that = this;
    this.setData({page : 1});
    util.request(api.IndexUrl).then(function(res) {
      wx.hideLoading();
      if (res.errno === 0) {
        that.setData({
          banner: res.data.banner,
          newGoods: res.data.newGoodsList,
          hotGoods: res.data.hotGoodsList,
          allGoods: res.data.allGoodsList.list,
          totalPages: res.data.allGoodsList.pages,
          coupons : res.data.couponList,
          channel: res.data.channel,
        });
      }
    });
  },

  /**分页获取商品 */
  getGoodsAll: function() {
    wx.showLoading({
      title: '加载中...',
    });
    let that = this;
    util.request(api.GoodsList, {
      page: that.data.page,
      limit: that.data.limit
    }).then(function(res) {
      wx.hideLoading();
      if (res.errno === 0) {
        that.setData({
          allGoods: that.data.allGoods.concat(res.data.list),
          totalPages: res.data.pages,
        });
      }
    });
  },

  //如果有可领优惠券则打开弹窗
  showCoupon(e){
    this.redPacket.openTheCouponPopUp();
  },

  /**
   * 页面初始化
   */
  onLoad: function(options) {
    var that = this;
    //轮播商品获取宽高度
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          windowWidth: res.windowWidth,
          windowHeight: res.windowHeight
        });
      }
    });
    let sceneId , sceneType;
    //页面初始化海报分享扫码
    if (options.scene) {
      var scene = decodeURIComponent(options.scene);
      let info_arr = scene.split(',');
      sceneType = info_arr[0];
      sceneId = info_arr[1];
    }
    // 页面初始化朋友圈分享
    if (options.goodId) {
      sceneId = options.goodId;
      sceneType = "goods";
    }
    if (options.grouponId) {
      sceneId = options.grouponId;
      sceneType = "groupon";
    }
    if (options.rewardId) {
      sceneId = options.rewardId;
      sceneType = "reward";
    }

    //判断首页初始化参数是否正常
    if (sceneType == 'goods' || sceneType == 'groupon' || sceneType == 'reward') {
      util.request(api.NavigateUrl,{
        sceneId : sceneId,
        sceneType : sceneType,
      },'POST').then(function(res) {
        if (res.errno === 0) {
          if (sceneType == 'goods') {
            util.navigate("/pages/goodsDetail/goodsDetail?goodId="+sceneId);
          }
          if (sceneType == 'groupon') {
            util.navigate("/pages/goodsDetail/goodsDetail?grouponId="+sceneId);
          }
          if (sceneType == 'reward') {
            util.navigate("/pages/goodsDetail/goodsDetail?rewardId="+sceneId);
          }
        }
      });
    }
    //加载优惠券组件
    this.redPacket = this.selectComponent("#redPacket");
    //加载首页数据
    this.getIndexData();
  },

   /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.totalPages > this.data.page) {
      this.setData({
        page: this.data.page + 1
      });
      this.getGoodsAll();
    } else {
      wx.showToast({
        title: '没有更多信息了',
        icon: 'none',
        duration: 2000
      });
      return false;
    }
  },

  onReady: function() {
    // 页面渲染完成
  },

  // 页面显示
  onShow: function() {
    //获取用户的登录信息
    if (app.globalData.hasLogin) {
      let userInfo = wx.getStorageSync('userInfo');
      this.setData({
        userInfo: userInfo,
        hasLogin: true
      });
      //管理员登录
      if (app.globalData.Administrator) {
        this.setData({
          Administrator: true
        });
      }
    }

    //自定义底部导航栏高亮显示不加会导致高亮随机跳
    if (typeof this.getTabBar === 'function' && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 0
      })
    }
  },
  onHide: function() {
    // 页面隐藏
  },
  onUnload: function() {
    // 页面关闭
  },

});