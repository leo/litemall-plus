const util = require('../../utils/util.js');
const api = require('../../config/api.js');
const user = require('../../utils/user.js');
//获取应用实例
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    hasLogin: false,
    Administrator: false,
    userInfo: {},
    ColorList: app.globalData.ColorList,
    modalName:'',
    timeVo: {},
    timelineVoList:[],//发布列表
    index:0,//外层循环下标
    vindex:0,//内层循环下标
    page: 1,
    limit: 10,
    totalPages: 1
  },

  //打开弹窗
  showModal(e) {
    this.setData({
      timeVo:e.currentTarget.dataset.timevo,
      index:e.currentTarget.dataset.index,
      vindex:e.currentTarget.dataset.vindex,
      modalName: 'menuModal',
    })
  },

  //关闭弹窗
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },

  previewImage: function(e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: [e.currentTarget.dataset.url] // 需要预览的图片http链接列表
    })
  },

  //编辑
  editTimeLine: function (e) {
    let that = this;
    util.showErrorModal('无权限');
    that.hideModal();
  },

  //删除
  deleteTimeLine: function (e) {
    let that = this;
    let timeVo = that.data.timeVo;
    util.request(api.TimeLineDelete, {
      timeVoId: timeVo.id,
    },'POST').then(function(res) {
      if (res.errno === 0) {
        wx.showToast({
          title: '删除成功',
          icon: 'success',
          duration: 2000//持续的时间
        })
        let index = that.data.index;
        let vindex = that.data.vindex;
        that.data.timelineVoList[index].splice(vindex,1);
        //渲染数据
        that.setData({
          timelineVoList:that.data.timelineVoList
        }); 
      }
    });
    that.hideModal();
  },

  //获取发布信息
  getTimeLineVoList(){
    let that = this;
    wx.showLoading({
      title: '加载中',
    });
    util.request(api.TimelineList, {
      page: that.data.page,
      limit: that.data.limit
    }).then(function(res) {
      if (res.errno === 0) {
        let f1 = that.data.timelineVoList;
        let f2 = res.data.list;
        for (let i = 0; i < f2.length; i++) {
          f2[i].addDate = f2[i].addTime.substring(0, 10);
          f2[i].time = f2[i].addTime.substring(11);
          let last = f1.length - 1;
          if (last >= 0 && f1[last][0].addDate === f2[i].addDate) {
            f1[last].push(f2[i]);
          } else {
            let tmp = [];
            tmp.push(f2[i])
            f1.push(tmp);
          }
        }
        console.log(f1);
        that.setData({
          timelineVoList: f1,
          totalPages: res.data.pages
        });
      }
      wx.hideLoading()
    });
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    that.getTimeLineVoList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    //获取用户的登录信息
    if (app.globalData.hasLogin) {
      let userInfo = wx.getStorageSync('userInfo');
      this.setData({
        userInfo: userInfo,
        hasLogin: true
      });
      //管理员登录
      if (app.globalData.Administrator) {
        this.setData({
          Administrator: true
        });
      }
    }
    //自定义底部导航栏高亮显示不加会导致高亮随机跳
    if (typeof this.getTabBar === 'function' && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 1
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    let that = this;
    that.setData({
      page: 1,
      timelineVoList : [],
    });
    that.getTimeLineVoList();
    wx.hideNavigationBarLoading() //完成停止加载
    wx.stopPullDownRefresh() //停止下拉刷新
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this;
    //获取发布信息
    if (that.data.totalPages > that.data.page) {
      that.setData({
        page: that.data.page + 1
      });
      that.getTimeLineVoList();
    } else {
      wx.showToast({
        title: '没有更多信息了',
        icon: 'none',
        duration: 2000
      });
      return false;
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
})