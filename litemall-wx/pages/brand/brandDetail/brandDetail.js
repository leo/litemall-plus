var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');

var app = getApp();

Page({
  data: {
    status: false,
    brandId: 0,
    brand: {},
    brandUser : {},
    goodsList: [],
    page: 1,
    limit: 10,
    totalPages: 1,
  },

  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    var that = this;
    that.setData({
      brandId: parseInt(options.id),
      status: options.status === '1' || app.globalData.Administrator ? true : false,
    });
    that.getBrandDetail();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    this.setData({
      page: 1 ,
      goodsList: []
    })
    this.getGoodsList();
    wx.hideNavigationBarLoading() //完成停止加载
    wx.stopPullDownRefresh() //停止下拉刷新
  },

  /**
   * 跳转到发布商品
   */
  issueGoods(){
    let that = this;
    let brand = that.data.brand;
    util.navigate("/pages/issue/issueGoods/issueGoods?brandId="+brand.id+"&brandName="+brand.name);
  },

  /**
   * 跳转订单详情
   */
  getBrandOrder(){
    util.navigate("/pages/brand/brandOrder/brandOrder");
  },

  getBrandDetail: function() {
    let that = this;
    util.request(api.BrandDetail, {
      brandId: that.data.brandId
    }).then(function(res) {
      if (res.errno === 0) {
        that.setData({
          brand: res.data.brand,
          brandUser : res.data.brandUser
        });
        that.getGoodsList();
      }
    });
  },

  getGoodsList() {
    var that = this;
    let getGoodsurl = api.GoodsList;
    if(that.data.status){
      getGoodsurl = api.BrandGoodsList;
    }
    util.request(getGoodsurl, {
      brandId: that.data.brandId,
      page: that.data.page,
      limit: that.data.limit
    }).then(function(res) {
      console.log(res)
      if (res.errno === 0) {
        that.setData({
          goodsList: that.data.goodsList.concat(res.data.list),
          totalPages: res.data.pages
        });
      }
    });
  },

  onReady: function() {
    // 页面渲染完成

  },
  onShow: function() {
    // 页面显示

  },
  onHide: function() {
    // 页面隐藏

  },
  onUnload: function() {
    // 页面关闭

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.totalPages > this.data.page) {
      this.setData({
        page: this.data.page + 1
      });
      this.getGoodsList();
    } else {
      wx.showToast({
        title: '没有更多信息了',
        icon: 'none',
        duration: 2000
      });
      return false;
    }
  },
})