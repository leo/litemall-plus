// pages/myBrand/myBrand.js
const util = require('../../../utils/util.js');
const api = require('../../../config/api.js');
const user = require('../../../utils/user.js');
//获取应用实例
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isPhone: false,
    brandId: 0,
    brand: {},
    picUrls: []
  },

  chooseImage(e){
    this.setData({
      picUrls: e.detail.picUrls
    })
  },

  bindPhoneNumber: function(e) {
    let that = this;
    // 拒绝授权
    if (e.detail.errMsg !== "getPhoneNumber:ok") {
      util.showErrorModal('保存店铺失败');
      return;
    }

    util.request(api.AuthBindPhone, {
      iv: e.detail.iv,
      encryptedData: e.detail.encryptedData
    }, 'POST').then(function(res) {
      if (res.errno === 0) {
        wx.showToast({
          title: '绑定手机号码成功',
          icon: 'success',
          duration: 2000
        });
        that.setData({
          isPhone: true,
        })
      }
    });
  },

  submitBrand: function(e) {
    let that = this;
    if (!app.globalData.hasLogin) {
      util.navigate("/pages/auth/login/login");
      return;
    }

    let brand = that.data.brand;
    brand.name = e.detail.value.brandName;
    brand.desc = e.detail.value.brandDesc;
    brand.mail = e.detail.value.brandMail;
    brand.userName = e.detail.value.userName;
    
    brand.floorPrice = parseFloat(e.detail.value.brandPrice);
    brand.picUrl = that.data.picUrls[0];

    console.log(brand)

    if(!brand.name || !brand.desc || !brand.mail || !brand.floorPrice || !brand.picUrl || !brand.userName){
      util.showErrorModal("参数错误");
      return;
    }
    
    wx.showModal({
      title: '保存店铺',
      content: '确定要保存吗？',
      cancelText: '再看看',
      confirmText: '保存',
      success: res => {
        if (res.confirm) {
          util.request(api.BrandSave, {
            id: brand.id,
            name : brand.name,
            desc : brand.desc,
            mail : brand.mail,
            userId : brand.userId,
            picUrl : brand.picUrl,
            userName: brand.userName,
            floorPrice : brand.floorPrice,
            version : brand.version,
          }, 'POST').then(function(res) {
            if (res.errno === 0) {
              wx.showToast({
                title: '保存成功！',
                icon: 'success',
                duration: 2000,
                complete: function() {
                  util.redirect("/pages/brand/brandDetail/brandDetail?id="+res.data.id+"&status=1")
                }
              });
            }
          });
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 页面初始化 options为页面跳转所带来的参数
    var that = this;
    if (options.id) {
      that.setData({
        brandId: parseInt(options.id)
      });
      that.getBrandDetail();
    }
  },

  getBrandDetail: function() {
    let that = this;
    util.request(api.BrandDetail, {
      brandId: that.data.brandId
    }).then(function(res) {
      if (res.errno === 0) {
        that.data.picUrls.push(res.data.brand.picUrl)
        that.setData({
          isPhone : true,
          brand: res.data.brand,
          picUrls: that.data.picUrls,
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})