var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');

var app = getApp();

Page({
  data: {
    tableName:['未使用','已使用','已过期'],
    couponList: [],
    code: '',
    status: 0,
    page: 1,
    limit: 10,
    totalPages: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getCouponList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    this.setData({
      page : 1,
      couponList: [],
    })
    this.getCouponList();
    wx.hideNavigationBarLoading() //完成停止加载
    wx.stopPullDownRefresh() //停止下拉刷新
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    if (this.data.totalPages > this.data.page) {
      this.setData({
        page: this.data.page + 1
      });
      this.getCouponList();
    } else {
      wx.showToast({
        title: '没有更多信息了',
        icon: 'none',
        duration: 2000
      });
      return false;
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  getCouponList: function() {
    let that = this;
    wx.showLoading({
      title: '加载中...',
    });
    util.request(api.CouponMyList, {
      status: that.data.status,
      page: that.data.page,
      limit: that.data.limit
    }).then(function(res) {
      console.log(res)
      if (res.errno === 0) {
        that.setData({
          couponList: that.data.couponList.concat(res.data.list),
          totalPages: res.data.pages,
        });
      }
      wx.hideLoading();
    });
  },

  bindExchange: function (e) {
    this.setData({
      code: e.detail.value
    });
  },
  clearExchange: function () {
    this.setData({
      code: ''
    });
  },
  goExchange: function() {
    if (this.data.code.length === 0) {
      util.showErrorModal("请输入兑换码");
      return;
    }
    let that = this;
    util.request(api.CouponExchange, {
      code: that.data.code
    }, 'POST').then(function (res) {
      if (res.errno === 0) {
        this.setData({
          page : 1,
          couponList: [],
        })
        that.getCouponList();
        that.clearExchange();
        wx.showToast({
          title: "领取成功",
          duration: 2000
        })
      } 
    });
  },

  switchTab: function(e) {
    this.setData({
      status: e.currentTarget.dataset.index,
      couponList: [],
      page: 1,
      limit: 10,
      totalPages: 1
    });
    this.getCouponList();
  },

})