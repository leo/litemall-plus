const util = require('../../../utils/util.js');
const api = require('../../../config/api.js');

Page({
  data: {
    showType: 0,
    tableName:['全部','待付款','待发货','待收货','待评价'],
    orderList: [],
    page: 1,
    limit: 10,
    totalPages: 1,
  },


  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    let that = this
    if (options.tab) {
      this.setData({
        showType: parseInt(options.tab)
      });
    }else{
      try {
        var tab = wx.getStorageSync('tab');
        that.setData({
          showType: tab
        });
      } catch (e) {}
    }
  },

  onPullDownRefresh() {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    this.setData({
      page: 1 ,
      orderList: []
    })
    this.getOrderList();
    wx.hideNavigationBarLoading() //完成停止加载
    wx.stopPullDownRefresh() //停止下拉刷新
  },

  getOrderList() {
    let that = this;
    // 防多点击，这个不用配合 wx.hideLoading();
    wx.showLoading({
      title: '加载中...',
    });

    util.request(api.OrderList, {
      showType: that.data.showType,
      page: that.data.page,
      limit: that.data.limit
    }).then(function(res) {
      if (res.errno === 0) {
        console.log(res)
        that.setData({
          orderList: that.data.orderList.concat(res.data.list),
          totalPages: res.data.pages
        });

        wx.hideLoading();
      }
    });
  },

  onReachBottom() {
    if (this.data.totalPages > this.data.page) {
      this.setData({
        page: this.data.page + 1
      });
      this.getOrderList();
    } else {
      wx.showToast({
        title: '没有更多订单了',
        icon: 'none',
        duration: 2000
      });
      return false;
    }
  },

  switchTab: function(e) {
    let showType = e.currentTarget.dataset.index;
    this.setData({
      orderList: [],
      showType: showType,
      page: 1,
      limit: 10,
      totalPages: 1
    });
    this.getOrderList();
  },

  orderToDetail:function(e){
    let order = e.currentTarget.dataset.order;
    util.navigate("/pages/ucenter/orderDetail/orderDetail?orderId="+order.id);
  },
  
  onReady: function() {
    // 页面渲染完成
  },
  onShow: function() {
    // 页面显示
    this.setData({
      orderList: [],
      page: 1,
      limit: 10,
      totalPages: 1
    })
    this.getOrderList();
  },
  onHide: function() {
    // 页面隐藏
  },
  onUnload: function() {
    // 页面关闭 
    wx.navigateBack({
      delta: 1
    })
  }
})