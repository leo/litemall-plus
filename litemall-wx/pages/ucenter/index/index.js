var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
var user = require('../../../utils/user.js');
var app = getApp();

let touchDotX = 0;//X按下时坐标
let touchDotY = 0;//y按下时坐标
let interval;//计时器
let time = 0;//从按下到松开共多少时间*100

Page({
  data: {
    hasLogin: false,
    Administrator: false,
    integralPrice: 0,
    userInfo: {},
    brand:{},
    order: {
      unpaid: 0,
      unship: 0,
      unrecv: 0,
      uncomment: 0
    },
  },

  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
  },
  onReady: function() {

  },
  
  onShow: function() {
    //获取用户的登录信息
    if (app.globalData.hasLogin) {
      let userInfo = wx.getStorageSync('userInfo');
      this.setData({
        userInfo: userInfo,
        hasLogin: true
      });
      //管理员登录
      if (app.globalData.Administrator) {
        this.setData({
          Administrator: true
        });
      }
      let that = this;
      util.request(api.UserIndex).then(function(res) {
        if (res.errno === 0) {
          that.setData({
            order: res.data.order,
            brand: res.data.brand,
            integralPrice: res.data.integralPrice
          });
        }
      });
    }

    //自定义底部导航栏高亮显示不加会导致高亮随机跳
    if (typeof this.getTabBar === 'function' && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 4
      })
    }
  },
  onHide: function() {
    // 页面隐藏

  },
  onUnload: function() {
    // 页面关闭
  },

  goBrand(){
    let that = this;
    let brand = that.data.brand;

    if(!that.data.hasLogin){
      util.navigate("/pages/auth/login/login");
      return;
    }

    if(!brand){
      util.navigate("/pages/brand/brandAdd/brandAdd");
      return;
    }

    util.navigate("/pages/brand/brandDetail/brandDetail?id="+brand.id+"&status=1");
  },
  
  goLogin() {
    if (!this.data.hasLogin) {
      util.navigate("/pages/auth/login/login");
    }
  },

  goOrder() {
    if (this.data.hasLogin) {
      util.navigate("/pages/ucenter/order/order?tab=0");
    } else {
      util.navigate("/pages/auth/login/login");
    }
  },
  goOrderIndex(e) {
    if (this.data.hasLogin) {
      let tab = e.currentTarget.dataset.index
      let route = e.currentTarget.dataset.route
      util.navigate(route+'?tab='+tab);
    } else {
      util.navigate("/pages/auth/login/login");
    };
  },

  goDaily() {
    util.navigate("/pages/issue/issueDaily/issueDaily");
  },
  goCoupon() {
    util.navigate("/pages/ucenter/couponList/couponList");
  },
  goCollect() {
    util.navigate("/pages/ucenter/collect/collect");
  },
  goFeedback(e) {
    util.navigate("/pages/ucenter/feedback/feedback");
  },
  goFootprint() {
    util.navigate("/pages/ucenter/footprint/footprint");
  },
  goAddress() {
    util.navigate("/pages/ucenter/address/address");
  },
  goAfterSale: function() {
    util.navigate("/pages/ucenter/aftersaleList/aftersaleList");
  },

  aboutUs: function() {
    util.navigate("/pages/ucenter/about/about");
  },
  goHelp: function () {
    util.navigate("/pages/ucenter/help/help");
  },  

  bindPhoneNumber: function(e) {
    if (e.detail.errMsg !== "getPhoneNumber:ok") {
      // 拒绝授权
      return;
    }
    if (!this.data.hasLogin) {
      wx.showToast({
        title: '绑定失败：请先登录',
        icon: 'none',
        duration: 2000
      });
      return;
    }
    util.request(api.AuthBindPhone, {
      iv: e.detail.iv,
      encryptedData: e.detail.encryptedData
    }, 'POST').then(function(res) {
      if (res.errno === 0) {
        wx.showToast({
          title: '绑定手机号码成功',
          icon: 'success',
          duration: 2000
        });
      }
    });
  },
  
  exitLogin: function() {
    wx.showModal({
      title: '退出登录',
      confirmColor: '#b4282d',
      content: '确认退出登录？',
      success: function(res) {
        if (!res.confirm) {
          return;
        }
        util.request(api.AuthLogout, {}, 'POST');
        app.globalData.hasLogin = false;
        app.globalData.Administrator = false;
        //清除用户数据
        wx.removeStorageSync('userToken');
        wx.removeStorageSync('userInfo');
        //清除管理员数据
        wx.removeStorageSync('adminInfo');
        wx.removeStorageSync('adminToken');
        wx.reLaunch({
          url: '/pages/index/index'
        });
      }
    })
  },

  // 触摸开始事件，滑动打开关闭抽屉
  touchStart: function (e) {
    touchDotX = e.touches[0].pageX; // 获取触摸时的原点
    touchDotY = e.touches[0].pageY;
    // 使用js计时器记录时间    
    interval = setInterval(function () {
        time++;
    }, 100);
  },
  // 触摸结束事件，滑动打开关闭抽屉
  touchEnd: function (e) {
    let touchMoveX = e.changedTouches[0].pageX;
    let touchMoveY = e.changedTouches[0].pageY;
    let tmX = touchMoveX - touchDotX;
    let tmY = touchMoveY - touchDotY;
    if (time < 20 && (tmX > 200 || tmX < -50)) {
        let absX = Math.abs(tmX);
        let absY = Math.abs(tmY);
        if (absX > 2 * absY) {
          if (tmX < 0) {
            this.setData({
              modalName: null
            })
          } else {
            this.setData({
              modalName: "viewModal"
            })
          }
        }
    }
    clearInterval(interval); // 清除setInterval
    time = 0;
  },


  //打开抽屉
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },

  //关闭抽屉
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },

  goLogin() {
    if (!this.data.hasLogin) {
      wx.navigateTo({
        url: "/pages/auth/login/login"
      });
    }
  },

})