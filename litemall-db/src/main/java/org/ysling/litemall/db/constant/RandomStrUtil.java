package org.ysling.litemall.db.constant;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * 随机生成字符串工具类
 *
 * @author ysling
 * @date 2022/08/23 15:33
 **/
public class RandomStrUtil {
    /**
     * 随机产生类型枚举
     */
    public enum TYPE {
        /**
         * 小字符型
         */
        LETTER,
        /**
         * 大写字符型
         */
        CAPITAL,
        /**
         * 数字型
         */
        NUMBER,
        /**
         * 大+小字符 型
         */
        LETTER_CAPITAL,
        /**
         * 小字符+数字 型
         */
        LETTER_NUMBER,
        /**
         * 大写字符+数字
         */
        CAPITAL_NUMBER,
        /**
         * 大+小字符+数字 型
         */
        LETTER_CAPITAL_NUMBER,
    }

    private static final String[] LOWERCASE = {
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
            "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

    private static final String[] CAPITAL = {
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
            "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    private static final String[] NUMBER = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"};

    /**
     * 静态随机数
     */
    private static final SecureRandom RANDOM = new SecureRandom();

    /**
     * 获取随机组合码
     *
     * @param size  位数
     * @param type 类型
     */
    public static String getRandom(int size, TYPE type) {
        ArrayList<String> temp = new ArrayList<String>();
        StringBuilder code = new StringBuilder();
        switch (type) {
            case LETTER:
                temp.addAll(Arrays.asList(LOWERCASE));
                break;
            case CAPITAL:
                temp.addAll(Arrays.asList(CAPITAL));
                break;
            case NUMBER:
                temp.addAll(Arrays.asList(NUMBER));
                break;
            case LETTER_CAPITAL:
                temp.addAll(Arrays.asList(LOWERCASE));
                temp.addAll(Arrays.asList(CAPITAL));
                break;
            case LETTER_NUMBER:
                temp.addAll(Arrays.asList(LOWERCASE));
                temp.addAll(Arrays.asList(NUMBER));
                break;
            case CAPITAL_NUMBER:
                temp.addAll(Arrays.asList(CAPITAL));
                temp.addAll(Arrays.asList(NUMBER));
                break;
            case LETTER_CAPITAL_NUMBER:
                temp.addAll(Arrays.asList(LOWERCASE));
                temp.addAll(Arrays.asList(CAPITAL));
                temp.addAll(Arrays.asList(NUMBER));
                break;
            default:
        }
        for (int i = 0; i < size; i++) {
            code.append(temp.get(RANDOM.nextInt(temp.size())));
        }
        return code.toString();
    }
}

