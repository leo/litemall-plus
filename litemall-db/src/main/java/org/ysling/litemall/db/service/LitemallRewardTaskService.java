package org.ysling.litemall.db.service;

import com.github.pagehelper.PageHelper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.ysling.litemall.db.dao.LitemallRewardTaskMapper;
import org.ysling.litemall.db.domain.LitemallOrder;
import org.ysling.litemall.db.domain.LitemallRewardTask;
import org.ysling.litemall.db.example.LitemallRewardTaskExample;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
@CacheConfig(cacheNames = "rewardTask")
public class LitemallRewardTaskService {
    
    @Resource
    private LitemallRewardTaskMapper rewardTaskMapper;
    
    @CacheEvict(allEntries = true)
    public int createRewardTask(LitemallRewardTask rewardTask) {
        rewardTask.setAddTime(LocalDateTime.now());
        rewardTask.setUpdateTime(LocalDateTime.now());
        return rewardTaskMapper.insertSelective(rewardTask);
    }

    @Cacheable(sync = true)
    public LitemallRewardTask findById(Integer id) {
        return rewardTaskMapper.selectByPrimaryKeyWithLogicalDelete(id,false);
    }

    @Cacheable(sync = true)
    public LitemallRewardTask findByGid(Integer goodsId) {
        LitemallRewardTaskExample example = new LitemallRewardTaskExample();
        example.or().andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        return rewardTaskMapper.selectOneByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallRewardTask> queryByGoodsId(Integer goodsId) {
        LitemallRewardTaskExample example = new LitemallRewardTaskExample();
        example.or().andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        return rewardTaskMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public int countByGoodsId(Integer goodsId) {
        LitemallRewardTaskExample example = new LitemallRewardTaskExample();
        example.or().andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        return (int)rewardTaskMapper.countByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallRewardTask> queryList(Integer page, Integer limit, String sort, String order) {
        LitemallRewardTaskExample example = new LitemallRewardTaskExample();
        example.or().andDeletedEqualTo(false);
        example.setOrderByClause(sort + " " + order);
        PageHelper.startPage(page, limit);
        return rewardTaskMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallRewardTask> querySelective(String goodsId, Integer page, Integer size, String sort, String order) {
        LitemallRewardTaskExample example = new LitemallRewardTaskExample();
        example.setOrderByClause(sort + " " + order);

        LitemallRewardTaskExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);

        if (StringUtils.hasText(goodsId)) {
            criteria.andGoodsIdEqualTo(Integer.parseInt(goodsId));
        }

        PageHelper.startPage(page, size);
        return rewardTaskMapper.selectByExample(example);
    }

    @CacheEvict(allEntries = true)
    public void delete(Integer id) {
        rewardTaskMapper.logicalDeleteByPrimaryKey(id);
    }

    @CacheEvict(allEntries = true)
    public void deleteByGid(Integer gid) {
        LitemallRewardTaskExample example = new LitemallRewardTaskExample();
        example.or().andGoodsIdEqualTo(gid);
        rewardTaskMapper.logicalDeleteByExample(example);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallRewardTask rewardTask) {
        rewardTask.setUpdateTime(LocalDateTime.now());
        return rewardTaskMapper.updateWithVersionByPrimaryKeySelective(rewardTask.getVersion(), rewardTask);
    }
}
