package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallRegion;
import org.ysling.litemall.db.example.LitemallRegionExample;

@Mapper
@Repository
public interface LitemallRegionMapper {
    /**
     * 行政区域表
     */
    long countByExample(LitemallRegionExample example);

    /**
     * 行政区域表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallRegionExample example);

    /**
     * 行政区域表
     */
    int deleteByExample(LitemallRegionExample example);

    /**
     * 行政区域表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 行政区域表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 行政区域表
     */
    int insert(LitemallRegion record);

    /**
     * 行政区域表
     */
    int insertSelective(LitemallRegion record);

    /**
     * 行政区域表
     */
    LitemallRegion selectOneByExample(LitemallRegionExample example);

    /**
     * 行政区域表
     */
    LitemallRegion selectOneByExampleSelective(@Param("example") LitemallRegionExample example, @Param("selective") LitemallRegion.Column ... selective);

    /**
     * 行政区域表
     */
    List<LitemallRegion> selectByExampleSelective(@Param("example") LitemallRegionExample example, @Param("selective") LitemallRegion.Column ... selective);

    /**
     * 行政区域表
     */
    List<LitemallRegion> selectByExample(LitemallRegionExample example);

    /**
     * 行政区域表
     */
    LitemallRegion selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallRegion.Column ... selective);

    /**
     * 行政区域表
     */
    LitemallRegion selectByPrimaryKey(Integer id);

    /**
     * 行政区域表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallRegion record, @Param("example") LitemallRegionExample example);

    /**
     * 行政区域表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallRegion record, @Param("example") LitemallRegionExample example);

    /**
     * 行政区域表
     */
    int updateByExampleSelective(@Param("record") LitemallRegion record, @Param("example") LitemallRegionExample example);

    /**
     * 行政区域表
     */
    int updateByExample(@Param("record") LitemallRegion record, @Param("example") LitemallRegionExample example);

    /**
     * 行政区域表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallRegion record);

    /**
     * 行政区域表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallRegion record);

    /**
     * 行政区域表
     */
    int updateByPrimaryKeySelective(LitemallRegion record);

    /**
     * 行政区域表
     */
    int updateByPrimaryKey(LitemallRegion record);
}