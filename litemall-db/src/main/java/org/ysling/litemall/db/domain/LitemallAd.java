package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_ad
 * Database Table Remarks : 
 *   广告表
 * @author ysling
 */
public class LitemallAd implements Serializable {
    /**
     * litemall_ad
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_ad
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 广告表ID
     */
    private Integer id;

    /**
     * 广告标题
     */
    private String name;

    /**
     * 所广告的商品页面或者活动页面链接地址
     */
    private String link;

    /**
     * 广告宣传图片
     */
    private String url;

    /**
     * 广告位置：1则是首页
     */
    private Byte position;

    /**
     * 活动内容
     */
    private String content;

    /**
     * 广告开始时间
     */
    private LocalDateTime startTime;

    /**
     * 广告结束时间
     */
    private LocalDateTime endTime;

    /**
     * 是否启动
     */
    private Boolean enabled;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_ad
     */
    private static final long serialVersionUID = 1L;

    /**
     * 广告表ID
     * @return id 广告表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 广告表ID
     * @param id 广告表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 广告标题
     * @return name 广告标题
     */
    public String getName() {
        return name;
    }

    /**
     * 广告标题
     * @param name 广告标题
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 所广告的商品页面或者活动页面链接地址
     * @return link 所广告的商品页面或者活动页面链接地址
     */
    public String getLink() {
        return link;
    }

    /**
     * 所广告的商品页面或者活动页面链接地址
     * @param link 所广告的商品页面或者活动页面链接地址
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * 广告宣传图片
     * @return url 广告宣传图片
     */
    public String getUrl() {
        return url;
    }

    /**
     * 广告宣传图片
     * @param url 广告宣传图片
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 广告位置：1则是首页
     * @return position 广告位置：1则是首页
     */
    public Byte getPosition() {
        return position;
    }

    /**
     * 广告位置：1则是首页
     * @param position 广告位置：1则是首页
     */
    public void setPosition(Byte position) {
        this.position = position;
    }

    /**
     * 活动内容
     * @return content 活动内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 活动内容
     * @param content 活动内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 广告开始时间
     * @return start_time 广告开始时间
     */
    public LocalDateTime getStartTime() {
        return startTime;
    }

    /**
     * 广告开始时间
     * @param startTime 广告开始时间
     */
    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    /**
     * 广告结束时间
     * @return end_time 广告结束时间
     */
    public LocalDateTime getEndTime() {
        return endTime;
    }

    /**
     * 广告结束时间
     * @param endTime 广告结束时间
     */
    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    /**
     * 是否启动
     * @return enabled 是否启动
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * 是否启动
     * @param enabled 是否启动
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 广告表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 广告表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", link=").append(link);
        sb.append(", url=").append(url);
        sb.append(", position=").append(position);
        sb.append(", content=").append(content);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", enabled=").append(enabled);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 广告表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallAd other = (LitemallAd) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getLink() == null ? other.getLink() == null : this.getLink().equals(other.getLink()))
            && (this.getUrl() == null ? other.getUrl() == null : this.getUrl().equals(other.getUrl()))
            && (this.getPosition() == null ? other.getPosition() == null : this.getPosition().equals(other.getPosition()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
            && (this.getStartTime() == null ? other.getStartTime() == null : this.getStartTime().equals(other.getStartTime()))
            && (this.getEndTime() == null ? other.getEndTime() == null : this.getEndTime().equals(other.getEndTime()))
            && (this.getEnabled() == null ? other.getEnabled() == null : this.getEnabled().equals(other.getEnabled()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * 广告表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getLink() == null) ? 0 : getLink().hashCode());
        result = prime * result + ((getUrl() == null) ? 0 : getUrl().hashCode());
        result = prime * result + ((getPosition() == null) ? 0 : getPosition().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + ((getStartTime() == null) ? 0 : getStartTime().hashCode());
        result = prime * result + ((getEndTime() == null) ? 0 : getEndTime().hashCode());
        result = prime * result + ((getEnabled() == null) ? 0 : getEnabled().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_ad
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_ad
         */
        private final Boolean value;

        /**
         * litemall_ad
         */
        private final String name;

        /**
         * 广告表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 广告表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 广告表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 广告表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 广告表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 广告表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_ad
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        name("name", "name", "VARCHAR", true),
        link("link", "link", "VARCHAR", false),
        url("url", "url", "VARCHAR", false),
        position("position", "position", "TINYINT", true),
        content("content", "content", "VARCHAR", false),
        startTime("start_time", "startTime", "TIMESTAMP", false),
        endTime("end_time", "endTime", "TIMESTAMP", false),
        enabled("enabled", "enabled", "BIT", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_ad
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_ad
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_ad
         */
        private final String column;

        /**
         * litemall_ad
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_ad
         */
        private final String javaProperty;

        /**
         * litemall_ad
         */
        private final String jdbcType;

        /**
         * 广告表
         */
        public String value() {
            return this.column;
        }

        /**
         * 广告表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 广告表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 广告表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 广告表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 广告表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 广告表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 广告表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 广告表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 广告表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 广告表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}