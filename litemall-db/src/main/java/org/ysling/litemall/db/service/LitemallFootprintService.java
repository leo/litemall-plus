package org.ysling.litemall.db.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import com.github.pagehelper.PageHelper;
import org.ysling.litemall.db.dao.LitemallFootprintMapper;
import org.ysling.litemall.db.domain.LitemallFootprint;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.ysling.litemall.db.domain.LitemallGoods;
import org.ysling.litemall.db.example.LitemallFootprintExample;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
@CacheConfig(cacheNames = "footprint")
public class LitemallFootprintService {

    @Resource
    private LitemallFootprintMapper footprintMapper;

    @Cacheable(sync = true)
    public List<LitemallFootprint> queryByAddTime(Integer userId, Integer page, Integer size) {
        LitemallFootprintExample example = new LitemallFootprintExample();
        example.or().andUserIdEqualTo(userId).andDeletedEqualTo(false);
        example.setOrderByClause(LitemallFootprint.Column.addTime.desc());
        PageHelper.startPage(page, size);
        return footprintMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public LitemallFootprint findById(Integer id) {
        return footprintMapper.selectByPrimaryKey(id);
    }

    @Cacheable(sync = true)
    public LitemallFootprint findById(Integer userId, Integer id) {
        LitemallFootprintExample example = new LitemallFootprintExample();
        example.or().andIdEqualTo(id).andUserIdEqualTo(userId).andDeletedEqualTo(false);
        return footprintMapper.selectOneByExample(example);
    }

    @CacheEvict(allEntries = true)
    public void deleteById(Integer id) {
        footprintMapper.logicalDeleteByPrimaryKey(id);
    }

    @CacheEvict(allEntries = true)
    public void add(LitemallFootprint footprint) {
        footprint.setAddTime(LocalDateTime.now());
        footprint.setUpdateTime(LocalDateTime.now());
        footprintMapper.insertSelective(footprint);
    }

    @Cacheable(sync = true)
    public LitemallFootprint findByGoodId(Integer userId, Integer goodId) {
        LitemallFootprintExample example = new LitemallFootprintExample();
        example.or().andGoodsIdEqualTo(goodId).andUserIdEqualTo(userId).andDeletedEqualTo(false);
        return footprintMapper.selectOneByExample(example);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallFootprint footprint) {
        footprint.setUpdateTime(LocalDateTime.now());
        return footprintMapper.updateWithVersionByPrimaryKeySelective(footprint.getVersion(), footprint);
    }

    @CacheEvict(allEntries = true)
    public void createFootprint(Integer userId , LitemallGoods goods) {
        if (userId == null) return;
        LitemallFootprint footprint = findByGoodId(userId, goods.getId());
        if (footprint != null){
            footprint.setName(goods.getName());
            footprint.setBrief(goods.getBrief());
            footprint.setPicUrl(goods.getPicUrl());
            footprint.setRetailPrice(goods.getRetailPrice());
            footprint.setAddTime(LocalDateTime.now());
            updateVersionSelective(footprint);
        }else {
            footprint = new LitemallFootprint();
            footprint.setUserId(userId);
            footprint.setGoodsId(goods.getId());
            footprint.setName(goods.getName());
            footprint.setBrief(goods.getBrief());
            footprint.setPicUrl(goods.getPicUrl());
            footprint.setRetailPrice(goods.getRetailPrice());
            footprint.setAddTime(LocalDateTime.now());
            footprint.setUpdateTime(LocalDateTime.now());
            footprint.setAddTime(LocalDateTime.now());
            footprint.setUpdateTime(LocalDateTime.now());
            footprintMapper.insertSelective(footprint);
        }
    }

    @Cacheable(sync = true)
    public List<LitemallFootprint> querySelective(String userId, String goodsId, Integer page, Integer size, String sort, String order) {
        LitemallFootprintExample example = new LitemallFootprintExample();
        LitemallFootprintExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);

        if (StringUtils.hasText(userId)) {
            criteria.andUserIdEqualTo(Integer.valueOf(userId));
        }
        if (StringUtils.hasText(goodsId)) {
            criteria.andGoodsIdEqualTo(Integer.valueOf(goodsId));
        }
        if (StringUtils.hasText(sort) && StringUtils.hasText(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return footprintMapper.selectByExample(example);
    }
}
