package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallMessage;
import org.ysling.litemall.db.example.LitemallMessageExample;

@Mapper
@Repository
public interface LitemallMessageMapper {
    /**
     * webSocket消息
     */
    long countByExample(LitemallMessageExample example);

    /**
     * webSocket消息
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallMessageExample example);

    /**
     * webSocket消息
     */
    int deleteByExample(LitemallMessageExample example);

    /**
     * webSocket消息
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * webSocket消息
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * webSocket消息
     */
    int insert(LitemallMessage record);

    /**
     * webSocket消息
     */
    int insertSelective(LitemallMessage record);

    /**
     * webSocket消息
     */
    LitemallMessage selectOneByExample(LitemallMessageExample example);

    /**
     * webSocket消息
     */
    LitemallMessage selectOneByExampleSelective(@Param("example") LitemallMessageExample example, @Param("selective") LitemallMessage.Column ... selective);

    /**
     * webSocket消息
     */
    List<LitemallMessage> selectByExampleSelective(@Param("example") LitemallMessageExample example, @Param("selective") LitemallMessage.Column ... selective);

    /**
     * webSocket消息
     */
    List<LitemallMessage> selectByExample(LitemallMessageExample example);

    /**
     * webSocket消息
     */
    LitemallMessage selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallMessage.Column ... selective);

    /**
     * webSocket消息
     */
    LitemallMessage selectByPrimaryKey(Integer id);

    /**
     * webSocket消息
     */
    LitemallMessage selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * webSocket消息
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallMessage record, @Param("example") LitemallMessageExample example);

    /**
     * webSocket消息
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallMessage record, @Param("example") LitemallMessageExample example);

    /**
     * webSocket消息
     */
    int updateByExampleSelective(@Param("record") LitemallMessage record, @Param("example") LitemallMessageExample example);

    /**
     * webSocket消息
     */
    int updateByExample(@Param("record") LitemallMessage record, @Param("example") LitemallMessageExample example);

    /**
     * webSocket消息
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallMessage record);

    /**
     * webSocket消息
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallMessage record);

    /**
     * webSocket消息
     */
    int updateByPrimaryKeySelective(LitemallMessage record);

    /**
     * webSocket消息
     */
    int updateByPrimaryKey(LitemallMessage record);

    /**
     * webSocket消息
     */
    int logicalDeleteByExample(@Param("example") LitemallMessageExample example);

    /**
     * webSocket消息
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallMessageExample example);

    /**
     * webSocket消息
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * webSocket消息
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}