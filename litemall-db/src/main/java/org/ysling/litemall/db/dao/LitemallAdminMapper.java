package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallAdmin;
import org.ysling.litemall.db.example.LitemallAdminExample;

@Mapper
@Repository
public interface LitemallAdminMapper {
    /**
     * 管理员表
     */
    long countByExample(LitemallAdminExample example);

    /**
     * 管理员表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallAdminExample example);

    /**
     * 管理员表
     */
    int deleteByExample(LitemallAdminExample example);

    /**
     * 管理员表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 管理员表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 管理员表
     */
    int insert(LitemallAdmin record);

    /**
     * 管理员表
     */
    int insertSelective(LitemallAdmin record);

    /**
     * 管理员表
     */
    LitemallAdmin selectOneByExample(LitemallAdminExample example);

    /**
     * 管理员表
     */
    LitemallAdmin selectOneByExampleSelective(@Param("example") LitemallAdminExample example, @Param("selective") LitemallAdmin.Column ... selective);

    /**
     * 管理员表
     */
    List<LitemallAdmin> selectByExampleSelective(@Param("example") LitemallAdminExample example, @Param("selective") LitemallAdmin.Column ... selective);

    /**
     * 管理员表
     */
    List<LitemallAdmin> selectByExample(LitemallAdminExample example);

    /**
     * 管理员表
     */
    LitemallAdmin selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallAdmin.Column ... selective);

    /**
     * 管理员表
     */
    LitemallAdmin selectByPrimaryKey(Integer id);

    /**
     * 管理员表
     */
    LitemallAdmin selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 管理员表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallAdmin record, @Param("example") LitemallAdminExample example);

    /**
     * 管理员表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallAdmin record, @Param("example") LitemallAdminExample example);

    /**
     * 管理员表
     */
    int updateByExampleSelective(@Param("record") LitemallAdmin record, @Param("example") LitemallAdminExample example);

    /**
     * 管理员表
     */
    int updateByExample(@Param("record") LitemallAdmin record, @Param("example") LitemallAdminExample example);

    /**
     * 管理员表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallAdmin record);

    /**
     * 管理员表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallAdmin record);

    /**
     * 管理员表
     */
    int updateByPrimaryKeySelective(LitemallAdmin record);

    /**
     * 管理员表
     */
    int updateByPrimaryKey(LitemallAdmin record);

    /**
     * 管理员表
     */
    int logicalDeleteByExample(@Param("example") LitemallAdminExample example);

    /**
     * 管理员表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallAdminExample example);

    /**
     * 管理员表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 管理员表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}