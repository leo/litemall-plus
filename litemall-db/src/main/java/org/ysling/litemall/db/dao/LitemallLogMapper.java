package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallLog;
import org.ysling.litemall.db.example.LitemallLogExample;

@Mapper
@Repository
public interface LitemallLogMapper {
    /**
     * 操作日志表
     */
    long countByExample(LitemallLogExample example);

    /**
     * 操作日志表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallLogExample example);

    /**
     * 操作日志表
     */
    int deleteByExample(LitemallLogExample example);

    /**
     * 操作日志表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 操作日志表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 操作日志表
     */
    int insert(LitemallLog record);

    /**
     * 操作日志表
     */
    int insertSelective(LitemallLog record);

    /**
     * 操作日志表
     */
    LitemallLog selectOneByExample(LitemallLogExample example);

    /**
     * 操作日志表
     */
    LitemallLog selectOneByExampleSelective(@Param("example") LitemallLogExample example, @Param("selective") LitemallLog.Column ... selective);

    /**
     * 操作日志表
     */
    List<LitemallLog> selectByExampleSelective(@Param("example") LitemallLogExample example, @Param("selective") LitemallLog.Column ... selective);

    /**
     * 操作日志表
     */
    List<LitemallLog> selectByExample(LitemallLogExample example);

    /**
     * 操作日志表
     */
    LitemallLog selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallLog.Column ... selective);

    /**
     * 操作日志表
     */
    LitemallLog selectByPrimaryKey(Integer id);

    /**
     * 操作日志表
     */
    LitemallLog selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 操作日志表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallLog record, @Param("example") LitemallLogExample example);

    /**
     * 操作日志表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallLog record, @Param("example") LitemallLogExample example);

    /**
     * 操作日志表
     */
    int updateByExampleSelective(@Param("record") LitemallLog record, @Param("example") LitemallLogExample example);

    /**
     * 操作日志表
     */
    int updateByExample(@Param("record") LitemallLog record, @Param("example") LitemallLogExample example);

    /**
     * 操作日志表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallLog record);

    /**
     * 操作日志表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallLog record);

    /**
     * 操作日志表
     */
    int updateByPrimaryKeySelective(LitemallLog record);

    /**
     * 操作日志表
     */
    int updateByPrimaryKey(LitemallLog record);

    /**
     * 操作日志表
     */
    int logicalDeleteByExample(@Param("example") LitemallLogExample example);

    /**
     * 操作日志表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallLogExample example);

    /**
     * 操作日志表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 操作日志表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}