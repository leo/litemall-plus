package org.ysling.litemall.db.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import com.github.pagehelper.PageHelper;
import org.ysling.litemall.db.dao.LitemallNoticeMapper;
import org.ysling.litemall.db.domain.LitemallNotice;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.ysling.litemall.db.domain.LitemallNoticeAdmin;
import org.ysling.litemall.db.example.LitemallNoticeExample;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
@CacheConfig(cacheNames = "notice")
public class LitemallNoticeService {

    @Resource
    private LitemallNoticeMapper noticeMapper;

    @Cacheable(sync = true)
    public LitemallNotice findById(Integer id) {
        return noticeMapper.selectByPrimaryKey(id);
    }

    @Cacheable(sync = true)
    public List<LitemallNotice> querySelective(String title, String content, Integer page, Integer limit, String sort, String order) {
        LitemallNoticeExample example = new LitemallNoticeExample();
        LitemallNoticeExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);

        if (StringUtils.hasText(title)) {
            criteria.andTitleLike("%" + title + "%");
        }
        if (StringUtils.hasText(content)) {
            criteria.andContentLike("%" + content + "%");
        }
        if (StringUtils.hasText(sort) && StringUtils.hasText(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, limit);
        return noticeMapper.selectByExample(example);
    }

    @CacheEvict(allEntries = true)
    public int updateById(LitemallNotice notice) {
        notice.setUpdateTime(LocalDateTime.now());
        return noticeMapper.updateByPrimaryKeySelective(notice);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallNotice notice) {
        notice.setUpdateTime(LocalDateTime.now());
        return noticeMapper.updateWithVersionByPrimaryKeySelective(notice.getVersion(), notice);
    }

    @CacheEvict(allEntries = true)
    public void deleteById(Integer id) {
        noticeMapper.logicalDeleteByPrimaryKey(id);
    }

    @CacheEvict(allEntries = true)
    public void add(LitemallNotice notice) {
        notice.setAddTime(LocalDateTime.now());
        notice.setUpdateTime(LocalDateTime.now());
        noticeMapper.insertSelective(notice);
    }

    @CacheEvict(allEntries = true)
    public void deleteByIds(List<Integer> ids) {
        LitemallNoticeExample example = new LitemallNoticeExample();
        example.or().andIdIn(ids).andDeletedEqualTo(false);
        LitemallNotice notice = new LitemallNotice();
        notice.setUpdateTime(LocalDateTime.now());
        notice.setDeleted(true);
        noticeMapper.updateByExampleSelective(notice, example);
    }
}
