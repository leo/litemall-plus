package org.ysling.litemall.db.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.

import com.github.pagehelper.PageHelper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.ysling.litemall.db.dao.LitemallRewardMapper;
import org.ysling.litemall.db.domain.LitemallReward;
import org.ysling.litemall.db.example.LitemallRewardExample;
import org.ysling.litemall.db.constant.RewardConstant;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
@CacheConfig(cacheNames = "reward")
public class LitemallRewardService {

    @Resource
    private LitemallRewardMapper rewardMapper;

    @Cacheable(sync = true)
    public Integer countReward(Integer taskId) {
        LitemallRewardExample example = new LitemallRewardExample();
        example.or().andTaskIdEqualTo(taskId).andStatusEqualTo(RewardConstant.STATUS_SUCCEED).andDeletedEqualTo(false);
        return (int)rewardMapper.countByExample(example);
    }

    @Cacheable(sync = true)
    public Integer countAndReward(Integer userId, Integer taskId) {
        LitemallRewardExample example = new LitemallRewardExample();
        example.or().andIdEqualTo(userId).andTaskIdEqualTo(taskId).andDeletedEqualTo(false);
        return (int)rewardMapper.countByExample(example);
    }

    /**
     * 获取分享者记录
     *
     * @param userId
     * @param taskId
     * @return
     */
    @Cacheable(sync = true)
    public LitemallReward findSharer(Integer userId, Integer taskId) {
        LitemallRewardExample example = new LitemallRewardExample();
        example.or().andTaskIdEqualTo(taskId).andUserIdEqualTo(userId).andRewardIdEqualTo(0).andDeletedEqualTo(false);
        return rewardMapper.selectOneByExample(example);
    }

    @CacheEvict(allEntries = true)
    public void add(LitemallReward reward) {
        reward.setAddTime(LocalDateTime.now());
        reward.setUpdateTime(LocalDateTime.now());
        rewardMapper.insertSelective(reward);
    }

    /**
     * 获取某个赏金活动参与的记录
     *
     * @param rewardId
     * @return
     */
    @Cacheable(sync = true)
    public List<LitemallReward> queryJoinRecord(Integer rewardId) {
        LitemallRewardExample example = new LitemallRewardExample();
        example.or().andRewardIdEqualTo(rewardId).andStatusEqualTo(RewardConstant.STATUS_SUCCEED).andDeletedEqualTo(false);
        example.orderBy("add_time desc");
        return rewardMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallReward> queryList(Integer userId, Integer taskId, Integer rewardId, Short status, Integer page, Integer size, String sort, String order) {
        LitemallRewardExample example = new LitemallRewardExample();
        LitemallRewardExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);

        if (userId != null) {
            criteria.andUserIdEqualTo(userId);
        }
        if(taskId != null){
            criteria.andTaskIdEqualTo(taskId);
        }
        if (status != null) {
            criteria.andStatusEqualTo(status);
        }
        if(rewardId != null){
            criteria.andRewardIdEqualTo(rewardId);
        }

        if (StringUtils.hasText(sort) && StringUtils.hasText(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        if (!Objects.isNull(page) && !Objects.isNull(size)) {
            PageHelper.startPage(page, size);
        }

        return rewardMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public LitemallReward findById(Integer id) {
        return rewardMapper.selectByPrimaryKey(id);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallReward reward) {
        reward.setUpdateTime(LocalDateTime.now());
        return rewardMapper.updateWithVersionByPrimaryKeySelective(reward.getVersion() ,reward);
    }

    @CacheEvict(allEntries = true)
    public int updateSelective(LitemallReward reward) {
        reward.setUpdateTime(LocalDateTime.now());
        return rewardMapper.updateByPrimaryKeySelective(reward);
    }

    @Cacheable(sync = true)
    public LitemallReward findByOrderId(Integer orderId) {
        LitemallRewardExample example = new LitemallRewardExample();
        example.or().andOrderIdEqualTo(orderId).andDeletedEqualTo(false);
        return rewardMapper.selectOneByExample(example);
    }
}
