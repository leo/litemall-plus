package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallCollect;
import org.ysling.litemall.db.example.LitemallCollectExample;

@Mapper
@Repository
public interface LitemallCollectMapper {
    /**
     * 收藏表
     */
    long countByExample(LitemallCollectExample example);

    /**
     * 收藏表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallCollectExample example);

    /**
     * 收藏表
     */
    int deleteByExample(LitemallCollectExample example);

    /**
     * 收藏表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 收藏表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 收藏表
     */
    int insert(LitemallCollect record);

    /**
     * 收藏表
     */
    int insertSelective(LitemallCollect record);

    /**
     * 收藏表
     */
    LitemallCollect selectOneByExample(LitemallCollectExample example);

    /**
     * 收藏表
     */
    LitemallCollect selectOneByExampleSelective(@Param("example") LitemallCollectExample example, @Param("selective") LitemallCollect.Column ... selective);

    /**
     * 收藏表
     */
    List<LitemallCollect> selectByExampleSelective(@Param("example") LitemallCollectExample example, @Param("selective") LitemallCollect.Column ... selective);

    /**
     * 收藏表
     */
    List<LitemallCollect> selectByExample(LitemallCollectExample example);

    /**
     * 收藏表
     */
    LitemallCollect selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallCollect.Column ... selective);

    /**
     * 收藏表
     */
    LitemallCollect selectByPrimaryKey(Integer id);

    /**
     * 收藏表
     */
    LitemallCollect selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 收藏表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallCollect record, @Param("example") LitemallCollectExample example);

    /**
     * 收藏表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallCollect record, @Param("example") LitemallCollectExample example);

    /**
     * 收藏表
     */
    int updateByExampleSelective(@Param("record") LitemallCollect record, @Param("example") LitemallCollectExample example);

    /**
     * 收藏表
     */
    int updateByExample(@Param("record") LitemallCollect record, @Param("example") LitemallCollectExample example);

    /**
     * 收藏表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallCollect record);

    /**
     * 收藏表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallCollect record);

    /**
     * 收藏表
     */
    int updateByPrimaryKeySelective(LitemallCollect record);

    /**
     * 收藏表
     */
    int updateByPrimaryKey(LitemallCollect record);

    /**
     * 收藏表
     */
    int logicalDeleteByExample(@Param("example") LitemallCollectExample example);

    /**
     * 收藏表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallCollectExample example);

    /**
     * 收藏表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 收藏表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}