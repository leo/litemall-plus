package org.ysling.litemall.db.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import org.ysling.litemall.db.dao.LitemallGoodsAttributeMapper;
import org.ysling.litemall.db.domain.LitemallCouponUser;
import org.ysling.litemall.db.domain.LitemallGoodsAttribute;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.ysling.litemall.db.example.LitemallGoodsAttributeExample;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
@CacheConfig(cacheNames = "goodsAttribute")
public class LitemallGoodsAttributeService {

    @Resource
    private LitemallGoodsAttributeMapper goodsAttributeMapper;

    @Cacheable(sync = true)
    public List<LitemallGoodsAttribute> queryByGid(Integer goodsId) {
        LitemallGoodsAttributeExample example = new LitemallGoodsAttributeExample();
        example.or().andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        return goodsAttributeMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public LitemallGoodsAttribute findById(Integer id) {
        return goodsAttributeMapper.selectByPrimaryKey(id);
    }

    @CacheEvict(allEntries = true)
    public void add(LitemallGoodsAttribute goodsAttribute) {
        goodsAttribute.setAddTime(LocalDateTime.now());
        goodsAttribute.setUpdateTime(LocalDateTime.now());
        goodsAttributeMapper.insertSelective(goodsAttribute);
    }

    @CacheEvict(allEntries = true)
    public void deleteByGid(Integer gid) {
        LitemallGoodsAttributeExample example = new LitemallGoodsAttributeExample();
        example.or().andGoodsIdEqualTo(gid);
        goodsAttributeMapper.logicalDeleteByExample(example);
    }

    @CacheEvict(allEntries = true)
    public void deleteById(Integer id) {
        goodsAttributeMapper.logicalDeleteByPrimaryKey(id);
    }

    @CacheEvict(allEntries = true)
    public void updateById(LitemallGoodsAttribute attribute) {
        attribute.setUpdateTime(LocalDateTime.now());
        goodsAttributeMapper.updateByPrimaryKeySelective(attribute);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallGoodsAttribute attribute) {
        attribute.setUpdateTime(LocalDateTime.now());
        return goodsAttributeMapper.updateWithVersionByPrimaryKeySelective(attribute.getVersion(), attribute);
    }
}
