package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_topic
 * Database Table Remarks : 
 *   专题表
 * @author ysling
 */
public class LitemallTopic implements Serializable {
    /**
     * litemall_topic
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_topic
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 专题表ID
     */
    private Integer id;

    /**
     * 专题标题
     */
    private String title;

    /**
     * 专题子标题
     */
    private String subtitle;

    /**
     * 专题相关商品最低价
     */
    private BigDecimal price;

    /**
     * 专题阅读量
     */
    private String readCount;

    /**
     * 专题图片
     */
    private String picUrl;

    /**
     * 排序
     */
    private Integer sortOrder;

    /**
     * 专题相关商品，采用JSON数组格式
     */
    private Integer[] goods;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * 专题内容，富文本格式
     */
    private String content;

    /**
     * litemall_topic
     */
    private static final long serialVersionUID = 1L;

    /**
     * 专题表ID
     * @return id 专题表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 专题表ID
     * @param id 专题表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 专题标题
     * @return title 专题标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 专题标题
     * @param title 专题标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 专题子标题
     * @return subtitle 专题子标题
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * 专题子标题
     * @param subtitle 专题子标题
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * 专题相关商品最低价
     * @return price 专题相关商品最低价
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 专题相关商品最低价
     * @param price 专题相关商品最低价
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * 专题阅读量
     * @return read_count 专题阅读量
     */
    public String getReadCount() {
        return readCount;
    }

    /**
     * 专题阅读量
     * @param readCount 专题阅读量
     */
    public void setReadCount(String readCount) {
        this.readCount = readCount;
    }

    /**
     * 专题图片
     * @return pic_url 专题图片
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 专题图片
     * @param picUrl 专题图片
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * 排序
     * @return sort_order 排序
     */
    public Integer getSortOrder() {
        return sortOrder;
    }

    /**
     * 排序
     * @param sortOrder 排序
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * 专题相关商品，采用JSON数组格式
     * @return goods 专题相关商品，采用JSON数组格式
     */
    public Integer[] getGoods() {
        return goods;
    }

    /**
     * 专题相关商品，采用JSON数组格式
     * @param goods 专题相关商品，采用JSON数组格式
     */
    public void setGoods(Integer[] goods) {
        this.goods = goods;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 专题表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 专题内容，富文本格式
     * @return content 专题内容，富文本格式
     */
    public String getContent() {
        return content;
    }

    /**
     * 专题内容，富文本格式
     * @param content 专题内容，富文本格式
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 专题表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", title=").append(title);
        sb.append(", subtitle=").append(subtitle);
        sb.append(", price=").append(price);
        sb.append(", readCount=").append(readCount);
        sb.append(", picUrl=").append(picUrl);
        sb.append(", sortOrder=").append(sortOrder);
        sb.append(", goods=").append(goods);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append(", content=").append(content);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 专题表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallTopic other = (LitemallTopic) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
            && (this.getSubtitle() == null ? other.getSubtitle() == null : this.getSubtitle().equals(other.getSubtitle()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (this.getReadCount() == null ? other.getReadCount() == null : this.getReadCount().equals(other.getReadCount()))
            && (this.getPicUrl() == null ? other.getPicUrl() == null : this.getPicUrl().equals(other.getPicUrl()))
            && (this.getSortOrder() == null ? other.getSortOrder() == null : this.getSortOrder().equals(other.getSortOrder()))
            && (Arrays.equals(this.getGoods(), other.getGoods()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()));
    }

    /**
     * 专题表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
        result = prime * result + ((getSubtitle() == null) ? 0 : getSubtitle().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getReadCount() == null) ? 0 : getReadCount().hashCode());
        result = prime * result + ((getPicUrl() == null) ? 0 : getPicUrl().hashCode());
        result = prime * result + ((getSortOrder() == null) ? 0 : getSortOrder().hashCode());
        result = prime * result + (Arrays.hashCode(getGoods()));
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        return result;
    }

    /**
     * litemall_topic
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_topic
         */
        private final Boolean value;

        /**
         * litemall_topic
         */
        private final String name;

        /**
         * 专题表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 专题表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 专题表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 专题表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 专题表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 专题表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_topic
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        title("title", "title", "VARCHAR", false),
        subtitle("subtitle", "subtitle", "VARCHAR", false),
        price("price", "price", "DECIMAL", false),
        readCount("read_count", "readCount", "VARCHAR", false),
        picUrl("pic_url", "picUrl", "VARCHAR", false),
        sortOrder("sort_order", "sortOrder", "INTEGER", false),
        goods("goods", "goods", "VARCHAR", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false),
        content("content", "content", "LONGVARCHAR", false);

        /**
         * litemall_topic
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_topic
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_topic
         */
        private final String column;

        /**
         * litemall_topic
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_topic
         */
        private final String javaProperty;

        /**
         * litemall_topic
         */
        private final String jdbcType;

        /**
         * 专题表
         */
        public String value() {
            return this.column;
        }

        /**
         * 专题表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 专题表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 专题表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 专题表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 专题表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 专题表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 专题表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 专题表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 专题表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 专题表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}