package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_collect
 * Database Table Remarks : 
 *   收藏表
 * @author ysling
 */
public class LitemallCollect implements Serializable {
    /**
     * litemall_collect
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_collect
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 收藏表ID
     */
    private Integer id;

    /**
     * 用户表的用户ID
     */
    private Integer userId;

    /**
     * 如果type=0，则是商品ID；如果type=1，则是专题ID
     */
    private Integer valueId;

    /**
     * 收藏名称
     */
    private String name;

    /**
     * 相关商品最低价
     */
    private BigDecimal price;

    /**
     * 收藏简介
     */
    private String brief;

    /**
     * 收藏图片
     */
    private String picUrl;

    /**
     * 收藏类型，如果type=0，则是商品ID；如果type=1，则是专题ID
     */
    private Byte type;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_collect
     */
    private static final long serialVersionUID = 1L;

    /**
     * 收藏表ID
     * @return id 收藏表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 收藏表ID
     * @param id 收藏表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 用户表的用户ID
     * @return user_id 用户表的用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 用户表的用户ID
     * @param userId 用户表的用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 如果type=0，则是商品ID；如果type=1，则是专题ID
     * @return value_id 如果type=0，则是商品ID；如果type=1，则是专题ID
     */
    public Integer getValueId() {
        return valueId;
    }

    /**
     * 如果type=0，则是商品ID；如果type=1，则是专题ID
     * @param valueId 如果type=0，则是商品ID；如果type=1，则是专题ID
     */
    public void setValueId(Integer valueId) {
        this.valueId = valueId;
    }

    /**
     * 收藏名称
     * @return name 收藏名称
     */
    public String getName() {
        return name;
    }

    /**
     * 收藏名称
     * @param name 收藏名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 相关商品最低价
     * @return price 相关商品最低价
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 相关商品最低价
     * @param price 相关商品最低价
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * 收藏简介
     * @return brief 收藏简介
     */
    public String getBrief() {
        return brief;
    }

    /**
     * 收藏简介
     * @param brief 收藏简介
     */
    public void setBrief(String brief) {
        this.brief = brief;
    }

    /**
     * 收藏图片
     * @return pic_url 收藏图片
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 收藏图片
     * @param picUrl 收藏图片
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * 收藏类型，如果type=0，则是商品ID；如果type=1，则是专题ID
     * @return type 收藏类型，如果type=0，则是商品ID；如果type=1，则是专题ID
     */
    public Byte getType() {
        return type;
    }

    /**
     * 收藏类型，如果type=0，则是商品ID；如果type=1，则是专题ID
     * @param type 收藏类型，如果type=0，则是商品ID；如果type=1，则是专题ID
     */
    public void setType(Byte type) {
        this.type = type;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 收藏表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 收藏表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", valueId=").append(valueId);
        sb.append(", name=").append(name);
        sb.append(", price=").append(price);
        sb.append(", brief=").append(brief);
        sb.append(", picUrl=").append(picUrl);
        sb.append(", type=").append(type);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 收藏表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallCollect other = (LitemallCollect) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getValueId() == null ? other.getValueId() == null : this.getValueId().equals(other.getValueId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (this.getBrief() == null ? other.getBrief() == null : this.getBrief().equals(other.getBrief()))
            && (this.getPicUrl() == null ? other.getPicUrl() == null : this.getPicUrl().equals(other.getPicUrl()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * 收藏表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getValueId() == null) ? 0 : getValueId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getBrief() == null) ? 0 : getBrief().hashCode());
        result = prime * result + ((getPicUrl() == null) ? 0 : getPicUrl().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_collect
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_collect
         */
        private final Boolean value;

        /**
         * litemall_collect
         */
        private final String name;

        /**
         * 收藏表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 收藏表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 收藏表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 收藏表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 收藏表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 收藏表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_collect
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        userId("user_id", "userId", "INTEGER", false),
        valueId("value_id", "valueId", "INTEGER", false),
        name("name", "name", "VARCHAR", true),
        price("price", "price", "DECIMAL", false),
        brief("brief", "brief", "VARCHAR", false),
        picUrl("pic_url", "picUrl", "VARCHAR", false),
        type("type", "type", "TINYINT", true),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_collect
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_collect
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_collect
         */
        private final String column;

        /**
         * litemall_collect
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_collect
         */
        private final String javaProperty;

        /**
         * litemall_collect
         */
        private final String jdbcType;

        /**
         * 收藏表
         */
        public String value() {
            return this.column;
        }

        /**
         * 收藏表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 收藏表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 收藏表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 收藏表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 收藏表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 收藏表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 收藏表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 收藏表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 收藏表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 收藏表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}