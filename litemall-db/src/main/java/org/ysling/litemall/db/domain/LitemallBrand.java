package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_brand
 * Database Table Remarks : 
 *   品牌商表
 * @author ysling
 */
public class LitemallBrand implements Serializable {
    /**
     * litemall_brand
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_brand
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 品牌商表ID
     */
    private Integer id;

    /**
     * 用户表的用户ID
     */
    private Integer userId;

    /**
     * 品牌商名称
     */
    private String name;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 排序
     */
    private Integer sortOrder;

    /**
     * 品牌商简介
     */
    private String desc;

    /**
     * 品牌商页的品牌商图片
     */
    private String picUrl;

    /**
     * 品牌商邮箱
     */
    private String mail;

    /**
     * 品牌商的商品低价，仅用于页面展示
     */
    private BigDecimal floorPrice;

    /**
     * 0 可用, 1 禁用, 2 注销
     */
    private Byte status;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_brand
     */
    private static final long serialVersionUID = 1L;

    /**
     * 品牌商表ID
     * @return id 品牌商表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 品牌商表ID
     * @param id 品牌商表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 用户表的用户ID
     * @return user_id 用户表的用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 用户表的用户ID
     * @param userId 用户表的用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 品牌商名称
     * @return name 品牌商名称
     */
    public String getName() {
        return name;
    }

    /**
     * 品牌商名称
     * @param name 品牌商名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 用户姓名
     * @return user_name 用户姓名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 用户姓名
     * @param userName 用户姓名
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 排序
     * @return sort_order 排序
     */
    public Integer getSortOrder() {
        return sortOrder;
    }

    /**
     * 排序
     * @param sortOrder 排序
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * 品牌商简介
     * @return desc 品牌商简介
     */
    public String getDesc() {
        return desc;
    }

    /**
     * 品牌商简介
     * @param desc 品牌商简介
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * 品牌商页的品牌商图片
     * @return pic_url 品牌商页的品牌商图片
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 品牌商页的品牌商图片
     * @param picUrl 品牌商页的品牌商图片
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * 品牌商邮箱
     * @return mail 品牌商邮箱
     */
    public String getMail() {
        return mail;
    }

    /**
     * 品牌商邮箱
     * @param mail 品牌商邮箱
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * 品牌商的商品低价，仅用于页面展示
     * @return floor_price 品牌商的商品低价，仅用于页面展示
     */
    public BigDecimal getFloorPrice() {
        return floorPrice;
    }

    /**
     * 品牌商的商品低价，仅用于页面展示
     * @param floorPrice 品牌商的商品低价，仅用于页面展示
     */
    public void setFloorPrice(BigDecimal floorPrice) {
        this.floorPrice = floorPrice;
    }

    /**
     * 0 可用, 1 禁用, 2 注销
     * @return status 0 可用, 1 禁用, 2 注销
     */
    public Byte getStatus() {
        return status;
    }

    /**
     * 0 可用, 1 禁用, 2 注销
     * @param status 0 可用, 1 禁用, 2 注销
     */
    public void setStatus(Byte status) {
        this.status = status;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 品牌商表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 品牌商表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", name=").append(name);
        sb.append(", userName=").append(userName);
        sb.append(", sortOrder=").append(sortOrder);
        sb.append(", desc=").append(desc);
        sb.append(", picUrl=").append(picUrl);
        sb.append(", mail=").append(mail);
        sb.append(", floorPrice=").append(floorPrice);
        sb.append(", status=").append(status);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 品牌商表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallBrand other = (LitemallBrand) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getUserName() == null ? other.getUserName() == null : this.getUserName().equals(other.getUserName()))
            && (this.getSortOrder() == null ? other.getSortOrder() == null : this.getSortOrder().equals(other.getSortOrder()))
            && (this.getDesc() == null ? other.getDesc() == null : this.getDesc().equals(other.getDesc()))
            && (this.getPicUrl() == null ? other.getPicUrl() == null : this.getPicUrl().equals(other.getPicUrl()))
            && (this.getMail() == null ? other.getMail() == null : this.getMail().equals(other.getMail()))
            && (this.getFloorPrice() == null ? other.getFloorPrice() == null : this.getFloorPrice().equals(other.getFloorPrice()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * 品牌商表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
        result = prime * result + ((getSortOrder() == null) ? 0 : getSortOrder().hashCode());
        result = prime * result + ((getDesc() == null) ? 0 : getDesc().hashCode());
        result = prime * result + ((getPicUrl() == null) ? 0 : getPicUrl().hashCode());
        result = prime * result + ((getMail() == null) ? 0 : getMail().hashCode());
        result = prime * result + ((getFloorPrice() == null) ? 0 : getFloorPrice().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_brand
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_brand
         */
        private final Boolean value;

        /**
         * litemall_brand
         */
        private final String name;

        /**
         * 品牌商表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 品牌商表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 品牌商表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 品牌商表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 品牌商表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 品牌商表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_brand
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        userId("user_id", "userId", "INTEGER", false),
        name("name", "name", "VARCHAR", true),
        userName("user_name", "userName", "VARCHAR", false),
        sortOrder("sort_order", "sortOrder", "INTEGER", false),
        desc("desc", "desc", "VARCHAR", true),
        picUrl("pic_url", "picUrl", "VARCHAR", false),
        mail("mail", "mail", "VARCHAR", false),
        floorPrice("floor_price", "floorPrice", "DECIMAL", false),
        status("status", "status", "TINYINT", true),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_brand
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_brand
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_brand
         */
        private final String column;

        /**
         * litemall_brand
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_brand
         */
        private final String javaProperty;

        /**
         * litemall_brand
         */
        private final String jdbcType;

        /**
         * 品牌商表
         */
        public String value() {
            return this.column;
        }

        /**
         * 品牌商表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 品牌商表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 品牌商表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 品牌商表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 品牌商表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 品牌商表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 品牌商表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 品牌商表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 品牌商表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 品牌商表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}