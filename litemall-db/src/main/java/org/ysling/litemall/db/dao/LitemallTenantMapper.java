package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallTenant;
import org.ysling.litemall.db.example.LitemallTenantExample;

@Mapper
@Repository
public interface LitemallTenantMapper {
    /**
     * 租户表，逻辑分库
     */
    long countByExample(LitemallTenantExample example);

    /**
     * 租户表，逻辑分库
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallTenantExample example);

    /**
     * 租户表，逻辑分库
     */
    int deleteByExample(LitemallTenantExample example);

    /**
     * 租户表，逻辑分库
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 租户表，逻辑分库
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 租户表，逻辑分库
     */
    int insert(LitemallTenant record);

    /**
     * 租户表，逻辑分库
     */
    int insertSelective(LitemallTenant record);

    /**
     * 租户表，逻辑分库
     */
    LitemallTenant selectOneByExample(LitemallTenantExample example);

    /**
     * 租户表，逻辑分库
     */
    LitemallTenant selectOneByExampleSelective(@Param("example") LitemallTenantExample example, @Param("selective") LitemallTenant.Column ... selective);

    /**
     * 租户表，逻辑分库
     */
    List<LitemallTenant> selectByExampleSelective(@Param("example") LitemallTenantExample example, @Param("selective") LitemallTenant.Column ... selective);

    /**
     * 租户表，逻辑分库
     */
    List<LitemallTenant> selectByExample(LitemallTenantExample example);

    /**
     * 租户表，逻辑分库
     */
    LitemallTenant selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallTenant.Column ... selective);

    /**
     * 租户表，逻辑分库
     */
    LitemallTenant selectByPrimaryKey(Integer id);

    /**
     * 租户表，逻辑分库
     */
    LitemallTenant selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 租户表，逻辑分库
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallTenant record, @Param("example") LitemallTenantExample example);

    /**
     * 租户表，逻辑分库
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallTenant record, @Param("example") LitemallTenantExample example);

    /**
     * 租户表，逻辑分库
     */
    int updateByExampleSelective(@Param("record") LitemallTenant record, @Param("example") LitemallTenantExample example);

    /**
     * 租户表，逻辑分库
     */
    int updateByExample(@Param("record") LitemallTenant record, @Param("example") LitemallTenantExample example);

    /**
     * 租户表，逻辑分库
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallTenant record);

    /**
     * 租户表，逻辑分库
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallTenant record);

    /**
     * 租户表，逻辑分库
     */
    int updateByPrimaryKeySelective(LitemallTenant record);

    /**
     * 租户表，逻辑分库
     */
    int updateByPrimaryKey(LitemallTenant record);

    /**
     * 租户表，逻辑分库
     */
    int logicalDeleteByExample(@Param("example") LitemallTenantExample example);

    /**
     * 租户表，逻辑分库
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallTenantExample example);

    /**
     * 租户表，逻辑分库
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 租户表，逻辑分库
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}