package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallNotice;
import org.ysling.litemall.db.example.LitemallNoticeExample;

@Mapper
@Repository
public interface LitemallNoticeMapper {
    /**
     * 通知表
     */
    long countByExample(LitemallNoticeExample example);

    /**
     * 通知表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallNoticeExample example);

    /**
     * 通知表
     */
    int deleteByExample(LitemallNoticeExample example);

    /**
     * 通知表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 通知表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 通知表
     */
    int insert(LitemallNotice record);

    /**
     * 通知表
     */
    int insertSelective(LitemallNotice record);

    /**
     * 通知表
     */
    LitemallNotice selectOneByExample(LitemallNoticeExample example);

    /**
     * 通知表
     */
    LitemallNotice selectOneByExampleSelective(@Param("example") LitemallNoticeExample example, @Param("selective") LitemallNotice.Column ... selective);

    /**
     * 通知表
     */
    List<LitemallNotice> selectByExampleSelective(@Param("example") LitemallNoticeExample example, @Param("selective") LitemallNotice.Column ... selective);

    /**
     * 通知表
     */
    List<LitemallNotice> selectByExample(LitemallNoticeExample example);

    /**
     * 通知表
     */
    LitemallNotice selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallNotice.Column ... selective);

    /**
     * 通知表
     */
    LitemallNotice selectByPrimaryKey(Integer id);

    /**
     * 通知表
     */
    LitemallNotice selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 通知表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallNotice record, @Param("example") LitemallNoticeExample example);

    /**
     * 通知表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallNotice record, @Param("example") LitemallNoticeExample example);

    /**
     * 通知表
     */
    int updateByExampleSelective(@Param("record") LitemallNotice record, @Param("example") LitemallNoticeExample example);

    /**
     * 通知表
     */
    int updateByExample(@Param("record") LitemallNotice record, @Param("example") LitemallNoticeExample example);

    /**
     * 通知表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallNotice record);

    /**
     * 通知表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallNotice record);

    /**
     * 通知表
     */
    int updateByPrimaryKeySelective(LitemallNotice record);

    /**
     * 通知表
     */
    int updateByPrimaryKey(LitemallNotice record);

    /**
     * 通知表
     */
    int logicalDeleteByExample(@Param("example") LitemallNoticeExample example);

    /**
     * 通知表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallNoticeExample example);

    /**
     * 通知表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 通知表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}