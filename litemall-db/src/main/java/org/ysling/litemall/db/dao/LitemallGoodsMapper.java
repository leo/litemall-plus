package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallGoods;
import org.ysling.litemall.db.example.LitemallGoodsExample;

@Mapper
@Repository
public interface LitemallGoodsMapper {
    /**
     * 商品基本信息表
     */
    long countByExample(LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int deleteByExample(LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 商品基本信息表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 商品基本信息表
     */
    int insert(LitemallGoods record);

    /**
     * 商品基本信息表
     */
    int insertSelective(LitemallGoods record);

    /**
     * 商品基本信息表
     */
    LitemallGoods selectOneByExample(LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    LitemallGoods selectOneByExampleSelective(@Param("example") LitemallGoodsExample example, @Param("selective") LitemallGoods.Column ... selective);

    /**
     * 商品基本信息表
     */
    LitemallGoods selectOneByExampleWithBLOBs(LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    List<LitemallGoods> selectByExampleSelective(@Param("example") LitemallGoodsExample example, @Param("selective") LitemallGoods.Column ... selective);

    /**
     * 商品基本信息表
     */
    List<LitemallGoods> selectByExampleWithBLOBs(LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    List<LitemallGoods> selectByExample(LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    LitemallGoods selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallGoods.Column ... selective);

    /**
     * 商品基本信息表
     */
    LitemallGoods selectByPrimaryKey(Integer id);

    /**
     * 商品基本信息表
     */
    LitemallGoods selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 商品基本信息表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallGoods record, @Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallGoods record, @Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int updateByExampleSelective(@Param("record") LitemallGoods record, @Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int updateWithVersionByExampleWithBLOBs(@Param("version") Integer version, @Param("record") LitemallGoods record, @Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int updateByExampleWithBLOBs(@Param("record") LitemallGoods record, @Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int updateByExample(@Param("record") LitemallGoods record, @Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallGoods record);

    /**
     * 商品基本信息表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallGoods record);

    /**
     * 商品基本信息表
     */
    int updateByPrimaryKeySelective(LitemallGoods record);

    /**
     * 商品基本信息表
     */
    int updateWithVersionByPrimaryKeyWithBLOBs(@Param("version") Integer version, @Param("record") LitemallGoods record);

    /**
     * 商品基本信息表
     */
    int updateByPrimaryKeyWithBLOBs(LitemallGoods record);

    /**
     * 商品基本信息表
     */
    int updateByPrimaryKey(LitemallGoods record);

    /**
     * 商品基本信息表
     */
    int logicalDeleteByExample(@Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallGoodsExample example);

    /**
     * 商品基本信息表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 商品基本信息表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}