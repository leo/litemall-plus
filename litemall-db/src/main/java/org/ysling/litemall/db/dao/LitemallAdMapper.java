package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallAd;
import org.ysling.litemall.db.example.LitemallAdExample;

@Mapper
@Repository
public interface LitemallAdMapper {
    /**
     * 广告表
     */
    long countByExample(LitemallAdExample example);

    /**
     * 广告表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallAdExample example);

    /**
     * 广告表
     */
    int deleteByExample(LitemallAdExample example);

    /**
     * 广告表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 广告表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 广告表
     */
    int insert(LitemallAd record);

    /**
     * 广告表
     */
    int insertSelective(LitemallAd record);

    /**
     * 广告表
     */
    LitemallAd selectOneByExample(LitemallAdExample example);

    /**
     * 广告表
     */
    LitemallAd selectOneByExampleSelective(@Param("example") LitemallAdExample example, @Param("selective") LitemallAd.Column ... selective);

    /**
     * 广告表
     */
    List<LitemallAd> selectByExampleSelective(@Param("example") LitemallAdExample example, @Param("selective") LitemallAd.Column ... selective);

    /**
     * 广告表
     */
    List<LitemallAd> selectByExample(LitemallAdExample example);

    /**
     * 广告表
     */
    LitemallAd selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallAd.Column ... selective);

    /**
     * 广告表
     */
    LitemallAd selectByPrimaryKey(Integer id);

    /**
     * 广告表
     */
    LitemallAd selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 广告表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallAd record, @Param("example") LitemallAdExample example);

    /**
     * 广告表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallAd record, @Param("example") LitemallAdExample example);

    /**
     * 广告表
     */
    int updateByExampleSelective(@Param("record") LitemallAd record, @Param("example") LitemallAdExample example);

    /**
     * 广告表
     */
    int updateByExample(@Param("record") LitemallAd record, @Param("example") LitemallAdExample example);

    /**
     * 广告表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallAd record);

    /**
     * 广告表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallAd record);

    /**
     * 广告表
     */
    int updateByPrimaryKeySelective(LitemallAd record);

    /**
     * 广告表
     */
    int updateByPrimaryKey(LitemallAd record);

    /**
     * 广告表
     */
    int logicalDeleteByExample(@Param("example") LitemallAdExample example);

    /**
     * 广告表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallAdExample example);

    /**
     * 广告表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 广告表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}