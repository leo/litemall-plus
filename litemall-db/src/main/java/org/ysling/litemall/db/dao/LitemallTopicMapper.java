package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallTopic;
import org.ysling.litemall.db.example.LitemallTopicExample;

@Mapper
@Repository
public interface LitemallTopicMapper {
    /**
     * 专题表
     */
    long countByExample(LitemallTopicExample example);

    /**
     * 专题表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallTopicExample example);

    /**
     * 专题表
     */
    int deleteByExample(LitemallTopicExample example);

    /**
     * 专题表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 专题表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 专题表
     */
    int insert(LitemallTopic record);

    /**
     * 专题表
     */
    int insertSelective(LitemallTopic record);

    /**
     * 专题表
     */
    LitemallTopic selectOneByExample(LitemallTopicExample example);

    /**
     * 专题表
     */
    LitemallTopic selectOneByExampleSelective(@Param("example") LitemallTopicExample example, @Param("selective") LitemallTopic.Column ... selective);

    /**
     * 专题表
     */
    LitemallTopic selectOneByExampleWithBLOBs(LitemallTopicExample example);

    /**
     * 专题表
     */
    List<LitemallTopic> selectByExampleSelective(@Param("example") LitemallTopicExample example, @Param("selective") LitemallTopic.Column ... selective);

    /**
     * 专题表
     */
    List<LitemallTopic> selectByExampleWithBLOBs(LitemallTopicExample example);

    /**
     * 专题表
     */
    List<LitemallTopic> selectByExample(LitemallTopicExample example);

    /**
     * 专题表
     */
    LitemallTopic selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallTopic.Column ... selective);

    /**
     * 专题表
     */
    LitemallTopic selectByPrimaryKey(Integer id);

    /**
     * 专题表
     */
    LitemallTopic selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 专题表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallTopic record, @Param("example") LitemallTopicExample example);

    /**
     * 专题表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallTopic record, @Param("example") LitemallTopicExample example);

    /**
     * 专题表
     */
    int updateByExampleSelective(@Param("record") LitemallTopic record, @Param("example") LitemallTopicExample example);

    /**
     * 专题表
     */
    int updateWithVersionByExampleWithBLOBs(@Param("version") Integer version, @Param("record") LitemallTopic record, @Param("example") LitemallTopicExample example);

    /**
     * 专题表
     */
    int updateByExampleWithBLOBs(@Param("record") LitemallTopic record, @Param("example") LitemallTopicExample example);

    /**
     * 专题表
     */
    int updateByExample(@Param("record") LitemallTopic record, @Param("example") LitemallTopicExample example);

    /**
     * 专题表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallTopic record);

    /**
     * 专题表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallTopic record);

    /**
     * 专题表
     */
    int updateByPrimaryKeySelective(LitemallTopic record);

    /**
     * 专题表
     */
    int updateWithVersionByPrimaryKeyWithBLOBs(@Param("version") Integer version, @Param("record") LitemallTopic record);

    /**
     * 专题表
     */
    int updateByPrimaryKeyWithBLOBs(LitemallTopic record);

    /**
     * 专题表
     */
    int updateByPrimaryKey(LitemallTopic record);

    /**
     * 专题表
     */
    int logicalDeleteByExample(@Param("example") LitemallTopicExample example);

    /**
     * 专题表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallTopicExample example);

    /**
     * 专题表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 专题表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}