package org.ysling.litemall.db.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import com.github.pagehelper.PageHelper;
import org.springframework.util.StringUtils;
import org.ysling.litemall.db.dao.LitemallRoleMapper;
import org.ysling.litemall.db.domain.LitemallRole;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.ysling.litemall.db.example.LitemallRoleExample;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

@Service
@CacheConfig(cacheNames = "role")
public class LitemallRoleService {

    @Resource
    private LitemallRoleMapper roleMapper;

    @Cacheable(sync = true)
    public Set<String> queryByIds(Integer[] roleIds) {
        Set<String> roles = new HashSet<String>();
        if(roleIds.length == 0){
            return roles;
        }

        LitemallRoleExample example = new LitemallRoleExample();
        example.or().andIdIn(Arrays.asList(roleIds)).andEnabledEqualTo(true).andDeletedEqualTo(false);
        List<LitemallRole> roleList = roleMapper.selectByExample(example);

        for(LitemallRole role : roleList){
            roles.add(role.getName());
        }

        return roles;
    }

    @Cacheable(sync = true)
    public List<LitemallRole> querySelective(String name, Integer page, Integer limit, String sort, String order) {
        LitemallRoleExample example = new LitemallRoleExample();
        LitemallRoleExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);

        if (StringUtils.hasText(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        if (StringUtils.hasText(sort) && StringUtils.hasText(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, limit);
        return roleMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public LitemallRole findById(Integer id) {
        return roleMapper.selectByPrimaryKey(id);
    }

    @Cacheable(sync = true)
    public boolean checkExist(String name) {
        LitemallRoleExample example = new LitemallRoleExample();
        example.or().andNameEqualTo(name).andDeletedEqualTo(false);
        return roleMapper.countByExample(example) != 0;
    }

    @Cacheable(sync = true)
    public List<LitemallRole> queryAll() {
        LitemallRoleExample example = new LitemallRoleExample();
        example.or().andDeletedEqualTo(false);
        return roleMapper.selectByExample(example);
    }

    @CacheEvict(allEntries = true)
    public void add(LitemallRole role) {
        role.setAddTime(LocalDateTime.now());
        role.setUpdateTime(LocalDateTime.now());
        roleMapper.insertSelective(role);
    }

    @CacheEvict(allEntries = true)
    public void deleteById(Integer id) {
        roleMapper.logicalDeleteByPrimaryKey(id);
    }

    @CacheEvict(allEntries = true)
    public void updateById(LitemallRole role) {
        role.setUpdateTime(LocalDateTime.now());
        roleMapper.updateByPrimaryKeySelective(role);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallRole role) {
        role.setUpdateTime(LocalDateTime.now());
        return roleMapper.updateWithVersionByPrimaryKeySelective(role.getVersion(), role);
    }


}
