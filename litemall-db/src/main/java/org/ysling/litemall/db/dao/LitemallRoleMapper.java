package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallRole;
import org.ysling.litemall.db.example.LitemallRoleExample;

@Mapper
@Repository
public interface LitemallRoleMapper {
    /**
     * 角色表
     */
    long countByExample(LitemallRoleExample example);

    /**
     * 角色表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallRoleExample example);

    /**
     * 角色表
     */
    int deleteByExample(LitemallRoleExample example);

    /**
     * 角色表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 角色表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 角色表
     */
    int insert(LitemallRole record);

    /**
     * 角色表
     */
    int insertSelective(LitemallRole record);

    /**
     * 角色表
     */
    LitemallRole selectOneByExample(LitemallRoleExample example);

    /**
     * 角色表
     */
    LitemallRole selectOneByExampleSelective(@Param("example") LitemallRoleExample example, @Param("selective") LitemallRole.Column ... selective);

    /**
     * 角色表
     */
    List<LitemallRole> selectByExampleSelective(@Param("example") LitemallRoleExample example, @Param("selective") LitemallRole.Column ... selective);

    /**
     * 角色表
     */
    List<LitemallRole> selectByExample(LitemallRoleExample example);

    /**
     * 角色表
     */
    LitemallRole selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallRole.Column ... selective);

    /**
     * 角色表
     */
    LitemallRole selectByPrimaryKey(Integer id);

    /**
     * 角色表
     */
    LitemallRole selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 角色表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallRole record, @Param("example") LitemallRoleExample example);

    /**
     * 角色表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallRole record, @Param("example") LitemallRoleExample example);

    /**
     * 角色表
     */
    int updateByExampleSelective(@Param("record") LitemallRole record, @Param("example") LitemallRoleExample example);

    /**
     * 角色表
     */
    int updateByExample(@Param("record") LitemallRole record, @Param("example") LitemallRoleExample example);

    /**
     * 角色表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallRole record);

    /**
     * 角色表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallRole record);

    /**
     * 角色表
     */
    int updateByPrimaryKeySelective(LitemallRole record);

    /**
     * 角色表
     */
    int updateByPrimaryKey(LitemallRole record);

    /**
     * 角色表
     */
    int logicalDeleteByExample(@Param("example") LitemallRoleExample example);

    /**
     * 角色表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallRoleExample example);

    /**
     * 角色表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 角色表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}