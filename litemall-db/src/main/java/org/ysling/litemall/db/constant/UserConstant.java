package org.ysling.litemall.db.constant;

import java.io.Serializable;

public enum UserConstant implements Serializable {

    /////////////////////////////////
    //       账号状态
    ////////////////////////////////

    /**正常*/
    STATUS_NORMAL((byte)0,"正常"),
    /**禁用*/
    STATUS_DISABLED((byte)1,"禁用"),
    /**注销*/
    STATUS_OUT((byte)2,"注销"),

    /////////////////////////////////
    //       账号等级
    ////////////////////////////////

    /**普通用户*/
    USER_LEVEL_0((byte)0,"普通用户"),
    /**vip用户*/
    USER_LEVEL_1((byte)1,"vip用户"),
    /**高级vip用户*/
    USER_LEVEL_2((byte)2,"高级vip用户");

    /**状态*/
    public final Byte status;
    /**描述*/
    public final String depict;

    UserConstant(Byte status, String depict) {
        this.status = status;
        this.depict = depict;
    }

}
