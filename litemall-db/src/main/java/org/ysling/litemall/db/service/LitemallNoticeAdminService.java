package org.ysling.litemall.db.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import com.github.pagehelper.PageHelper;
import org.ysling.litemall.db.dao.LitemallNoticeAdminMapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.ysling.litemall.db.domain.LitemallKeyword;
import org.ysling.litemall.db.domain.LitemallNoticeAdmin;
import org.ysling.litemall.db.example.LitemallNoticeAdminExample;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
@CacheConfig(cacheNames = "noticeAdmin")
public class LitemallNoticeAdminService {

    @Resource
    private LitemallNoticeAdminMapper noticeAdminMapper;

    @Cacheable(sync = true)
    public List<LitemallNoticeAdmin> querySelective(String title, String type, Integer adminId, Integer page, Integer limit, String sort, String order) {
        LitemallNoticeAdminExample example = new LitemallNoticeAdminExample();
        LitemallNoticeAdminExample.Criteria criteria = example.createCriteria();

        criteria.andAdminIdEqualTo(adminId);
        criteria.andDeletedEqualTo(false);

        if(type.equals("read")){
            criteria.andReadTimeIsNotNull();
        }
        if(type.equals("unread")){
            criteria.andReadTimeIsNull();
        }
        if(StringUtils.hasText(title)){
            criteria.andNoticeTitleLike("%" + title + "%");
        }
        if (StringUtils.hasText(sort) && StringUtils.hasText(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, limit);
        return noticeAdminMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public LitemallNoticeAdmin find(Integer noticeId, Integer adminId) {
        LitemallNoticeAdminExample example = new LitemallNoticeAdminExample();
        example.or().andNoticeIdEqualTo(noticeId).andAdminIdEqualTo(adminId).andDeletedEqualTo(false);
        return noticeAdminMapper.selectOneByExample(example);
    }

    @CacheEvict(allEntries = true)
    public void add(LitemallNoticeAdmin noticeAdmin) {
        noticeAdmin.setAddTime(LocalDateTime.now());
        noticeAdmin.setUpdateTime(LocalDateTime.now());
        noticeAdminMapper.insertSelective(noticeAdmin);
    }

    @CacheEvict(allEntries = true)
    public void update(LitemallNoticeAdmin noticeAdmin) {
        noticeAdmin.setUpdateTime(LocalDateTime.now());
        noticeAdminMapper.updateByPrimaryKeySelective(noticeAdmin);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallNoticeAdmin noticeAdmin) {
        noticeAdmin.setUpdateTime(LocalDateTime.now());
        return noticeAdminMapper.updateWithVersionByPrimaryKeySelective(noticeAdmin.getVersion(), noticeAdmin);
    }

    @CacheEvict(allEntries = true)
    public void markReadByIds(List<Integer> ids, Integer adminId) {
        LitemallNoticeAdminExample example = new LitemallNoticeAdminExample();
        example.or().andIdIn(ids).andAdminIdEqualTo(adminId).andDeletedEqualTo(false);
        LitemallNoticeAdmin noticeAdmin = new LitemallNoticeAdmin();
        LocalDateTime now = LocalDateTime.now();
        noticeAdmin.setReadTime(now);
        noticeAdmin.setUpdateTime(now);
        noticeAdminMapper.updateByExampleSelective(noticeAdmin, example);
    }

    @CacheEvict(allEntries = true)
    public void deleteById(Integer id, Integer adminId) {
        LitemallNoticeAdminExample example = new LitemallNoticeAdminExample();
        example.or().andIdEqualTo(id).andAdminIdEqualTo(adminId).andDeletedEqualTo(false);
        LitemallNoticeAdmin noticeAdmin = new LitemallNoticeAdmin();
        noticeAdmin.setUpdateTime(LocalDateTime.now());
        noticeAdmin.setDeleted(true);
        noticeAdminMapper.updateByExampleSelective(noticeAdmin, example);
    }

    @CacheEvict(allEntries = true)
    public void deleteByIds(List<Integer> ids, Integer adminId) {
        LitemallNoticeAdminExample example = new LitemallNoticeAdminExample();
        example.or().andIdIn(ids).andAdminIdEqualTo(adminId).andDeletedEqualTo(false);
        LitemallNoticeAdmin noticeAdmin = new LitemallNoticeAdmin();
        noticeAdmin.setUpdateTime(LocalDateTime.now());
        noticeAdmin.setDeleted(true);
        noticeAdminMapper.updateByExampleSelective(noticeAdmin, example);
    }

    @Cacheable(sync = true)
    public int countUnread(Integer adminId) {
        LitemallNoticeAdminExample example = new LitemallNoticeAdminExample();
        example.or().andAdminIdEqualTo(adminId).andReadTimeIsNull().andDeletedEqualTo(false);
        return (int)noticeAdminMapper.countByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallNoticeAdmin> queryByNoticeId(Integer noticeId) {
        LitemallNoticeAdminExample example = new LitemallNoticeAdminExample();
        example.or().andNoticeIdEqualTo(noticeId).andDeletedEqualTo(false);
        return noticeAdminMapper.selectByExample(example);
    }

    @CacheEvict(allEntries = true)
    public void deleteByNoticeId(Integer id) {
        LitemallNoticeAdminExample example = new LitemallNoticeAdminExample();
        example.or().andNoticeIdEqualTo(id).andDeletedEqualTo(false);
        LitemallNoticeAdmin noticeAdmin = new LitemallNoticeAdmin();
        noticeAdmin.setUpdateTime(LocalDateTime.now());
        noticeAdmin.setDeleted(true);
        noticeAdminMapper.updateByExampleSelective(noticeAdmin, example);
    }

    @CacheEvict(allEntries = true)
    public void deleteByNoticeIds(List<Integer> ids) {
        LitemallNoticeAdminExample example = new LitemallNoticeAdminExample();
        example.or().andNoticeIdIn(ids).andDeletedEqualTo(false);
        LitemallNoticeAdmin noticeAdmin = new LitemallNoticeAdmin();
        noticeAdmin.setUpdateTime(LocalDateTime.now());
        noticeAdmin.setDeleted(true);
        noticeAdminMapper.updateByExampleSelective(noticeAdmin, example);
    }

    @Cacheable(sync = true)
    public int countReadByNoticeId(Integer noticeId) {
        LitemallNoticeAdminExample example = new LitemallNoticeAdminExample();
        example.or().andNoticeIdEqualTo(noticeId).andReadTimeIsNotNull().andDeletedEqualTo(false);
        return (int)noticeAdminMapper.countByExample(example);
    }

    @CacheEvict(allEntries = true)
    public void updateByNoticeId(LitemallNoticeAdmin noticeAdmin, Integer noticeId) {
        LitemallNoticeAdminExample example = new LitemallNoticeAdminExample();
        example.or().andNoticeIdEqualTo(noticeId).andDeletedEqualTo(false);
        noticeAdmin.setUpdateTime(LocalDateTime.now());
        noticeAdminMapper.updateByExampleSelective(noticeAdmin, example);
    }
}
