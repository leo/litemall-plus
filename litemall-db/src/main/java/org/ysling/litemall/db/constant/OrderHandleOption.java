package org.ysling.litemall.db.constant;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import java.io.Serializable;

/**
 * 用户可执行操作类
 */
public class OrderHandleOption implements Serializable {
    private boolean cancel = false;     // 取消操作
    private boolean delete = false;     // 删除操作
    private boolean pay = false;        // 支付操作
    private boolean comment = false;    // 评论操作
    private boolean confirm = false;    // 确认收货操作
    private boolean refund = false;     // 取消订单并退款操作
    private boolean rebuy = false;      // 再次购买
    private boolean aftersale = false;  // 售后操作

    /**取消操作*/
    public boolean isCancel() {
        return cancel;
    }

    /**取消操作*/
    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }

    /**删除操作*/
    public boolean isDelete() {
        return delete;
    }

    /**删除操作*/
    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    /**支付操作*/
    public boolean isPay() {
        return pay;
    }

    /**支付操作*/
    public void setPay(boolean pay) {
        this.pay = pay;
    }

    /**评论操作*/
    public boolean isComment() {
        return comment;
    }

    /**评论操作*/
    public void setComment(boolean comment) {
        this.comment = comment;
    }

    /**确认收货操作*/
    public boolean isConfirm() {
        return confirm;
    }

    /**确认收货操作*/
    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    /**取消订单并退款操作*/
    public boolean isRefund() {
        return refund;
    }

    /**取消订单并退款操作*/
    public void setRefund(boolean refund) {
        this.refund = refund;
    }

    /**再次购买*/
    public boolean isRebuy() {
        return rebuy;
    }

    /**再次购买*/
    public void setRebuy(boolean rebuy) {
        this.rebuy = rebuy;
    }

    /**售后操作*/
    public boolean isAftersale() {
        return aftersale;
    }

    /**售后操作*/
    public void setAftersale(boolean aftersale) {
        this.aftersale = aftersale;
    }
}
