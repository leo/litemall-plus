package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_qrcode
 * Database Table Remarks : 
 *   二维码生成表
 * @author ysling
 */
public class LitemallQrcode implements Serializable {
    /**
     * litemall_qrcode
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_qrcode
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 二维码生成表ID
     */
    private Integer id;

    /**
     * 桌号
     */
    private String tableNo;

    /**
     * 二维码大小
     */
    private Integer imgSize;

    /**
     * 协议
     */
    private String httpTcp;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * 桌面二维码
     */
    private byte[] picBlob;

    /**
     * litemall_qrcode
     */
    private static final long serialVersionUID = 1L;

    /**
     * 二维码生成表ID
     * @return id 二维码生成表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 二维码生成表ID
     * @param id 二维码生成表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 桌号
     * @return table_no 桌号
     */
    public String getTableNo() {
        return tableNo;
    }

    /**
     * 桌号
     * @param tableNo 桌号
     */
    public void setTableNo(String tableNo) {
        this.tableNo = tableNo;
    }

    /**
     * 二维码大小
     * @return img_size 二维码大小
     */
    public Integer getImgSize() {
        return imgSize;
    }

    /**
     * 二维码大小
     * @param imgSize 二维码大小
     */
    public void setImgSize(Integer imgSize) {
        this.imgSize = imgSize;
    }

    /**
     * 协议
     * @return http_tcp 协议
     */
    public String getHttpTcp() {
        return httpTcp;
    }

    /**
     * 协议
     * @param httpTcp 协议
     */
    public void setHttpTcp(String httpTcp) {
        this.httpTcp = httpTcp;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 二维码生成表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 桌面二维码
     * @return pic_blob 桌面二维码
     */
    public byte[] getPicBlob() {
        return picBlob;
    }

    /**
     * 桌面二维码
     * @param picBlob 桌面二维码
     */
    public void setPicBlob(byte[] picBlob) {
        this.picBlob = picBlob;
    }

    /**
     * 二维码生成表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", tableNo=").append(tableNo);
        sb.append(", imgSize=").append(imgSize);
        sb.append(", httpTcp=").append(httpTcp);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append(", picBlob=").append(picBlob);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 二维码生成表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallQrcode other = (LitemallQrcode) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTableNo() == null ? other.getTableNo() == null : this.getTableNo().equals(other.getTableNo()))
            && (this.getImgSize() == null ? other.getImgSize() == null : this.getImgSize().equals(other.getImgSize()))
            && (this.getHttpTcp() == null ? other.getHttpTcp() == null : this.getHttpTcp().equals(other.getHttpTcp()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
            && (Arrays.equals(this.getPicBlob(), other.getPicBlob()));
    }

    /**
     * 二维码生成表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTableNo() == null) ? 0 : getTableNo().hashCode());
        result = prime * result + ((getImgSize() == null) ? 0 : getImgSize().hashCode());
        result = prime * result + ((getHttpTcp() == null) ? 0 : getHttpTcp().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + (Arrays.hashCode(getPicBlob()));
        return result;
    }

    /**
     * litemall_qrcode
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_qrcode
         */
        private final Boolean value;

        /**
         * litemall_qrcode
         */
        private final String name;

        /**
         * 二维码生成表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 二维码生成表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 二维码生成表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 二维码生成表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 二维码生成表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 二维码生成表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_qrcode
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        tableNo("table_no", "tableNo", "VARCHAR", false),
        imgSize("img_size", "imgSize", "INTEGER", false),
        httpTcp("http_tcp", "httpTcp", "VARCHAR", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false),
        picBlob("pic_blob", "picBlob", "LONGVARBINARY", false);

        /**
         * litemall_qrcode
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_qrcode
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_qrcode
         */
        private final String column;

        /**
         * litemall_qrcode
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_qrcode
         */
        private final String javaProperty;

        /**
         * litemall_qrcode
         */
        private final String jdbcType;

        /**
         * 二维码生成表
         */
        public String value() {
            return this.column;
        }

        /**
         * 二维码生成表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 二维码生成表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 二维码生成表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 二维码生成表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 二维码生成表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 二维码生成表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 二维码生成表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 二维码生成表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 二维码生成表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 二维码生成表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}