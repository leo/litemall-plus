package org.ysling.litemall.db.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import org.ysling.litemall.db.dao.GoodsProductMapper;
import org.ysling.litemall.db.dao.LitemallGoodsProductMapper;
import org.ysling.litemall.db.domain.LitemallGoodsAttribute;
import org.ysling.litemall.db.domain.LitemallGoodsProduct;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.ysling.litemall.db.example.LitemallGoodsProductExample;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
@CacheConfig(cacheNames = "goodsProduct")
public class LitemallGoodsProductService {

    @Resource
    private GoodsProductMapper goodsProductMapper;
    @Resource
    private LitemallGoodsProductMapper litemallGoodsProductMapper;


    @Cacheable(sync = true)
    public List<LitemallGoodsProduct> queryByGid(Integer gid) {
        LitemallGoodsProductExample example = new LitemallGoodsProductExample();
        example.or().andGoodsIdEqualTo(gid).andDeletedEqualTo(false);
        return litemallGoodsProductMapper.selectByExample(example);
    }

    /**
     * 判断商品是否库存不足
     * @param gid
     * @return
     */
    @Cacheable(sync = true)
    public Boolean isGoodsNOStock(Integer gid) {
        LitemallGoodsProductExample example = new LitemallGoodsProductExample();
        example.or().andGoodsIdEqualTo(gid).andDeletedEqualTo(false);
        List<LitemallGoodsProduct> goodsProducts = litemallGoodsProductMapper.selectByExample(example);
        Integer number = 0;
        for (LitemallGoodsProduct product :goodsProducts) {
            number += product.getNumber();
        }
        return number <= 0;
    }

    @Cacheable(sync = true)
    public LitemallGoodsProduct findById(Integer id) {
        return litemallGoodsProductMapper.selectByPrimaryKey(id);
    }

    @Cacheable(sync = true)
    public int count() {
        LitemallGoodsProductExample example = new LitemallGoodsProductExample();
        example.or().andDeletedEqualTo(false);
        return (int) litemallGoodsProductMapper.countByExample(example);
    }

    @CacheEvict(allEntries = true)
    public void deleteById(Integer id) {
        litemallGoodsProductMapper.logicalDeleteByPrimaryKey(id);
    }

    @CacheEvict(allEntries = true)
    public void add(LitemallGoodsProduct goodsProduct) {
        goodsProduct.setAddTime(LocalDateTime.now());
        goodsProduct.setUpdateTime(LocalDateTime.now());
        litemallGoodsProductMapper.insertSelective(goodsProduct);
    }

    @CacheEvict(allEntries = true)
    public void deleteByGid(Integer gid) {
        LitemallGoodsProductExample example = new LitemallGoodsProductExample();
        example.or().andGoodsIdEqualTo(gid);
        litemallGoodsProductMapper.logicalDeleteByExample(example);
    }

    @CacheEvict(allEntries = true)
    public int addStock(Integer id, Short num){
        return goodsProductMapper.addStock(id, num);
    }

    @CacheEvict(allEntries = true)
    public int reduceStock(Integer id, Short num){
        return goodsProductMapper.reduceStock(id, num);
    }

    @CacheEvict(allEntries = true)
    public void updateById(LitemallGoodsProduct product) {
        product.setUpdateTime(LocalDateTime.now());
        litemallGoodsProductMapper.updateByPrimaryKeySelective(product);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallGoodsProduct product) {
        product.setUpdateTime(LocalDateTime.now());
        return litemallGoodsProductMapper.updateWithVersionByPrimaryKeySelective(product.getVersion(), product);
    }

}