package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_region
 * Database Table Remarks : 
 *   行政区域表
 * @author ysling
 */
public class LitemallRegion implements Serializable {
    /**
     * 行政区域表ID
     */
    private Integer id;

    /**
     * 行政区域父ID，例如区县的pid指向市，市的pid指向省，省的pid则是0
     */
    private Integer pid;

    /**
     * 行政区域名称
     */
    private String name;

    /**
     * 行政区域类型，如如1则是省， 如果是2则是市，如果是3则是区县
     */
    private Byte type;

    /**
     * 行政区域编码
     */
    private Integer code;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_region
     */
    private static final long serialVersionUID = 1L;

    /**
     * 行政区域表ID
     * @return id 行政区域表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 行政区域表ID
     * @param id 行政区域表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 行政区域父ID，例如区县的pid指向市，市的pid指向省，省的pid则是0
     * @return pid 行政区域父ID，例如区县的pid指向市，市的pid指向省，省的pid则是0
     */
    public Integer getPid() {
        return pid;
    }

    /**
     * 行政区域父ID，例如区县的pid指向市，市的pid指向省，省的pid则是0
     * @param pid 行政区域父ID，例如区县的pid指向市，市的pid指向省，省的pid则是0
     */
    public void setPid(Integer pid) {
        this.pid = pid;
    }

    /**
     * 行政区域名称
     * @return name 行政区域名称
     */
    public String getName() {
        return name;
    }

    /**
     * 行政区域名称
     * @param name 行政区域名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 行政区域类型，如如1则是省， 如果是2则是市，如果是3则是区县
     * @return type 行政区域类型，如如1则是省， 如果是2则是市，如果是3则是区县
     */
    public Byte getType() {
        return type;
    }

    /**
     * 行政区域类型，如如1则是省， 如果是2则是市，如果是3则是区县
     * @param type 行政区域类型，如如1则是省， 如果是2则是市，如果是3则是区县
     */
    public void setType(Byte type) {
        this.type = type;
    }

    /**
     * 行政区域编码
     * @return code 行政区域编码
     */
    public Integer getCode() {
        return code;
    }

    /**
     * 行政区域编码
     * @param code 行政区域编码
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 行政区域表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", pid=").append(pid);
        sb.append(", name=").append(name);
        sb.append(", type=").append(type);
        sb.append(", code=").append(code);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 行政区域表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallRegion other = (LitemallRegion) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getPid() == null ? other.getPid() == null : this.getPid().equals(other.getPid()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * 行政区域表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getPid() == null) ? 0 : getPid().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_region
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        pid("pid", "pid", "INTEGER", false),
        name("name", "name", "VARCHAR", true),
        type("type", "type", "TINYINT", true),
        code("code", "code", "INTEGER", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_region
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_region
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_region
         */
        private final String column;

        /**
         * litemall_region
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_region
         */
        private final String javaProperty;

        /**
         * litemall_region
         */
        private final String jdbcType;

        /**
         * 行政区域表
         */
        public String value() {
            return this.column;
        }

        /**
         * 行政区域表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 行政区域表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 行政区域表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 行政区域表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 行政区域表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 行政区域表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 行政区域表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 行政区域表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 行政区域表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 行政区域表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}