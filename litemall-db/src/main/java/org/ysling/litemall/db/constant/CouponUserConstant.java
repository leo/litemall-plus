package org.ysling.litemall.db.constant;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import java.io.Serializable;

public class CouponUserConstant implements Serializable {
    /**未使用*/
    public static final Short STATUS_USABLE = 0;
    /**已使用*/
    public static final Short STATUS_USED = 1;
    /**已过期*/
    public static final Short STATUS_EXPIRED = 2;
    /**已下架*/
    public static final Short STATUS_OUT = 3;
}
