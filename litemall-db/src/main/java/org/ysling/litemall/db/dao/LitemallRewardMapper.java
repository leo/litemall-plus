package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallReward;
import org.ysling.litemall.db.example.LitemallRewardExample;

@Mapper
@Repository
public interface LitemallRewardMapper {
    /**
     * 赏金任务参与表
     */
    long countByExample(LitemallRewardExample example);

    /**
     * 赏金任务参与表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallRewardExample example);

    /**
     * 赏金任务参与表
     */
    int deleteByExample(LitemallRewardExample example);

    /**
     * 赏金任务参与表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 赏金任务参与表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 赏金任务参与表
     */
    int insert(LitemallReward record);

    /**
     * 赏金任务参与表
     */
    int insertSelective(LitemallReward record);

    /**
     * 赏金任务参与表
     */
    LitemallReward selectOneByExample(LitemallRewardExample example);

    /**
     * 赏金任务参与表
     */
    LitemallReward selectOneByExampleSelective(@Param("example") LitemallRewardExample example, @Param("selective") LitemallReward.Column ... selective);

    /**
     * 赏金任务参与表
     */
    List<LitemallReward> selectByExampleSelective(@Param("example") LitemallRewardExample example, @Param("selective") LitemallReward.Column ... selective);

    /**
     * 赏金任务参与表
     */
    List<LitemallReward> selectByExample(LitemallRewardExample example);

    /**
     * 赏金任务参与表
     */
    LitemallReward selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallReward.Column ... selective);

    /**
     * 赏金任务参与表
     */
    LitemallReward selectByPrimaryKey(Integer id);

    /**
     * 赏金任务参与表
     */
    LitemallReward selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 赏金任务参与表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallReward record, @Param("example") LitemallRewardExample example);

    /**
     * 赏金任务参与表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallReward record, @Param("example") LitemallRewardExample example);

    /**
     * 赏金任务参与表
     */
    int updateByExampleSelective(@Param("record") LitemallReward record, @Param("example") LitemallRewardExample example);

    /**
     * 赏金任务参与表
     */
    int updateByExample(@Param("record") LitemallReward record, @Param("example") LitemallRewardExample example);

    /**
     * 赏金任务参与表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallReward record);

    /**
     * 赏金任务参与表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallReward record);

    /**
     * 赏金任务参与表
     */
    int updateByPrimaryKeySelective(LitemallReward record);

    /**
     * 赏金任务参与表
     */
    int updateByPrimaryKey(LitemallReward record);

    /**
     * 赏金任务参与表
     */
    int logicalDeleteByExample(@Param("example") LitemallRewardExample example);

    /**
     * 赏金任务参与表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallRewardExample example);

    /**
     * 赏金任务参与表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 赏金任务参与表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}