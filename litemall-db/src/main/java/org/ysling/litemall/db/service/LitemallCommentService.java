package org.ysling.litemall.db.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import com.github.pagehelper.PageHelper;
import org.ysling.litemall.db.dao.LitemallCommentMapper;
import org.ysling.litemall.db.domain.LitemallCategory;
import org.ysling.litemall.db.domain.LitemallComment;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.ysling.litemall.db.example.LitemallCommentExample;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
@CacheConfig(cacheNames = "comment")
public class LitemallCommentService {

    @Resource
    private LitemallCommentMapper commentMapper;

    @Cacheable(sync = true)
    public List<LitemallComment> queryGoodsByGid(Integer id, int offset, int limit) {
        LitemallCommentExample example = new LitemallCommentExample();
        example.setOrderByClause(LitemallComment.Column.addTime.desc());
        example.or().andValueIdEqualTo(id).andTypeEqualTo((byte) 0).andDeletedEqualTo(false);
        PageHelper.startPage(offset, limit);
        return commentMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallComment> query(Byte type, Integer valueId, Integer showType, Integer offset, Integer limit) {
        LitemallCommentExample example = new LitemallCommentExample();
        example.setOrderByClause(LitemallComment.Column.addTime.desc());
        if (showType == 0) {
            example.or().andValueIdEqualTo(valueId).andTypeEqualTo(type).andDeletedEqualTo(false);
        } else if (showType == 1) {
            example.or().andValueIdEqualTo(valueId).andTypeEqualTo(type).andHasPictureEqualTo(true).andDeletedEqualTo(false);
        } else {
            throw new RuntimeException("showType不支持");
        }
        PageHelper.startPage(offset, limit);
        return commentMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public int count(Byte type, Integer valueId, Integer showType) {
        LitemallCommentExample example = new LitemallCommentExample();
        if (showType == 0) {
            example.or().andValueIdEqualTo(valueId).andTypeEqualTo(type).andDeletedEqualTo(false);
        } else if (showType == 1) {
            example.or().andValueIdEqualTo(valueId).andTypeEqualTo(type).andHasPictureEqualTo(true).andDeletedEqualTo(false);
        } else {
            throw new RuntimeException("showType不支持");
        }
        return (int) commentMapper.countByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallComment> queryCommentList(Integer valueId, Byte type, int offset, int limit) {
        LitemallCommentExample example = new LitemallCommentExample();
        example.setOrderByClause(LitemallComment.Column.addTime.desc());
        example.or().andValueIdEqualTo(valueId).andTypeEqualTo(type).andDeletedEqualTo(false);
        PageHelper.startPage(offset, limit);
        return commentMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public int starCount(Byte type, Integer valueId, Short star) {
        LitemallCommentExample example = new LitemallCommentExample();
        example.or().andValueIdEqualTo(valueId).andTypeEqualTo(type).andStarEqualTo(star).andDeletedEqualTo(false);
        return (int) commentMapper.countByExample(example);
    }

    @CacheEvict(allEntries = true)
    public int save(LitemallComment comment) {
        comment.setAddTime(LocalDateTime.now());
        comment.setUpdateTime(LocalDateTime.now());
        return commentMapper.insertSelective(comment);
    }

    @Cacheable(sync = true)
    public List<LitemallComment> querySelective(String userId, String valueId, Integer page, Integer size, String sort, String order) {
        LitemallCommentExample example = new LitemallCommentExample();
        LitemallCommentExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);
        // type=2 是订单商品回复，这里过滤
        criteria.andTypeNotEqualTo((byte) 2);

        if (StringUtils.hasText(userId)) {
            criteria.andUserIdEqualTo(Integer.valueOf(userId));
        }
        if (StringUtils.hasText(valueId)) {
            criteria.andValueIdEqualTo(Integer.valueOf(valueId)).andTypeEqualTo((byte) 0);
        }
        if (StringUtils.hasText(sort) && StringUtils.hasText(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return commentMapper.selectByExample(example);
    }

    @CacheEvict(allEntries = true)
    public void deleteById(Integer id) {
        commentMapper.logicalDeleteByPrimaryKey(id);
    }

    @Cacheable(sync = true)
    public LitemallComment findById(Integer id) {
        return commentMapper.selectByPrimaryKey(id);
    }

    @CacheEvict(allEntries = true)
    public int updateById(LitemallComment comment) {
        return commentMapper.updateByPrimaryKeySelective(comment);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallComment comment) {
        comment.setUpdateTime(LocalDateTime.now());
        return commentMapper.updateWithVersionByPrimaryKeySelective(comment.getVersion(), comment);
    }
}
