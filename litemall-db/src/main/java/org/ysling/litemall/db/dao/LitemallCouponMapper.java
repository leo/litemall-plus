package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallCoupon;
import org.ysling.litemall.db.example.LitemallCouponExample;

@Mapper
@Repository
public interface LitemallCouponMapper {
    /**
     * 优惠券信息及规则表
     */
    long countByExample(LitemallCouponExample example);

    /**
     * 优惠券信息及规则表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallCouponExample example);

    /**
     * 优惠券信息及规则表
     */
    int deleteByExample(LitemallCouponExample example);

    /**
     * 优惠券信息及规则表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 优惠券信息及规则表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 优惠券信息及规则表
     */
    int insert(LitemallCoupon record);

    /**
     * 优惠券信息及规则表
     */
    int insertSelective(LitemallCoupon record);

    /**
     * 优惠券信息及规则表
     */
    LitemallCoupon selectOneByExample(LitemallCouponExample example);

    /**
     * 优惠券信息及规则表
     */
    LitemallCoupon selectOneByExampleSelective(@Param("example") LitemallCouponExample example, @Param("selective") LitemallCoupon.Column ... selective);

    /**
     * 优惠券信息及规则表
     */
    List<LitemallCoupon> selectByExampleSelective(@Param("example") LitemallCouponExample example, @Param("selective") LitemallCoupon.Column ... selective);

    /**
     * 优惠券信息及规则表
     */
    List<LitemallCoupon> selectByExample(LitemallCouponExample example);

    /**
     * 优惠券信息及规则表
     */
    LitemallCoupon selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallCoupon.Column ... selective);

    /**
     * 优惠券信息及规则表
     */
    LitemallCoupon selectByPrimaryKey(Integer id);

    /**
     * 优惠券信息及规则表
     */
    LitemallCoupon selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 优惠券信息及规则表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallCoupon record, @Param("example") LitemallCouponExample example);

    /**
     * 优惠券信息及规则表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallCoupon record, @Param("example") LitemallCouponExample example);

    /**
     * 优惠券信息及规则表
     */
    int updateByExampleSelective(@Param("record") LitemallCoupon record, @Param("example") LitemallCouponExample example);

    /**
     * 优惠券信息及规则表
     */
    int updateByExample(@Param("record") LitemallCoupon record, @Param("example") LitemallCouponExample example);

    /**
     * 优惠券信息及规则表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallCoupon record);

    /**
     * 优惠券信息及规则表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallCoupon record);

    /**
     * 优惠券信息及规则表
     */
    int updateByPrimaryKeySelective(LitemallCoupon record);

    /**
     * 优惠券信息及规则表
     */
    int updateByPrimaryKey(LitemallCoupon record);

    /**
     * 优惠券信息及规则表
     */
    int logicalDeleteByExample(@Param("example") LitemallCouponExample example);

    /**
     * 优惠券信息及规则表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallCouponExample example);

    /**
     * 优惠券信息及规则表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 优惠券信息及规则表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}