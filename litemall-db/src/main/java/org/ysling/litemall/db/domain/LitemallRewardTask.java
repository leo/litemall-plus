package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_reward_task
 * Database Table Remarks : 
 *   赏金任务规则表
 * @author ysling
 */
public class LitemallRewardTask implements Serializable {
    /**
     * litemall_reward_task
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_reward_task
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 赏金任务规则表ID
     */
    private Integer id;

    /**
     * 商品表的商品ID
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品价格
     */
    private BigDecimal goodsPrice;

    /**
     * 商品图片或者商品货品图片
     */
    private String picUrl;

    /**
     * 奖励金额
     */
    private BigDecimal award;

    /**
     * 参与者数量
     */
    private Integer joinersMember;

    /**
     * 活动数量
     */
    private Integer rewardMember;

    /**
     * 赏金任务状态，0正常，1已完成，2已取消
     */
    private Short status;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_reward_task
     */
    private static final long serialVersionUID = 1L;

    /**
     * 赏金任务规则表ID
     * @return id 赏金任务规则表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 赏金任务规则表ID
     * @param id 赏金任务规则表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 商品表的商品ID
     * @return goods_id 商品表的商品ID
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * 商品表的商品ID
     * @param goodsId 商品表的商品ID
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 商品名称
     * @return goods_name 商品名称
     */
    public String getGoodsName() {
        return goodsName;
    }

    /**
     * 商品名称
     * @param goodsName 商品名称
     */
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    /**
     * 商品价格
     * @return goods_price 商品价格
     */
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    /**
     * 商品价格
     * @param goodsPrice 商品价格
     */
    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    /**
     * 商品图片或者商品货品图片
     * @return pic_url 商品图片或者商品货品图片
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 商品图片或者商品货品图片
     * @param picUrl 商品图片或者商品货品图片
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * 奖励金额
     * @return award 奖励金额
     */
    public BigDecimal getAward() {
        return award;
    }

    /**
     * 奖励金额
     * @param award 奖励金额
     */
    public void setAward(BigDecimal award) {
        this.award = award;
    }

    /**
     * 参与者数量
     * @return joiners_member 参与者数量
     */
    public Integer getJoinersMember() {
        return joinersMember;
    }

    /**
     * 参与者数量
     * @param joinersMember 参与者数量
     */
    public void setJoinersMember(Integer joinersMember) {
        this.joinersMember = joinersMember;
    }

    /**
     * 活动数量
     * @return reward_member 活动数量
     */
    public Integer getRewardMember() {
        return rewardMember;
    }

    /**
     * 活动数量
     * @param rewardMember 活动数量
     */
    public void setRewardMember(Integer rewardMember) {
        this.rewardMember = rewardMember;
    }

    /**
     * 赏金任务状态，0正常，1已完成，2已取消
     * @return status 赏金任务状态，0正常，1已完成，2已取消
     */
    public Short getStatus() {
        return status;
    }

    /**
     * 赏金任务状态，0正常，1已完成，2已取消
     * @param status 赏金任务状态，0正常，1已完成，2已取消
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 赏金任务规则表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 赏金任务规则表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", goodsName=").append(goodsName);
        sb.append(", goodsPrice=").append(goodsPrice);
        sb.append(", picUrl=").append(picUrl);
        sb.append(", award=").append(award);
        sb.append(", joinersMember=").append(joinersMember);
        sb.append(", rewardMember=").append(rewardMember);
        sb.append(", status=").append(status);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 赏金任务规则表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallRewardTask other = (LitemallRewardTask) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getGoodsId() == null ? other.getGoodsId() == null : this.getGoodsId().equals(other.getGoodsId()))
            && (this.getGoodsName() == null ? other.getGoodsName() == null : this.getGoodsName().equals(other.getGoodsName()))
            && (this.getGoodsPrice() == null ? other.getGoodsPrice() == null : this.getGoodsPrice().equals(other.getGoodsPrice()))
            && (this.getPicUrl() == null ? other.getPicUrl() == null : this.getPicUrl().equals(other.getPicUrl()))
            && (this.getAward() == null ? other.getAward() == null : this.getAward().equals(other.getAward()))
            && (this.getJoinersMember() == null ? other.getJoinersMember() == null : this.getJoinersMember().equals(other.getJoinersMember()))
            && (this.getRewardMember() == null ? other.getRewardMember() == null : this.getRewardMember().equals(other.getRewardMember()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * 赏金任务规则表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getGoodsId() == null) ? 0 : getGoodsId().hashCode());
        result = prime * result + ((getGoodsName() == null) ? 0 : getGoodsName().hashCode());
        result = prime * result + ((getGoodsPrice() == null) ? 0 : getGoodsPrice().hashCode());
        result = prime * result + ((getPicUrl() == null) ? 0 : getPicUrl().hashCode());
        result = prime * result + ((getAward() == null) ? 0 : getAward().hashCode());
        result = prime * result + ((getJoinersMember() == null) ? 0 : getJoinersMember().hashCode());
        result = prime * result + ((getRewardMember() == null) ? 0 : getRewardMember().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_reward_task
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_reward_task
         */
        private final Boolean value;

        /**
         * litemall_reward_task
         */
        private final String name;

        /**
         * 赏金任务规则表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 赏金任务规则表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 赏金任务规则表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 赏金任务规则表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 赏金任务规则表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 赏金任务规则表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_reward_task
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        goodsId("goods_id", "goodsId", "INTEGER", false),
        goodsName("goods_name", "goodsName", "VARCHAR", false),
        goodsPrice("goods_price", "goodsPrice", "DECIMAL", false),
        picUrl("pic_url", "picUrl", "VARCHAR", false),
        award("award", "award", "DECIMAL", false),
        joinersMember("joiners_member", "joinersMember", "INTEGER", false),
        rewardMember("reward_member", "rewardMember", "INTEGER", false),
        status("status", "status", "SMALLINT", true),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_reward_task
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_reward_task
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_reward_task
         */
        private final String column;

        /**
         * litemall_reward_task
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_reward_task
         */
        private final String javaProperty;

        /**
         * litemall_reward_task
         */
        private final String jdbcType;

        /**
         * 赏金任务规则表
         */
        public String value() {
            return this.column;
        }

        /**
         * 赏金任务规则表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 赏金任务规则表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 赏金任务规则表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 赏金任务规则表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 赏金任务规则表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 赏金任务规则表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 赏金任务规则表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 赏金任务规则表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 赏金任务规则表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 赏金任务规则表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}