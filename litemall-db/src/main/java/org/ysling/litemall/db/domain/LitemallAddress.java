package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_address
 * Database Table Remarks : 
 *   收货地址表
 * @author ysling
 */
public class LitemallAddress implements Serializable {
    /**
     * litemall_address
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_address
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 收货地址表ID
     */
    private Integer id;

    /**
     * 收货人名称
     */
    private String name;

    /**
     * 用户表的用户ID
     */
    private Integer userId;

    /**
     * 行政区域表的省ID
     */
    private String province;

    /**
     * 行政区域表的市ID
     */
    private String city;

    /**
     * 行政区域表的区县ID
     */
    private String county;

    /**
     * 详细收货地址
     */
    private String addressDetail;

    /**
     * 地区编码
     */
    private String areaCode;

    /**
     * 邮政编码
     */
    private String postalCode;

    /**
     * 手机号码
     */
    private String tel;

    /**
     * 是否默认地址
     */
    private Boolean isDefault;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_address
     */
    private static final long serialVersionUID = 1L;

    /**
     * 收货地址表ID
     * @return id 收货地址表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 收货地址表ID
     * @param id 收货地址表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 收货人名称
     * @return name 收货人名称
     */
    public String getName() {
        return name;
    }

    /**
     * 收货人名称
     * @param name 收货人名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 用户表的用户ID
     * @return user_id 用户表的用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 用户表的用户ID
     * @param userId 用户表的用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 行政区域表的省ID
     * @return province 行政区域表的省ID
     */
    public String getProvince() {
        return province;
    }

    /**
     * 行政区域表的省ID
     * @param province 行政区域表的省ID
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * 行政区域表的市ID
     * @return city 行政区域表的市ID
     */
    public String getCity() {
        return city;
    }

    /**
     * 行政区域表的市ID
     * @param city 行政区域表的市ID
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * 行政区域表的区县ID
     * @return county 行政区域表的区县ID
     */
    public String getCounty() {
        return county;
    }

    /**
     * 行政区域表的区县ID
     * @param county 行政区域表的区县ID
     */
    public void setCounty(String county) {
        this.county = county;
    }

    /**
     * 详细收货地址
     * @return address_detail 详细收货地址
     */
    public String getAddressDetail() {
        return addressDetail;
    }

    /**
     * 详细收货地址
     * @param addressDetail 详细收货地址
     */
    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    /**
     * 地区编码
     * @return area_code 地区编码
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * 地区编码
     * @param areaCode 地区编码
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * 邮政编码
     * @return postal_code 邮政编码
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * 邮政编码
     * @param postalCode 邮政编码
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * 手机号码
     * @return tel 手机号码
     */
    public String getTel() {
        return tel;
    }

    /**
     * 手机号码
     * @param tel 手机号码
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 是否默认地址
     * @return is_default 是否默认地址
     */
    public Boolean getIsDefault() {
        return isDefault;
    }

    /**
     * 是否默认地址
     * @param isDefault 是否默认地址
     */
    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 收货地址表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 收货地址表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", userId=").append(userId);
        sb.append(", province=").append(province);
        sb.append(", city=").append(city);
        sb.append(", county=").append(county);
        sb.append(", addressDetail=").append(addressDetail);
        sb.append(", areaCode=").append(areaCode);
        sb.append(", postalCode=").append(postalCode);
        sb.append(", tel=").append(tel);
        sb.append(", isDefault=").append(isDefault);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 收货地址表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallAddress other = (LitemallAddress) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getProvince() == null ? other.getProvince() == null : this.getProvince().equals(other.getProvince()))
            && (this.getCity() == null ? other.getCity() == null : this.getCity().equals(other.getCity()))
            && (this.getCounty() == null ? other.getCounty() == null : this.getCounty().equals(other.getCounty()))
            && (this.getAddressDetail() == null ? other.getAddressDetail() == null : this.getAddressDetail().equals(other.getAddressDetail()))
            && (this.getAreaCode() == null ? other.getAreaCode() == null : this.getAreaCode().equals(other.getAreaCode()))
            && (this.getPostalCode() == null ? other.getPostalCode() == null : this.getPostalCode().equals(other.getPostalCode()))
            && (this.getTel() == null ? other.getTel() == null : this.getTel().equals(other.getTel()))
            && (this.getIsDefault() == null ? other.getIsDefault() == null : this.getIsDefault().equals(other.getIsDefault()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * 收货地址表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getProvince() == null) ? 0 : getProvince().hashCode());
        result = prime * result + ((getCity() == null) ? 0 : getCity().hashCode());
        result = prime * result + ((getCounty() == null) ? 0 : getCounty().hashCode());
        result = prime * result + ((getAddressDetail() == null) ? 0 : getAddressDetail().hashCode());
        result = prime * result + ((getAreaCode() == null) ? 0 : getAreaCode().hashCode());
        result = prime * result + ((getPostalCode() == null) ? 0 : getPostalCode().hashCode());
        result = prime * result + ((getTel() == null) ? 0 : getTel().hashCode());
        result = prime * result + ((getIsDefault() == null) ? 0 : getIsDefault().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_address
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_address
         */
        private final Boolean value;

        /**
         * litemall_address
         */
        private final String name;

        /**
         * 收货地址表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 收货地址表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 收货地址表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 收货地址表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 收货地址表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 收货地址表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_address
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        name("name", "name", "VARCHAR", true),
        userId("user_id", "userId", "INTEGER", false),
        province("province", "province", "VARCHAR", false),
        city("city", "city", "VARCHAR", false),
        county("county", "county", "VARCHAR", false),
        addressDetail("address_detail", "addressDetail", "VARCHAR", false),
        areaCode("area_code", "areaCode", "CHAR", false),
        postalCode("postal_code", "postalCode", "CHAR", false),
        tel("tel", "tel", "VARCHAR", false),
        isDefault("is_default", "isDefault", "BIT", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_address
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_address
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_address
         */
        private final String column;

        /**
         * litemall_address
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_address
         */
        private final String javaProperty;

        /**
         * litemall_address
         */
        private final String jdbcType;

        /**
         * 收货地址表
         */
        public String value() {
            return this.column;
        }

        /**
         * 收货地址表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 收货地址表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 收货地址表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 收货地址表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 收货地址表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 收货地址表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 收货地址表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 收货地址表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 收货地址表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 收货地址表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}