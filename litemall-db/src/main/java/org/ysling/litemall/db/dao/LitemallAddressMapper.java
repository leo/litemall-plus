package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallAddress;
import org.ysling.litemall.db.example.LitemallAddressExample;

@Mapper
@Repository
public interface LitemallAddressMapper {
    /**
     * 收货地址表
     */
    long countByExample(LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int deleteByExample(LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 收货地址表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 收货地址表
     */
    int insert(LitemallAddress record);

    /**
     * 收货地址表
     */
    int insertSelective(LitemallAddress record);

    /**
     * 收货地址表
     */
    LitemallAddress selectOneByExample(LitemallAddressExample example);

    /**
     * 收货地址表
     */
    LitemallAddress selectOneByExampleSelective(@Param("example") LitemallAddressExample example, @Param("selective") LitemallAddress.Column ... selective);

    /**
     * 收货地址表
     */
    List<LitemallAddress> selectByExampleSelective(@Param("example") LitemallAddressExample example, @Param("selective") LitemallAddress.Column ... selective);

    /**
     * 收货地址表
     */
    List<LitemallAddress> selectByExample(LitemallAddressExample example);

    /**
     * 收货地址表
     */
    LitemallAddress selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallAddress.Column ... selective);

    /**
     * 收货地址表
     */
    LitemallAddress selectByPrimaryKey(Integer id);

    /**
     * 收货地址表
     */
    LitemallAddress selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 收货地址表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallAddress record, @Param("example") LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallAddress record, @Param("example") LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int updateByExampleSelective(@Param("record") LitemallAddress record, @Param("example") LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int updateByExample(@Param("record") LitemallAddress record, @Param("example") LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallAddress record);

    /**
     * 收货地址表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallAddress record);

    /**
     * 收货地址表
     */
    int updateByPrimaryKeySelective(LitemallAddress record);

    /**
     * 收货地址表
     */
    int updateByPrimaryKey(LitemallAddress record);

    /**
     * 收货地址表
     */
    int logicalDeleteByExample(@Param("example") LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallAddressExample example);

    /**
     * 收货地址表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 收货地址表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}