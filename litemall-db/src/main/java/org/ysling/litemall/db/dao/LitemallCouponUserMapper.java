package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallCouponUser;
import org.ysling.litemall.db.example.LitemallCouponUserExample;

@Mapper
@Repository
public interface LitemallCouponUserMapper {
    /**
     * 优惠券用户使用表
     */
    long countByExample(LitemallCouponUserExample example);

    /**
     * 优惠券用户使用表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallCouponUserExample example);

    /**
     * 优惠券用户使用表
     */
    int deleteByExample(LitemallCouponUserExample example);

    /**
     * 优惠券用户使用表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 优惠券用户使用表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 优惠券用户使用表
     */
    int insert(LitemallCouponUser record);

    /**
     * 优惠券用户使用表
     */
    int insertSelective(LitemallCouponUser record);

    /**
     * 优惠券用户使用表
     */
    LitemallCouponUser selectOneByExample(LitemallCouponUserExample example);

    /**
     * 优惠券用户使用表
     */
    LitemallCouponUser selectOneByExampleSelective(@Param("example") LitemallCouponUserExample example, @Param("selective") LitemallCouponUser.Column ... selective);

    /**
     * 优惠券用户使用表
     */
    List<LitemallCouponUser> selectByExampleSelective(@Param("example") LitemallCouponUserExample example, @Param("selective") LitemallCouponUser.Column ... selective);

    /**
     * 优惠券用户使用表
     */
    List<LitemallCouponUser> selectByExample(LitemallCouponUserExample example);

    /**
     * 优惠券用户使用表
     */
    LitemallCouponUser selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallCouponUser.Column ... selective);

    /**
     * 优惠券用户使用表
     */
    LitemallCouponUser selectByPrimaryKey(Integer id);

    /**
     * 优惠券用户使用表
     */
    LitemallCouponUser selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 优惠券用户使用表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallCouponUser record, @Param("example") LitemallCouponUserExample example);

    /**
     * 优惠券用户使用表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallCouponUser record, @Param("example") LitemallCouponUserExample example);

    /**
     * 优惠券用户使用表
     */
    int updateByExampleSelective(@Param("record") LitemallCouponUser record, @Param("example") LitemallCouponUserExample example);

    /**
     * 优惠券用户使用表
     */
    int updateByExample(@Param("record") LitemallCouponUser record, @Param("example") LitemallCouponUserExample example);

    /**
     * 优惠券用户使用表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallCouponUser record);

    /**
     * 优惠券用户使用表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallCouponUser record);

    /**
     * 优惠券用户使用表
     */
    int updateByPrimaryKeySelective(LitemallCouponUser record);

    /**
     * 优惠券用户使用表
     */
    int updateByPrimaryKey(LitemallCouponUser record);

    /**
     * 优惠券用户使用表
     */
    int logicalDeleteByExample(@Param("example") LitemallCouponUserExample example);

    /**
     * 优惠券用户使用表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallCouponUserExample example);

    /**
     * 优惠券用户使用表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 优惠券用户使用表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}