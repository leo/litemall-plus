package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_keyword
 * Database Table Remarks : 
 *   关键字表
 * @author ysling
 */
public class LitemallKeyword implements Serializable {
    /**
     * litemall_keyword
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_keyword
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 关键字表ID
     */
    private Integer id;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 关键字的跳转链接
     */
    private String url;

    /**
     * 是否是热门关键字
     */
    private Boolean isHot;

    /**
     * 是否是默认关键字
     */
    private Boolean isDefault;

    /**
     * 排序
     */
    private Integer sortOrder;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_keyword
     */
    private static final long serialVersionUID = 1L;

    /**
     * 关键字表ID
     * @return id 关键字表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 关键字表ID
     * @param id 关键字表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 关键字
     * @return keyword 关键字
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * 关键字
     * @param keyword 关键字
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * 关键字的跳转链接
     * @return url 关键字的跳转链接
     */
    public String getUrl() {
        return url;
    }

    /**
     * 关键字的跳转链接
     * @param url 关键字的跳转链接
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 是否是热门关键字
     * @return is_hot 是否是热门关键字
     */
    public Boolean getIsHot() {
        return isHot;
    }

    /**
     * 是否是热门关键字
     * @param isHot 是否是热门关键字
     */
    public void setIsHot(Boolean isHot) {
        this.isHot = isHot;
    }

    /**
     * 是否是默认关键字
     * @return is_default 是否是默认关键字
     */
    public Boolean getIsDefault() {
        return isDefault;
    }

    /**
     * 是否是默认关键字
     * @param isDefault 是否是默认关键字
     */
    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * 排序
     * @return sort_order 排序
     */
    public Integer getSortOrder() {
        return sortOrder;
    }

    /**
     * 排序
     * @param sortOrder 排序
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 关键字表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 关键字表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", keyword=").append(keyword);
        sb.append(", url=").append(url);
        sb.append(", isHot=").append(isHot);
        sb.append(", isDefault=").append(isDefault);
        sb.append(", sortOrder=").append(sortOrder);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 关键字表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallKeyword other = (LitemallKeyword) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getKeyword() == null ? other.getKeyword() == null : this.getKeyword().equals(other.getKeyword()))
            && (this.getUrl() == null ? other.getUrl() == null : this.getUrl().equals(other.getUrl()))
            && (this.getIsHot() == null ? other.getIsHot() == null : this.getIsHot().equals(other.getIsHot()))
            && (this.getIsDefault() == null ? other.getIsDefault() == null : this.getIsDefault().equals(other.getIsDefault()))
            && (this.getSortOrder() == null ? other.getSortOrder() == null : this.getSortOrder().equals(other.getSortOrder()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * 关键字表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getKeyword() == null) ? 0 : getKeyword().hashCode());
        result = prime * result + ((getUrl() == null) ? 0 : getUrl().hashCode());
        result = prime * result + ((getIsHot() == null) ? 0 : getIsHot().hashCode());
        result = prime * result + ((getIsDefault() == null) ? 0 : getIsDefault().hashCode());
        result = prime * result + ((getSortOrder() == null) ? 0 : getSortOrder().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_keyword
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_keyword
         */
        private final Boolean value;

        /**
         * litemall_keyword
         */
        private final String name;

        /**
         * 关键字表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 关键字表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 关键字表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 关键字表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 关键字表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 关键字表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_keyword
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        keyword("keyword", "keyword", "VARCHAR", false),
        url("url", "url", "VARCHAR", false),
        isHot("is_hot", "isHot", "BIT", false),
        isDefault("is_default", "isDefault", "BIT", false),
        sortOrder("sort_order", "sortOrder", "INTEGER", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_keyword
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_keyword
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_keyword
         */
        private final String column;

        /**
         * litemall_keyword
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_keyword
         */
        private final String javaProperty;

        /**
         * litemall_keyword
         */
        private final String jdbcType;

        /**
         * 关键字表
         */
        public String value() {
            return this.column;
        }

        /**
         * 关键字表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 关键字表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 关键字表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 关键字表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 关键字表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 关键字表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 关键字表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 关键字表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 关键字表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 关键字表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}