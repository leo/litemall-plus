package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallPermission;
import org.ysling.litemall.db.example.LitemallPermissionExample;

@Mapper
@Repository
public interface LitemallPermissionMapper {
    /**
     * 权限表
     */
    long countByExample(LitemallPermissionExample example);

    /**
     * 权限表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallPermissionExample example);

    /**
     * 权限表
     */
    int deleteByExample(LitemallPermissionExample example);

    /**
     * 权限表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 权限表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 权限表
     */
    int insert(LitemallPermission record);

    /**
     * 权限表
     */
    int insertSelective(LitemallPermission record);

    /**
     * 权限表
     */
    LitemallPermission selectOneByExample(LitemallPermissionExample example);

    /**
     * 权限表
     */
    LitemallPermission selectOneByExampleSelective(@Param("example") LitemallPermissionExample example, @Param("selective") LitemallPermission.Column ... selective);

    /**
     * 权限表
     */
    List<LitemallPermission> selectByExampleSelective(@Param("example") LitemallPermissionExample example, @Param("selective") LitemallPermission.Column ... selective);

    /**
     * 权限表
     */
    List<LitemallPermission> selectByExample(LitemallPermissionExample example);

    /**
     * 权限表
     */
    LitemallPermission selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallPermission.Column ... selective);

    /**
     * 权限表
     */
    LitemallPermission selectByPrimaryKey(Integer id);

    /**
     * 权限表
     */
    LitemallPermission selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 权限表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallPermission record, @Param("example") LitemallPermissionExample example);

    /**
     * 权限表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallPermission record, @Param("example") LitemallPermissionExample example);

    /**
     * 权限表
     */
    int updateByExampleSelective(@Param("record") LitemallPermission record, @Param("example") LitemallPermissionExample example);

    /**
     * 权限表
     */
    int updateByExample(@Param("record") LitemallPermission record, @Param("example") LitemallPermissionExample example);

    /**
     * 权限表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallPermission record);

    /**
     * 权限表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallPermission record);

    /**
     * 权限表
     */
    int updateByPrimaryKeySelective(LitemallPermission record);

    /**
     * 权限表
     */
    int updateByPrimaryKey(LitemallPermission record);

    /**
     * 权限表
     */
    int logicalDeleteByExample(@Param("example") LitemallPermissionExample example);

    /**
     * 权限表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallPermissionExample example);

    /**
     * 权限表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 权限表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}