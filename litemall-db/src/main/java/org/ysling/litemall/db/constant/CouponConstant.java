package org.ysling.litemall.db.constant;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import java.io.Serializable;

public class CouponConstant implements Serializable {
    /**通用*/
    public static final Short TYPE_COMMON = 0;
    /**注册*/
    public static final Short TYPE_REGISTER = 1;
    /**兑换*/
    public static final Short TYPE_CODE = 2;

    /**全商品*/
    public static final Short GOODS_TYPE_ALL = 0;
    /**类目限制*/
    public static final Short GOODS_TYPE_CATEGORY = 1;
    /**商品限制*/
    public static final Short GOODS_TYPE_ARRAY = 2;

    /**正常*/
    public static final Short STATUS_NORMAL = 0;
    /**过期*/
    public static final Short STATUS_EXPIRED = 1;
    /**下架*/
    public static final Short STATUS_OUT = 2;

    /**过期类型基于领取时间的有效天数days*/
    public static final Short TIME_TYPE_DAYS = 0;
    /**过期类型基于start_time和end_time*/
    public static final Short TIME_TYPE_TIME = 1;
}
