package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallTimeline;
import org.ysling.litemall.db.example.LitemallTimelineExample;

@Mapper
@Repository
public interface LitemallTimelineMapper {
    /**
     * 动态表
     */
    long countByExample(LitemallTimelineExample example);

    /**
     * 动态表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallTimelineExample example);

    /**
     * 动态表
     */
    int deleteByExample(LitemallTimelineExample example);

    /**
     * 动态表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 动态表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 动态表
     */
    int insert(LitemallTimeline record);

    /**
     * 动态表
     */
    int insertSelective(LitemallTimeline record);

    /**
     * 动态表
     */
    LitemallTimeline selectOneByExample(LitemallTimelineExample example);

    /**
     * 动态表
     */
    LitemallTimeline selectOneByExampleSelective(@Param("example") LitemallTimelineExample example, @Param("selective") LitemallTimeline.Column ... selective);

    /**
     * 动态表
     */
    List<LitemallTimeline> selectByExampleSelective(@Param("example") LitemallTimelineExample example, @Param("selective") LitemallTimeline.Column ... selective);

    /**
     * 动态表
     */
    List<LitemallTimeline> selectByExample(LitemallTimelineExample example);

    /**
     * 动态表
     */
    LitemallTimeline selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallTimeline.Column ... selective);

    /**
     * 动态表
     */
    LitemallTimeline selectByPrimaryKey(Integer id);

    /**
     * 动态表
     */
    LitemallTimeline selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 动态表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallTimeline record, @Param("example") LitemallTimelineExample example);

    /**
     * 动态表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallTimeline record, @Param("example") LitemallTimelineExample example);

    /**
     * 动态表
     */
    int updateByExampleSelective(@Param("record") LitemallTimeline record, @Param("example") LitemallTimelineExample example);

    /**
     * 动态表
     */
    int updateByExample(@Param("record") LitemallTimeline record, @Param("example") LitemallTimelineExample example);

    /**
     * 动态表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallTimeline record);

    /**
     * 动态表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallTimeline record);

    /**
     * 动态表
     */
    int updateByPrimaryKeySelective(LitemallTimeline record);

    /**
     * 动态表
     */
    int updateByPrimaryKey(LitemallTimeline record);

    /**
     * 动态表
     */
    int logicalDeleteByExample(@Param("example") LitemallTimelineExample example);

    /**
     * 动态表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallTimelineExample example);

    /**
     * 动态表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 动态表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}