package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallQrcode;
import org.ysling.litemall.db.example.LitemallQrcodeExample;

@Mapper
@Repository
public interface LitemallQrcodeMapper {
    /**
     * 二维码生成表
     */
    long countByExample(LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    int deleteByExample(LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 二维码生成表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 二维码生成表
     */
    int insert(LitemallQrcode record);

    /**
     * 二维码生成表
     */
    int insertSelective(LitemallQrcode record);

    /**
     * 二维码生成表
     */
    LitemallQrcode selectOneByExample(LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    LitemallQrcode selectOneByExampleSelective(@Param("example") LitemallQrcodeExample example, @Param("selective") LitemallQrcode.Column ... selective);

    /**
     * 二维码生成表
     */
    LitemallQrcode selectOneByExampleWithBLOBs(LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    List<LitemallQrcode> selectByExampleSelective(@Param("example") LitemallQrcodeExample example, @Param("selective") LitemallQrcode.Column ... selective);

    /**
     * 二维码生成表
     */
    List<LitemallQrcode> selectByExampleWithBLOBs(LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    List<LitemallQrcode> selectByExample(LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    LitemallQrcode selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallQrcode.Column ... selective);

    /**
     * 二维码生成表
     */
    LitemallQrcode selectByPrimaryKey(Integer id);

    /**
     * 二维码生成表
     */
    LitemallQrcode selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 二维码生成表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallQrcode record, @Param("example") LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallQrcode record, @Param("example") LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    int updateByExampleSelective(@Param("record") LitemallQrcode record, @Param("example") LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    int updateWithVersionByExampleWithBLOBs(@Param("version") Integer version, @Param("record") LitemallQrcode record, @Param("example") LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    int updateByExampleWithBLOBs(@Param("record") LitemallQrcode record, @Param("example") LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    int updateByExample(@Param("record") LitemallQrcode record, @Param("example") LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallQrcode record);

    /**
     * 二维码生成表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallQrcode record);

    /**
     * 二维码生成表
     */
    int updateByPrimaryKeySelective(LitemallQrcode record);

    /**
     * 二维码生成表
     */
    int updateWithVersionByPrimaryKeyWithBLOBs(@Param("version") Integer version, @Param("record") LitemallQrcode record);

    /**
     * 二维码生成表
     */
    int updateByPrimaryKeyWithBLOBs(LitemallQrcode record);

    /**
     * 二维码生成表
     */
    int updateByPrimaryKey(LitemallQrcode record);

    /**
     * 二维码生成表
     */
    int logicalDeleteByExample(@Param("example") LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallQrcodeExample example);

    /**
     * 二维码生成表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 二维码生成表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}