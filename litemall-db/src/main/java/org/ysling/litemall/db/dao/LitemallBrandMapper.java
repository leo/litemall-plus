package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallBrand;
import org.ysling.litemall.db.example.LitemallBrandExample;

@Mapper
@Repository
public interface LitemallBrandMapper {
    /**
     * 品牌商表
     */
    long countByExample(LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int deleteByExample(LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 品牌商表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 品牌商表
     */
    int insert(LitemallBrand record);

    /**
     * 品牌商表
     */
    int insertSelective(LitemallBrand record);

    /**
     * 品牌商表
     */
    LitemallBrand selectOneByExample(LitemallBrandExample example);

    /**
     * 品牌商表
     */
    LitemallBrand selectOneByExampleSelective(@Param("example") LitemallBrandExample example, @Param("selective") LitemallBrand.Column ... selective);

    /**
     * 品牌商表
     */
    List<LitemallBrand> selectByExampleSelective(@Param("example") LitemallBrandExample example, @Param("selective") LitemallBrand.Column ... selective);

    /**
     * 品牌商表
     */
    List<LitemallBrand> selectByExample(LitemallBrandExample example);

    /**
     * 品牌商表
     */
    LitemallBrand selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallBrand.Column ... selective);

    /**
     * 品牌商表
     */
    LitemallBrand selectByPrimaryKey(Integer id);

    /**
     * 品牌商表
     */
    LitemallBrand selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 品牌商表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallBrand record, @Param("example") LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallBrand record, @Param("example") LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int updateByExampleSelective(@Param("record") LitemallBrand record, @Param("example") LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int updateByExample(@Param("record") LitemallBrand record, @Param("example") LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallBrand record);

    /**
     * 品牌商表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallBrand record);

    /**
     * 品牌商表
     */
    int updateByPrimaryKeySelective(LitemallBrand record);

    /**
     * 品牌商表
     */
    int updateByPrimaryKey(LitemallBrand record);

    /**
     * 品牌商表
     */
    int logicalDeleteByExample(@Param("example") LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallBrandExample example);

    /**
     * 品牌商表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 品牌商表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}