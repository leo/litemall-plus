package org.ysling.litemall.db.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.ysling.litemall.db.dao.LitemallQrcodeMapper;
import org.ysling.litemall.db.domain.LitemallOrderGoods;
import org.ysling.litemall.db.domain.LitemallQrcode;
import org.ysling.litemall.db.example.LitemallQrcodeExample;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 燃烧的头发
 * @since 2022-01-20
 */
@Service
@CacheConfig(cacheNames = "qrcode")
public class LitemallQrcodeService {

	@Resource
	private LitemallQrcodeMapper qrcodeMapper;

	@CacheEvict(allEntries = true)
	public Integer add(LitemallQrcode qrcode) {
		qrcode.setId(0);
		qrcode.setAddTime(LocalDateTime.now());
		qrcode.setUpdateTime(LocalDateTime.now());
		return qrcodeMapper.insertSelective(qrcode);
	}

	@Cacheable(sync = true)
	public int count() {
		LitemallQrcodeExample example = new LitemallQrcodeExample();
		example.or().andDeletedEqualTo(false);
		return (int) qrcodeMapper.countByExample(example);
	}

	@Cacheable(sync = true)
	public List<LitemallQrcode> findQrcode(String tableNo) {
		LitemallQrcodeExample example = new LitemallQrcodeExample();
		example.or().andTableNoEqualTo(tableNo).andDeletedEqualTo(false);
		return qrcodeMapper.selectByExample(example);
	}

	@Cacheable(sync = true)
	public List<LitemallQrcode> queryList(Integer page, Integer limit, String sort, String order) {
		LitemallQrcodeExample example = new LitemallQrcodeExample();
		example.or().andDeletedEqualTo(false);

		if (StringUtils.hasText(sort) && StringUtils.hasText(order)) {
			example.setOrderByClause(sort + " " + order);
		}
		PageHelper.startPage(page, limit);
		return qrcodeMapper.selectByExampleWithBLOBs(example);
	}

	@CacheEvict(allEntries = true)
	public int updateBlobById(LitemallQrcode qrcode) {
		qrcode.setUpdateTime(LocalDateTime.now());
		return qrcodeMapper.updateByPrimaryKeyWithBLOBs(qrcode);
	}

	@CacheEvict(allEntries = true)
	public int deleteById(Integer id) {
		return qrcodeMapper.logicalDeleteByPrimaryKey(id);
	}

}
