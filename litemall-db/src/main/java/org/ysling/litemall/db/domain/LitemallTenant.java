package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_tenant
 * Database Table Remarks : 
 *   租户表，逻辑分库
 * @author ysling
 */
public class LitemallTenant implements Serializable {
    /**
     * litemall_tenant
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_tenant
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 租户表ID
     */
    private Integer id;

    /**
     * 租户地址
     */
    private String address;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_tenant
     */
    private static final long serialVersionUID = 1L;

    /**
     * 租户表ID
     * @return id 租户表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 租户表ID
     * @param id 租户表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 租户地址
     * @return address 租户地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 租户地址
     * @param address 租户地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 租户表，逻辑分库
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 租户表，逻辑分库
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", address=").append(address);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 租户表，逻辑分库
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallTenant other = (LitemallTenant) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getAddress() == null ? other.getAddress() == null : this.getAddress().equals(other.getAddress()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * 租户表，逻辑分库
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_tenant
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_tenant
         */
        private final Boolean value;

        /**
         * litemall_tenant
         */
        private final String name;

        /**
         * 租户表，逻辑分库
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 租户表，逻辑分库
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 租户表，逻辑分库
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 租户表，逻辑分库
         */
        public String getName() {
            return this.name;
        }

        /**
         * 租户表，逻辑分库
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 租户表，逻辑分库
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_tenant
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        address("address", "address", "VARCHAR", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_tenant
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_tenant
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_tenant
         */
        private final String column;

        /**
         * litemall_tenant
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_tenant
         */
        private final String javaProperty;

        /**
         * litemall_tenant
         */
        private final String jdbcType;

        /**
         * 租户表，逻辑分库
         */
        public String value() {
            return this.column;
        }

        /**
         * 租户表，逻辑分库
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 租户表，逻辑分库
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 租户表，逻辑分库
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 租户表，逻辑分库
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 租户表，逻辑分库
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 租户表，逻辑分库
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 租户表，逻辑分库
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 租户表，逻辑分库
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 租户表，逻辑分库
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 租户表，逻辑分库
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}