package org.ysling.litemall.db.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import com.github.pagehelper.PageHelper;
import org.ysling.litemall.db.dao.LitemallTimelineMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.ysling.litemall.db.domain.LitemallOrder;
import org.ysling.litemall.db.domain.LitemallTenant;
import org.ysling.litemall.db.domain.LitemallTimeline;
import org.ysling.litemall.db.example.LitemallTimelineExample;

import java.time.LocalDateTime;
import java.util.List;

@Service
@CacheConfig(cacheNames = "timeline")
public class LitemallTimelineService {

    @Autowired
    private LitemallTimelineMapper timelineMapper;

    @CacheEvict(allEntries = true)
    public Integer add(LitemallTimeline timeline) {
        timeline.setAddTime(LocalDateTime.now());
        timeline.setUpdateTime(LocalDateTime.now());
        return timelineMapper.insertSelective(timeline);
    }

    @Cacheable(sync = true)
    public LitemallTimeline findById(Integer id) {
        return timelineMapper.selectByPrimaryKey(id);
    }

    @Cacheable(sync = true)
    public int count() {
        LitemallTimelineExample example = new LitemallTimelineExample();
        example.or().andDeletedEqualTo(false);
        return (int) timelineMapper.countByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallTimeline> queryList(Integer page, Integer limit) {
        LitemallTimelineExample example = new LitemallTimelineExample();
        example.or().andDeletedEqualTo(false);
        example.setOrderByClause(LitemallTimeline.Column.addTime.desc());
        PageHelper.startPage(page, limit);
        return timelineMapper.selectByExample(example);
    }

    @CacheEvict(allEntries = true)
    public int updateById(LitemallTimeline timeline) {
        timeline.setUpdateTime(LocalDateTime.now());
        return timelineMapper.updateByPrimaryKeySelective(timeline);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallTimeline timeline) {
        timeline.setUpdateTime(LocalDateTime.now());
        return timelineMapper.updateWithVersionByPrimaryKeySelective(timeline.getVersion(), timeline);
    }

    @CacheEvict(allEntries = true)
    public void deleteById(Integer id) {
        LitemallTimelineExample example = new LitemallTimelineExample();
        example.or().andIdEqualTo(id);
        timelineMapper.logicalDeleteByExample(example);
    }


}
