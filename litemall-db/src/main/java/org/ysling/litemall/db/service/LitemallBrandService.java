package org.ysling.litemall.db.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import com.github.pagehelper.PageHelper;
import org.ysling.litemall.db.dao.LitemallBrandMapper;
import org.ysling.litemall.db.domain.LitemallAftersale;
import org.ysling.litemall.db.domain.LitemallBrand;
import org.ysling.litemall.db.domain.LitemallBrand.Column;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.ysling.litemall.db.example.LitemallBrandExample;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
@CacheConfig(cacheNames = "brand")
public class LitemallBrandService {

    @Resource
    private LitemallBrandMapper brandMapper;

    @Cacheable(sync = true)
    public List<LitemallBrand> query(Integer page, Integer limit, String sort, String order) {
        LitemallBrandExample example = new LitemallBrandExample();
        example.or().andStatusEqualTo((byte)0).andDeletedEqualTo(false);
        if (!Objects.isNull(sort) && !Objects.isNull(order)) {
            example.setOrderByClause(sort + " " + order);
        }
        PageHelper.startPage(page, limit);
        return brandMapper.selectByExampleSelective(example);
    }

    @Cacheable(sync = true)
    public List<LitemallBrand> all() {
        LitemallBrandExample example = new LitemallBrandExample();
        example.or().andStatusEqualTo((byte)0).andDeletedEqualTo(false);
        return brandMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public LitemallBrand findById(Integer id) {
        if (id == null || id == 0) return new LitemallBrand();
        return brandMapper.selectByPrimaryKey(id);
    }

    @Cacheable(sync = true)
    public LitemallBrand findByUserId(Integer userId) {
        LitemallBrandExample example = new LitemallBrandExample();
        example.or().andUserIdEqualTo(userId).andDeletedEqualTo(false);
        return brandMapper.selectOneByExample(example);
    }

    @Cacheable(sync = true)
    public LitemallBrand findByBrandName(String name) {
        LitemallBrandExample example = new LitemallBrandExample();
        example.or().andNameEqualTo(name).andDeletedEqualTo(false);
        return brandMapper.selectOneByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallBrand> queryByUserId(Integer userId) {
        LitemallBrandExample example = new LitemallBrandExample();
        example.or().andUserIdEqualTo(userId).andDeletedEqualTo(false);
        return brandMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallBrand> querySelective(String id, String name, Integer page, Integer size, String sort, String order) {
        LitemallBrandExample example = new LitemallBrandExample();
        LitemallBrandExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);

        if (!Objects.isNull(id)) {
            criteria.andIdEqualTo(Integer.valueOf(id));
        }
        if (!Objects.isNull(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        if (!Objects.isNull(sort) && !Objects.isNull(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return brandMapper.selectByExample(example);
    }

    @CacheEvict(allEntries = true)
    public int updateById(LitemallBrand brand) {
        brand.setUpdateTime(LocalDateTime.now());
        return brandMapper.updateByPrimaryKeySelective(brand);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallBrand brand) {
        brand.setUpdateTime(LocalDateTime.now());
        return brandMapper.updateWithVersionByPrimaryKeySelective(brand.getVersion(), brand);
    }

    @CacheEvict(allEntries = true)
    public void deleteById(Integer id) {
        brandMapper.logicalDeleteByPrimaryKey(id);
    }

    @CacheEvict(allEntries = true)
    public void add(LitemallBrand brand) {
        brand.setAddTime(LocalDateTime.now());
        brand.setUpdateTime(LocalDateTime.now());
        brandMapper.insertSelective(brand);
    }


}
