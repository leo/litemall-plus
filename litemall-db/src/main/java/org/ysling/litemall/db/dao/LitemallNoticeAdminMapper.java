package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallNoticeAdmin;
import org.ysling.litemall.db.example.LitemallNoticeAdminExample;

@Mapper
@Repository
public interface LitemallNoticeAdminMapper {
    /**
     * 通知管理员表
     */
    long countByExample(LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int deleteByExample(LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 通知管理员表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 通知管理员表
     */
    int insert(LitemallNoticeAdmin record);

    /**
     * 通知管理员表
     */
    int insertSelective(LitemallNoticeAdmin record);

    /**
     * 通知管理员表
     */
    LitemallNoticeAdmin selectOneByExample(LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    LitemallNoticeAdmin selectOneByExampleSelective(@Param("example") LitemallNoticeAdminExample example, @Param("selective") LitemallNoticeAdmin.Column ... selective);

    /**
     * 通知管理员表
     */
    List<LitemallNoticeAdmin> selectByExampleSelective(@Param("example") LitemallNoticeAdminExample example, @Param("selective") LitemallNoticeAdmin.Column ... selective);

    /**
     * 通知管理员表
     */
    List<LitemallNoticeAdmin> selectByExample(LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    LitemallNoticeAdmin selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallNoticeAdmin.Column ... selective);

    /**
     * 通知管理员表
     */
    LitemallNoticeAdmin selectByPrimaryKey(Integer id);

    /**
     * 通知管理员表
     */
    LitemallNoticeAdmin selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 通知管理员表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallNoticeAdmin record, @Param("example") LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallNoticeAdmin record, @Param("example") LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int updateByExampleSelective(@Param("record") LitemallNoticeAdmin record, @Param("example") LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int updateByExample(@Param("record") LitemallNoticeAdmin record, @Param("example") LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallNoticeAdmin record);

    /**
     * 通知管理员表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallNoticeAdmin record);

    /**
     * 通知管理员表
     */
    int updateByPrimaryKeySelective(LitemallNoticeAdmin record);

    /**
     * 通知管理员表
     */
    int updateByPrimaryKey(LitemallNoticeAdmin record);

    /**
     * 通知管理员表
     */
    int logicalDeleteByExample(@Param("example") LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallNoticeAdminExample example);

    /**
     * 通知管理员表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 通知管理员表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}