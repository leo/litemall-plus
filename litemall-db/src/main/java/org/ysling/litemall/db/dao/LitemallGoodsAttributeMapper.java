package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallGoodsAttribute;
import org.ysling.litemall.db.example.LitemallGoodsAttributeExample;

@Mapper
@Repository
public interface LitemallGoodsAttributeMapper {
    /**
     * 商品参数表
     */
    long countByExample(LitemallGoodsAttributeExample example);

    /**
     * 商品参数表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallGoodsAttributeExample example);

    /**
     * 商品参数表
     */
    int deleteByExample(LitemallGoodsAttributeExample example);

    /**
     * 商品参数表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 商品参数表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 商品参数表
     */
    int insert(LitemallGoodsAttribute record);

    /**
     * 商品参数表
     */
    int insertSelective(LitemallGoodsAttribute record);

    /**
     * 商品参数表
     */
    LitemallGoodsAttribute selectOneByExample(LitemallGoodsAttributeExample example);

    /**
     * 商品参数表
     */
    LitemallGoodsAttribute selectOneByExampleSelective(@Param("example") LitemallGoodsAttributeExample example, @Param("selective") LitemallGoodsAttribute.Column ... selective);

    /**
     * 商品参数表
     */
    List<LitemallGoodsAttribute> selectByExampleSelective(@Param("example") LitemallGoodsAttributeExample example, @Param("selective") LitemallGoodsAttribute.Column ... selective);

    /**
     * 商品参数表
     */
    List<LitemallGoodsAttribute> selectByExample(LitemallGoodsAttributeExample example);

    /**
     * 商品参数表
     */
    LitemallGoodsAttribute selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallGoodsAttribute.Column ... selective);

    /**
     * 商品参数表
     */
    LitemallGoodsAttribute selectByPrimaryKey(Integer id);

    /**
     * 商品参数表
     */
    LitemallGoodsAttribute selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 商品参数表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallGoodsAttribute record, @Param("example") LitemallGoodsAttributeExample example);

    /**
     * 商品参数表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallGoodsAttribute record, @Param("example") LitemallGoodsAttributeExample example);

    /**
     * 商品参数表
     */
    int updateByExampleSelective(@Param("record") LitemallGoodsAttribute record, @Param("example") LitemallGoodsAttributeExample example);

    /**
     * 商品参数表
     */
    int updateByExample(@Param("record") LitemallGoodsAttribute record, @Param("example") LitemallGoodsAttributeExample example);

    /**
     * 商品参数表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallGoodsAttribute record);

    /**
     * 商品参数表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallGoodsAttribute record);

    /**
     * 商品参数表
     */
    int updateByPrimaryKeySelective(LitemallGoodsAttribute record);

    /**
     * 商品参数表
     */
    int updateByPrimaryKey(LitemallGoodsAttribute record);

    /**
     * 商品参数表
     */
    int logicalDeleteByExample(@Param("example") LitemallGoodsAttributeExample example);

    /**
     * 商品参数表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallGoodsAttributeExample example);

    /**
     * 商品参数表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 商品参数表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}