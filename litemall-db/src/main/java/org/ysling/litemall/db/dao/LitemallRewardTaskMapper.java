package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallRewardTask;
import org.ysling.litemall.db.example.LitemallRewardTaskExample;

@Mapper
@Repository
public interface LitemallRewardTaskMapper {
    /**
     * 赏金任务规则表
     */
    long countByExample(LitemallRewardTaskExample example);

    /**
     * 赏金任务规则表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallRewardTaskExample example);

    /**
     * 赏金任务规则表
     */
    int deleteByExample(LitemallRewardTaskExample example);

    /**
     * 赏金任务规则表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 赏金任务规则表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 赏金任务规则表
     */
    int insert(LitemallRewardTask record);

    /**
     * 赏金任务规则表
     */
    int insertSelective(LitemallRewardTask record);

    /**
     * 赏金任务规则表
     */
    LitemallRewardTask selectOneByExample(LitemallRewardTaskExample example);

    /**
     * 赏金任务规则表
     */
    LitemallRewardTask selectOneByExampleSelective(@Param("example") LitemallRewardTaskExample example, @Param("selective") LitemallRewardTask.Column ... selective);

    /**
     * 赏金任务规则表
     */
    List<LitemallRewardTask> selectByExampleSelective(@Param("example") LitemallRewardTaskExample example, @Param("selective") LitemallRewardTask.Column ... selective);

    /**
     * 赏金任务规则表
     */
    List<LitemallRewardTask> selectByExample(LitemallRewardTaskExample example);

    /**
     * 赏金任务规则表
     */
    LitemallRewardTask selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallRewardTask.Column ... selective);

    /**
     * 赏金任务规则表
     */
    LitemallRewardTask selectByPrimaryKey(Integer id);

    /**
     * 赏金任务规则表
     */
    LitemallRewardTask selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 赏金任务规则表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallRewardTask record, @Param("example") LitemallRewardTaskExample example);

    /**
     * 赏金任务规则表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallRewardTask record, @Param("example") LitemallRewardTaskExample example);

    /**
     * 赏金任务规则表
     */
    int updateByExampleSelective(@Param("record") LitemallRewardTask record, @Param("example") LitemallRewardTaskExample example);

    /**
     * 赏金任务规则表
     */
    int updateByExample(@Param("record") LitemallRewardTask record, @Param("example") LitemallRewardTaskExample example);

    /**
     * 赏金任务规则表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallRewardTask record);

    /**
     * 赏金任务规则表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallRewardTask record);

    /**
     * 赏金任务规则表
     */
    int updateByPrimaryKeySelective(LitemallRewardTask record);

    /**
     * 赏金任务规则表
     */
    int updateByPrimaryKey(LitemallRewardTask record);

    /**
     * 赏金任务规则表
     */
    int logicalDeleteByExample(@Param("example") LitemallRewardTaskExample example);

    /**
     * 赏金任务规则表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallRewardTaskExample example);

    /**
     * 赏金任务规则表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 赏金任务规则表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}