package org.ysling.litemall.db.mybatis;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 运行此方法生成mybatis代码
 * 生成代码自动放入对应目录
 * 配置文件targetProject应从项目名称开始到要生成到的classpath
 * Created by huhaichao on 2017/5/15.
 */
public class MyBatisGeneratorRun {

    /**
     * 代码生成完注释掉改main方法
     * @throws Exception
     */
//    public static void main(String[] args) throws Exception{
//        MyBatisGeneratorRun app = new MyBatisGeneratorRun();
//        app.generator();
//    }

    public void generator() throws Exception{
        List<String> warnings = new ArrayList<>();
        boolean overwrite = true;
        File file = new File("litemall-db/mybatis-generator/generatorConfig.xml");
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp.parseConfiguration(file);
        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        myBatisGenerator.generate(null);
        for(String warning:warnings){
            System.out.println(warning);
        }
    }
}
