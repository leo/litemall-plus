package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallUser;
import org.ysling.litemall.db.example.LitemallUserExample;

@Mapper
@Repository
public interface LitemallUserMapper {
    /**
     * 用户表
     */
    long countByExample(LitemallUserExample example);

    /**
     * 用户表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallUserExample example);

    /**
     * 用户表
     */
    int deleteByExample(LitemallUserExample example);

    /**
     * 用户表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 用户表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 用户表
     */
    int insert(LitemallUser record);

    /**
     * 用户表
     */
    int insertSelective(LitemallUser record);

    /**
     * 用户表
     */
    LitemallUser selectOneByExample(LitemallUserExample example);

    /**
     * 用户表
     */
    LitemallUser selectOneByExampleSelective(@Param("example") LitemallUserExample example, @Param("selective") LitemallUser.Column ... selective);

    /**
     * 用户表
     */
    List<LitemallUser> selectByExampleSelective(@Param("example") LitemallUserExample example, @Param("selective") LitemallUser.Column ... selective);

    /**
     * 用户表
     */
    List<LitemallUser> selectByExample(LitemallUserExample example);

    /**
     * 用户表
     */
    LitemallUser selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallUser.Column ... selective);

    /**
     * 用户表
     */
    LitemallUser selectByPrimaryKey(Integer id);

    /**
     * 用户表
     */
    LitemallUser selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 用户表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallUser record, @Param("example") LitemallUserExample example);

    /**
     * 用户表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallUser record, @Param("example") LitemallUserExample example);

    /**
     * 用户表
     */
    int updateByExampleSelective(@Param("record") LitemallUser record, @Param("example") LitemallUserExample example);

    /**
     * 用户表
     */
    int updateByExample(@Param("record") LitemallUser record, @Param("example") LitemallUserExample example);

    /**
     * 用户表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallUser record);

    /**
     * 用户表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallUser record);

    /**
     * 用户表
     */
    int updateByPrimaryKeySelective(LitemallUser record);

    /**
     * 用户表
     */
    int updateByPrimaryKey(LitemallUser record);

    /**
     * 用户表
     */
    int logicalDeleteByExample(@Param("example") LitemallUserExample example);

    /**
     * 用户表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallUserExample example);

    /**
     * 用户表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 用户表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}