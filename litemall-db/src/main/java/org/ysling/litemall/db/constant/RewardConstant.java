package org.ysling.litemall.db.constant;

public class RewardConstant {

    /**赏金规则正常*/
    public static final Short TASK_STATUS_ON = 0;
    /**赏金规则完成*/
    public static final Short TASK_STATUS_SUCCEED = 1;
    /**赏金规则提前下线*/
    public static final Short TASK_STATUS_CANCEL = 2;

    /**赏金待加入*/
    public static final Short STATUS_NONE = 0;
    /**赏金已加入*/
    public static final Short STATUS_SUCCEED = 1;
    /**赏金已取消*/
    public static final Short STATUS_CANCEL = 2;
}
