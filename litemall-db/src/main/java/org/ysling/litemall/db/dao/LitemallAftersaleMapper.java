package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallAftersale;
import org.ysling.litemall.db.example.LitemallAftersaleExample;

@Mapper
@Repository
public interface LitemallAftersaleMapper {
    /**
     * 售后表
     */
    long countByExample(LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int deleteByExample(LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 售后表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 售后表
     */
    int insert(LitemallAftersale record);

    /**
     * 售后表
     */
    int insertSelective(LitemallAftersale record);

    /**
     * 售后表
     */
    LitemallAftersale selectOneByExample(LitemallAftersaleExample example);

    /**
     * 售后表
     */
    LitemallAftersale selectOneByExampleSelective(@Param("example") LitemallAftersaleExample example, @Param("selective") LitemallAftersale.Column ... selective);

    /**
     * 售后表
     */
    List<LitemallAftersale> selectByExampleSelective(@Param("example") LitemallAftersaleExample example, @Param("selective") LitemallAftersale.Column ... selective);

    /**
     * 售后表
     */
    List<LitemallAftersale> selectByExample(LitemallAftersaleExample example);

    /**
     * 售后表
     */
    LitemallAftersale selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallAftersale.Column ... selective);

    /**
     * 售后表
     */
    LitemallAftersale selectByPrimaryKey(Integer id);

    /**
     * 售后表
     */
    LitemallAftersale selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 售后表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallAftersale record, @Param("example") LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallAftersale record, @Param("example") LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int updateByExampleSelective(@Param("record") LitemallAftersale record, @Param("example") LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int updateByExample(@Param("record") LitemallAftersale record, @Param("example") LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallAftersale record);

    /**
     * 售后表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallAftersale record);

    /**
     * 售后表
     */
    int updateByPrimaryKeySelective(LitemallAftersale record);

    /**
     * 售后表
     */
    int updateByPrimaryKey(LitemallAftersale record);

    /**
     * 售后表
     */
    int logicalDeleteByExample(@Param("example") LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallAftersaleExample example);

    /**
     * 售后表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 售后表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}