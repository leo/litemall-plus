package org.ysling.litemall.db.service;
/**
 *  Copyright (c) [ysling] [927069313@qq.com]
 *  [litemall-plus] is licensed under Mulan PSL v2.
 *  You can use this software according to the terms and conditions of the Mulan PSL v2.
 *  You may obtain a copy of Mulan PSL v2 at:
 *              http://license.coscl.org.cn/MulanPSL2
 *  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 *  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *  See the Mulan PSL v2 for more details.
 */

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.ysling.litemall.db.dao.LitemallMessageMapper;
import org.ysling.litemall.db.domain.LitemallMessage;
import org.ysling.litemall.db.example.LitemallMessageExample;

import java.time.LocalDateTime;
import java.util.List;


@Service
@CacheConfig(cacheNames = "message")
public class LitemallMessageService{

    @Autowired
    private LitemallMessageMapper messageMapper;

    @CacheEvict(allEntries = true)
    public Integer add(LitemallMessage message) {
        message.setAddTime(LocalDateTime.now());
        message.setUpdateTime(LocalDateTime.now());
        return messageMapper.insertSelective(message);
    }

    @Cacheable(sync = true)
    public LitemallMessage findById(Integer id) {
        return messageMapper.selectByPrimaryKey(id);
    }

    @Cacheable(sync = true)
    public Integer count() {
        LitemallMessageExample example = new LitemallMessageExample();
        example.or().andDeletedEqualTo(false);
        return (int) messageMapper.countByExample(example);
    }

    /**
     * 获取历史消息
     * @param receiveUserId 接收用户
     * @param sendUserId 发送用户
     * @return 消息集合
     */
    @Cacheable(sync = true)
    public List<LitemallMessage> getHistoryMessage(Integer receiveUserId, Integer sendUserId) {
        LitemallMessageExample example = new LitemallMessageExample();
        example.or().andReceiveUserIdEqualTo(receiveUserId)
                .andSendUserIdEqualTo(sendUserId)
                .andDeletedEqualTo(false);
        return messageMapper.selectByExample(example);
    }

    /**
     * 获取当前用户作为接收者的所有消息
     * @param receiveUserId 接收者用户id
     * @return 消息集合
     */
    @Cacheable(sync = true)
    public List<LitemallMessage> queryByReceiveUserId(Integer receiveUserId) {
        LitemallMessageExample example = new LitemallMessageExample();
        example.or().andReceiveUserIdEqualTo(receiveUserId).andDeletedEqualTo(false);
        return messageMapper.selectByExample(example);
    }

    /**
     * 获取当前用户作为发送者的所有消息
     * @param sendUserId 发送者用户id
     * @return 消息集合
     */
    @Cacheable(sync = true)
    public List<LitemallMessage> queryBySendUserId(Integer sendUserId) {
        LitemallMessageExample example = new LitemallMessageExample();
        example.or().andSendUserIdEqualTo(sendUserId).andDeletedEqualTo(false);
        return messageMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallMessage> queryList(Integer page, Integer limit) {
        LitemallMessageExample example = new LitemallMessageExample();
        example.or().andDeletedEqualTo(false);
        example.setOrderByClause(LitemallMessage.Column.addTime.desc());
        if (page != null && limit != null && limit != 0){
            PageHelper.startPage(page, limit);
        }
        return messageMapper.selectByExample(example);
    }

    @CacheEvict(allEntries = true)
    public Integer updateById(LitemallMessage message) {
        message.setUpdateTime(LocalDateTime.now());
        return messageMapper.updateByPrimaryKeySelective(message);
    }

    @CacheEvict(allEntries = true)
    public Integer updateVersionSelective(LitemallMessage message) {
        message.setUpdateTime(LocalDateTime.now());
        return messageMapper.updateWithVersionByPrimaryKeySelective(message.getVersion(), message);
    }

    @CacheEvict(allEntries = true)
    public void deleteById(Integer id) {
        LitemallMessageExample example = new LitemallMessageExample();
        example.or().andIdEqualTo(id);
        messageMapper.logicalDeleteByExample(example);
    }

    /**
     * 删除信息
     * @param receiveUserId 接收者
     * @param sendUserId 发送者
     */
    @CacheEvict(allEntries = true)
    public void deleteMessage(Integer receiveUserId, Integer sendUserId) {
        LitemallMessageExample example = new LitemallMessageExample();
        example.or().andReceiveUserIdEqualTo(receiveUserId).andSendUserIdEqualTo(sendUserId);
        messageMapper.logicalDeleteByExample(example);
    }


}
