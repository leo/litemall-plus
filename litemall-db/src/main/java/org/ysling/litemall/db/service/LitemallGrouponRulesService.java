package org.ysling.litemall.db.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.

import com.github.pagehelper.PageHelper;
import org.springframework.util.StringUtils;
import org.ysling.litemall.db.dao.LitemallGrouponRulesMapper;
import org.ysling.litemall.db.domain.LitemallGrouponRules;
import org.ysling.litemall.db.example.LitemallGrouponRulesExample;
import org.ysling.litemall.db.constant.GrouponConstant;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
@CacheConfig(cacheNames = "grouponRules")
public class LitemallGrouponRulesService {

    @Resource
    private LitemallGrouponRulesMapper grouponRulesMapper;

    @CacheEvict(allEntries = true)
    public int createRules(LitemallGrouponRules rules) {
        rules.setAddTime(LocalDateTime.now());
        rules.setUpdateTime(LocalDateTime.now());
        return grouponRulesMapper.insertSelective(rules);
    }

    @Cacheable(sync = true)
    public LitemallGrouponRules findById(Integer id) {
        return grouponRulesMapper.selectByPrimaryKeyWithLogicalDelete(id,false);
    }

    @Cacheable(sync = true)
    public LitemallGrouponRules findByGid(Integer goodsId) {
        LitemallGrouponRulesExample example = new LitemallGrouponRulesExample();
        example.or().andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        return grouponRulesMapper.selectOneByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallGrouponRules> queryByGoodsId(Integer goodsId) {
        LitemallGrouponRulesExample example = new LitemallGrouponRulesExample();
        example.or().andGoodsIdEqualTo(goodsId).andStatusEqualTo(GrouponConstant.RULE_STATUS_ON).andDeletedEqualTo(false);
        return grouponRulesMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public int countByGoodsId(Integer goodsId) {
        LitemallGrouponRulesExample example = new LitemallGrouponRulesExample();
        example.or().andGoodsIdEqualTo(goodsId).andStatusEqualTo(GrouponConstant.RULE_STATUS_ON).andDeletedEqualTo(false);
        return (int)grouponRulesMapper.countByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallGrouponRules> queryByStatus(Short status) {
        LitemallGrouponRulesExample example = new LitemallGrouponRulesExample();
        example.or().andStatusEqualTo(status).andDeletedEqualTo(false);
        return grouponRulesMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallGrouponRules> queryExpired() {
        LitemallGrouponRulesExample example = new LitemallGrouponRulesExample();
        example.or().andStatusEqualTo(GrouponConstant.RULE_STATUS_ON).andDeletedEqualTo(false);
        return grouponRulesMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallGrouponRules> queryList(Integer page, Integer limit, String sort, String order) {
        LitemallGrouponRulesExample example = new LitemallGrouponRulesExample();
        example.or().andStatusEqualTo(GrouponConstant.RULE_STATUS_ON).andDeletedEqualTo(false);
        example.setOrderByClause(sort + " " + order);
        PageHelper.startPage(page, limit);
        return grouponRulesMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public boolean isExpired(LitemallGrouponRules rules) {
        return (rules == null || rules.getExpireTime().isBefore(LocalDateTime.now()));
    }

    @Cacheable(sync = true)
    public List<LitemallGrouponRules> querySelective(String goodsId, Integer page, Integer size, String sort, String order) {
        LitemallGrouponRulesExample example = new LitemallGrouponRulesExample();
        LitemallGrouponRulesExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);

        if (StringUtils.hasText(goodsId)) {
            criteria.andGoodsIdEqualTo(Integer.parseInt(goodsId));
        }

        example.setOrderByClause(sort + " " + order);
        PageHelper.startPage(page, size);
        return grouponRulesMapper.selectByExample(example);
    }

    @CacheEvict(allEntries = true)
    public void delete(Integer id) {
        grouponRulesMapper.logicalDeleteByPrimaryKey(id);
    }

    @CacheEvict(allEntries = true)
    public void deleteByGid(Integer gid) {
        LitemallGrouponRulesExample example = new LitemallGrouponRulesExample();
        example.or().andGoodsIdEqualTo(gid);
        grouponRulesMapper.logicalDeleteByExample(example);
    }

    @CacheEvict(allEntries = true)
    public int updateById(LitemallGrouponRules grouponRules) {
        grouponRules.setUpdateTime(LocalDateTime.now());
        return grouponRulesMapper.updateByPrimaryKeySelective(grouponRules);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallGrouponRules grouponRules) {
        grouponRules.setUpdateTime(LocalDateTime.now());
        return grouponRulesMapper.updateWithVersionByPrimaryKeySelective(grouponRules.getVersion(), grouponRules);
    }
}