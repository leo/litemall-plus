package org.ysling.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.ysling.litemall.db.domain.LitemallSearchHistory;
import org.ysling.litemall.db.example.LitemallSearchHistoryExample;

@Mapper
@Repository
public interface LitemallSearchHistoryMapper {
    /**
     * 搜索历史表
     */
    long countByExample(LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int deleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int deleteByExample(LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int deleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);

    /**
     * 搜索历史表
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 搜索历史表
     */
    int insert(LitemallSearchHistory record);

    /**
     * 搜索历史表
     */
    int insertSelective(LitemallSearchHistory record);

    /**
     * 搜索历史表
     */
    LitemallSearchHistory selectOneByExample(LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    LitemallSearchHistory selectOneByExampleSelective(@Param("example") LitemallSearchHistoryExample example, @Param("selective") LitemallSearchHistory.Column ... selective);

    /**
     * 搜索历史表
     */
    List<LitemallSearchHistory> selectByExampleSelective(@Param("example") LitemallSearchHistoryExample example, @Param("selective") LitemallSearchHistory.Column ... selective);

    /**
     * 搜索历史表
     */
    List<LitemallSearchHistory> selectByExample(LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    LitemallSearchHistory selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") LitemallSearchHistory.Column ... selective);

    /**
     * 搜索历史表
     */
    LitemallSearchHistory selectByPrimaryKey(Integer id);

    /**
     * 搜索历史表
     */
    LitemallSearchHistory selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * 搜索历史表
     */
    int updateWithVersionByExample(@Param("version") Integer version, @Param("record") LitemallSearchHistory record, @Param("example") LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int updateWithVersionByExampleSelective(@Param("version") Integer version, @Param("record") LitemallSearchHistory record, @Param("example") LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int updateByExampleSelective(@Param("record") LitemallSearchHistory record, @Param("example") LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int updateByExample(@Param("record") LitemallSearchHistory record, @Param("example") LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int updateWithVersionByPrimaryKey(@Param("version") Integer version, @Param("record") LitemallSearchHistory record);

    /**
     * 搜索历史表
     */
    int updateWithVersionByPrimaryKeySelective(@Param("version") Integer version, @Param("record") LitemallSearchHistory record);

    /**
     * 搜索历史表
     */
    int updateByPrimaryKeySelective(LitemallSearchHistory record);

    /**
     * 搜索历史表
     */
    int updateByPrimaryKey(LitemallSearchHistory record);

    /**
     * 搜索历史表
     */
    int logicalDeleteByExample(@Param("example") LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int logicalDeleteWithVersionByExample(@Param("version") Integer version, @Param("example") LitemallSearchHistoryExample example);

    /**
     * 搜索历史表
     */
    int logicalDeleteByPrimaryKey(Integer id);

    /**
     * 搜索历史表
     */
    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Integer version, @Param("key") Integer id);
}