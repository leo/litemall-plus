package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_role
 * Database Table Remarks : 
 *   角色表
 * @author ysling
 */
public class LitemallRole implements Serializable {
    /**
     * litemall_role
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_role
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 角色表ID
     */
    private Integer id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色描述
     */
    private String desc;

    /**
     * 是否启用
     */
    private Boolean enabled;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_role
     */
    private static final long serialVersionUID = 1L;

    /**
     * 角色表ID
     * @return id 角色表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 角色表ID
     * @param id 角色表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 角色名称
     * @return name 角色名称
     */
    public String getName() {
        return name;
    }

    /**
     * 角色名称
     * @param name 角色名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 角色描述
     * @return desc 角色描述
     */
    public String getDesc() {
        return desc;
    }

    /**
     * 角色描述
     * @param desc 角色描述
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * 是否启用
     * @return enabled 是否启用
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * 是否启用
     * @param enabled 是否启用
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 角色表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 角色表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", desc=").append(desc);
        sb.append(", enabled=").append(enabled);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 角色表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallRole other = (LitemallRole) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getDesc() == null ? other.getDesc() == null : this.getDesc().equals(other.getDesc()))
            && (this.getEnabled() == null ? other.getEnabled() == null : this.getEnabled().equals(other.getEnabled()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * 角色表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getDesc() == null) ? 0 : getDesc().hashCode());
        result = prime * result + ((getEnabled() == null) ? 0 : getEnabled().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_role
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_role
         */
        private final Boolean value;

        /**
         * litemall_role
         */
        private final String name;

        /**
         * 角色表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 角色表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 角色表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 角色表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 角色表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 角色表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_role
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        name("name", "name", "VARCHAR", true),
        desc("desc", "desc", "VARCHAR", true),
        enabled("enabled", "enabled", "BIT", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_role
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_role
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_role
         */
        private final String column;

        /**
         * litemall_role
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_role
         */
        private final String javaProperty;

        /**
         * litemall_role
         */
        private final String jdbcType;

        /**
         * 角色表
         */
        public String value() {
            return this.column;
        }

        /**
         * 角色表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 角色表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 角色表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 角色表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 角色表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 角色表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 角色表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 角色表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 角色表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 角色表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}