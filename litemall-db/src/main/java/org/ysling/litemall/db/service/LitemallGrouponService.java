package org.ysling.litemall.db.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import com.github.pagehelper.PageHelper;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.ysling.litemall.db.dao.LitemallGrouponMapper;
import org.ysling.litemall.db.domain.LitemallGroupon;
import org.ysling.litemall.db.example.LitemallGrouponExample;
import org.ysling.litemall.db.constant.GrouponConstant;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
@CacheConfig(cacheNames = "groupon")
public class LitemallGrouponService {

    @Resource
    private LitemallGrouponMapper grouponMapper;

    /**
     * 获取用户发起的团购记录
     *
     * @param userId
     * @return
     */
    @Cacheable(sync = true)
    public List<LitemallGroupon> queryMyGroupon(Integer userId) {
        LitemallGrouponExample example = new LitemallGrouponExample();
        example.or().andUserIdEqualTo(userId).andCreatorUserIdEqualTo(userId).andGrouponIdEqualTo(0).andStatusNotEqualTo(GrouponConstant.STATUS_NONE).andDeletedEqualTo(false);
        example.orderBy("add_time desc");
        return grouponMapper.selectByExample(example);
    }

    /**
     * 获取用户参与的团购记录
     *
     * @param userId
     * @return
     */
    @Cacheable(sync = true)
    public List<LitemallGroupon> queryMyJoinGroupon(Integer userId) {
        LitemallGrouponExample example = new LitemallGrouponExample();
        example.or().andUserIdEqualTo(userId).andGrouponIdNotEqualTo(0).andStatusNotEqualTo(GrouponConstant.STATUS_NONE).andDeletedEqualTo(false);
        example.orderBy("add_time desc");
        return grouponMapper.selectByExample(example);
    }

    /**
     * 根据OrderId查询团购记录
     *
     * @param orderId
     * @return
     */
    @Cacheable(sync = true)
    public LitemallGroupon queryByOrderId(Integer orderId) {
        LitemallGrouponExample example = new LitemallGrouponExample();
        example.or().andOrderIdEqualTo(orderId).andDeletedEqualTo(false);
        return grouponMapper.selectOneByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallGroupon> queryList(Integer userId, Integer ruleId, Integer grouponId, Short status, Integer page, Integer size, String sort, String order) {
        LitemallGrouponExample example = new LitemallGrouponExample();
        LitemallGrouponExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);

        if (userId != null) {
            criteria.andUserIdEqualTo(userId);
        }
        if(ruleId != null){
            criteria.andRulesIdEqualTo(ruleId);
        }
        if (status != null) {
            criteria.andStatusEqualTo(status);
        }
        if(grouponId != null){
            criteria.andGrouponIdEqualTo(grouponId);
        }
        if (StringUtils.hasText(sort) && StringUtils.hasText(order)) {
            example.setOrderByClause(sort + " " + order);
        }
        if (ObjectUtils.isEmpty(page) && ObjectUtils.isEmpty(size)) {
            PageHelper.startPage(page, size);
        }
        return grouponMapper.selectByExample(example);
    }

    /**
     * 获取某个团购活动参与的记录
     *
     * @param grouponId
     * @return
     */
    @Cacheable(sync = true)
    public List<LitemallGroupon> queryJoinRecord(Integer grouponId) {
        LitemallGrouponExample example = new LitemallGrouponExample();
        example.or().andGrouponIdEqualTo(grouponId).andStatusEqualTo(GrouponConstant.STATUS_ON).andDeletedEqualTo(false);
        example.orderBy("add_time desc");
        return grouponMapper.selectByExample(example);
    }

    /**
     * 根据ID查询记录
     *
     * @param id
     * @return
     */
    @Cacheable(sync = true)
    public LitemallGroupon queryById(Integer id) {
        LitemallGrouponExample example = new LitemallGrouponExample();
        example.or().andIdEqualTo(id).andDeletedEqualTo(false);
        return grouponMapper.selectOneByExample(example);
    }

    /**
     * 根据ID查询记录
     *
     * @param userId
     * @param id
     * @return
     */
    @Cacheable(sync = true)
    public LitemallGroupon queryById(Integer userId, Integer id) {
        LitemallGrouponExample example = new LitemallGrouponExample();
        example.or().andIdEqualTo(id).andUserIdEqualTo(userId).andDeletedEqualTo(false);
        return grouponMapper.selectOneByExample(example);
    }

    /**
     * 返回某个发起的团购参与人数
     *
     * @param grouponId
     * @return
     */
    @Cacheable(sync = true)
    public int countGroupon(Integer grouponId) {
        LitemallGrouponExample example = new LitemallGrouponExample();
        example.or().andGrouponIdEqualTo(grouponId).andStatusNotEqualTo(GrouponConstant.STATUS_NONE).andDeletedEqualTo(false);
        return (int) grouponMapper.countByExample(example);
    }

    @Cacheable(sync = true)
    public boolean hasJoin(Integer userId, Integer grouponId) {
        LitemallGrouponExample example = new LitemallGrouponExample();
        example.or().andUserIdEqualTo(userId).andGrouponIdEqualTo(grouponId).andStatusNotEqualTo(GrouponConstant.STATUS_NONE).andDeletedEqualTo(false);
        return  grouponMapper.countByExample(example) != 0;
    }

    @CacheEvict(allEntries = true)
    public int updateById(LitemallGroupon groupon) {
        groupon.setUpdateTime(LocalDateTime.now());
        return grouponMapper.updateByPrimaryKeySelective(groupon);
    }

    @CacheEvict(allEntries = true)
    public int updateVersionSelective(LitemallGroupon groupon) {
        groupon.setUpdateTime(LocalDateTime.now());
        return grouponMapper.updateWithVersionByPrimaryKeySelective(groupon.getVersion(), groupon);
    }

    /**
     * 创建或参与一个团购
     *
     * @param groupon
     * @return
     */
    @CacheEvict(allEntries = true)
    public int createGroupon(LitemallGroupon groupon) {
        groupon.setAddTime(LocalDateTime.now());
        groupon.setUpdateTime(LocalDateTime.now());
        return grouponMapper.insertSelective(groupon);
    }


    /**
     * 查询所有发起的团购记录
     *
     * @param rulesId
     * @param page
     * @param size
     * @param sort
     * @param order
     * @return
     */
    @Cacheable(sync = true)
    public List<LitemallGroupon> querySelective(String rulesId, Integer page, Integer size, String sort, String order) {
        LitemallGrouponExample example = new LitemallGrouponExample();
        LitemallGrouponExample.Criteria criteria = example.createCriteria();

        if (StringUtils.hasText(rulesId)) {
            criteria.andRulesIdEqualTo(Integer.parseInt(rulesId));
        }
        criteria.andDeletedEqualTo(false);
        criteria.andStatusNotEqualTo(GrouponConstant.STATUS_NONE);
        criteria.andGrouponIdEqualTo(0);

        PageHelper.startPage(page, size);
        return grouponMapper.selectByExample(example);
    }

    @Cacheable(sync = true)
    public List<LitemallGroupon> queryByRuleId(int grouponRuleId) {
        LitemallGrouponExample example = new LitemallGrouponExample();
        example.or().andRulesIdEqualTo(grouponRuleId).andDeletedEqualTo(false);
        return grouponMapper.selectByExample(example);
    }
}
