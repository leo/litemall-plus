package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_order
 * Database Table Remarks : 
 *   订单表
 * @author ysling
 */
public class LitemallOrder implements Serializable {
    /**
     * litemall_order
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_order
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 订单表ID
     */
    private Integer id;

    /**
     * 用户表的用户ID
     */
    private Integer userId;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 店铺ID
     */
    private Integer brandId;

    /**
     * 订单状态
     */
    private Short orderStatus;

    /**
     * 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
     */
    private Short aftersaleStatus;

    /**
     * 收货人名称
     */
    private String consignee;

    /**
     * 收货人手机号
     */
    private String mobile;

    /**
     * 收货具体地址
     */
    private String address;

    /**
     * 用户订单留言
     */
    private String message;

    /**
     * 商品总费用
     */
    private BigDecimal goodsPrice;

    /**
     * 配送费用
     */
    private BigDecimal freightPrice;

    /**
     * 优惠券减免
     */
    private BigDecimal couponPrice;

    /**
     * 用户积分减免
     */
    private BigDecimal integralPrice;

    /**
     * 团购优惠价减免
     */
    private BigDecimal grouponPrice;

    /**
     * 订单提交时总费用
     */
    private BigDecimal orderPrice;

    /**
     * 实付费用， = goods_price + freight_price - grouponPrice - integral_price - coupon_price
     */
    private BigDecimal actualPrice;

    /**
     * 微信付款编号
     */
    private String payId;

    /**
     * 微信付款时间
     */
    private LocalDateTime payTime;

    /**
     * 微信转账批次单号
     */
    private String batchId;

    /**
     * 转账时间
     */
    private LocalDateTime batchTime;

    /**
     * 发货编号
     */
    private String shipSn;

    /**
     * 发货快递公司
     */
    private String shipChannel;

    /**
     * 发货开始时间
     */
    private LocalDateTime shipTime;

    /**
     * 实际退款金额，（有可能退款金额小于实际支付金额）
     */
    private BigDecimal refundAmount;

    /**
     * 退款方式
     */
    private String refundType;

    /**
     * 退款备注
     */
    private String refundContent;

    /**
     * 退款时间
     */
    private LocalDateTime refundTime;

    /**
     * 用户确认收货时间
     */
    private LocalDateTime confirmTime;

    /**
     * 待评价订单商品数量
     */
    private Short comments;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_order
     */
    private static final long serialVersionUID = 1L;

    /**
     * 订单表ID
     * @return id 订单表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 订单表ID
     * @param id 订单表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 用户表的用户ID
     * @return user_id 用户表的用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 用户表的用户ID
     * @param userId 用户表的用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 订单编号
     * @return order_sn 订单编号
     */
    public String getOrderSn() {
        return orderSn;
    }

    /**
     * 订单编号
     * @param orderSn 订单编号
     */
    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    /**
     * 店铺ID
     * @return brand_id 店铺ID
     */
    public Integer getBrandId() {
        return brandId;
    }

    /**
     * 店铺ID
     * @param brandId 店铺ID
     */
    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    /**
     * 订单状态
     * @return order_status 订单状态
     */
    public Short getOrderStatus() {
        return orderStatus;
    }

    /**
     * 订单状态
     * @param orderStatus 订单状态
     */
    public void setOrderStatus(Short orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
     * @return aftersale_status 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
     */
    public Short getAftersaleStatus() {
        return aftersaleStatus;
    }

    /**
     * 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
     * @param aftersaleStatus 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
     */
    public void setAftersaleStatus(Short aftersaleStatus) {
        this.aftersaleStatus = aftersaleStatus;
    }

    /**
     * 收货人名称
     * @return consignee 收货人名称
     */
    public String getConsignee() {
        return consignee;
    }

    /**
     * 收货人名称
     * @param consignee 收货人名称
     */
    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    /**
     * 收货人手机号
     * @return mobile 收货人手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 收货人手机号
     * @param mobile 收货人手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 收货具体地址
     * @return address 收货具体地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 收货具体地址
     * @param address 收货具体地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 用户订单留言
     * @return message 用户订单留言
     */
    public String getMessage() {
        return message;
    }

    /**
     * 用户订单留言
     * @param message 用户订单留言
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 商品总费用
     * @return goods_price 商品总费用
     */
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    /**
     * 商品总费用
     * @param goodsPrice 商品总费用
     */
    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    /**
     * 配送费用
     * @return freight_price 配送费用
     */
    public BigDecimal getFreightPrice() {
        return freightPrice;
    }

    /**
     * 配送费用
     * @param freightPrice 配送费用
     */
    public void setFreightPrice(BigDecimal freightPrice) {
        this.freightPrice = freightPrice;
    }

    /**
     * 优惠券减免
     * @return coupon_price 优惠券减免
     */
    public BigDecimal getCouponPrice() {
        return couponPrice;
    }

    /**
     * 优惠券减免
     * @param couponPrice 优惠券减免
     */
    public void setCouponPrice(BigDecimal couponPrice) {
        this.couponPrice = couponPrice;
    }

    /**
     * 用户积分减免
     * @return integral_price 用户积分减免
     */
    public BigDecimal getIntegralPrice() {
        return integralPrice;
    }

    /**
     * 用户积分减免
     * @param integralPrice 用户积分减免
     */
    public void setIntegralPrice(BigDecimal integralPrice) {
        this.integralPrice = integralPrice;
    }

    /**
     * 团购优惠价减免
     * @return groupon_price 团购优惠价减免
     */
    public BigDecimal getGrouponPrice() {
        return grouponPrice;
    }

    /**
     * 团购优惠价减免
     * @param grouponPrice 团购优惠价减免
     */
    public void setGrouponPrice(BigDecimal grouponPrice) {
        this.grouponPrice = grouponPrice;
    }

    /**
     * 订单提交时总费用
     * @return order_price 订单提交时总费用
     */
    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    /**
     * 订单提交时总费用
     * @param orderPrice 订单提交时总费用
     */
    public void setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }

    /**
     * 实付费用， = goods_price + freight_price - grouponPrice - integral_price - coupon_price
     * @return actual_price 实付费用， = goods_price + freight_price - grouponPrice - integral_price - coupon_price
     */
    public BigDecimal getActualPrice() {
        return actualPrice;
    }

    /**
     * 实付费用， = goods_price + freight_price - grouponPrice - integral_price - coupon_price
     * @param actualPrice 实付费用， = goods_price + freight_price - grouponPrice - integral_price - coupon_price
     */
    public void setActualPrice(BigDecimal actualPrice) {
        this.actualPrice = actualPrice;
    }

    /**
     * 微信付款编号
     * @return pay_id 微信付款编号
     */
    public String getPayId() {
        return payId;
    }

    /**
     * 微信付款编号
     * @param payId 微信付款编号
     */
    public void setPayId(String payId) {
        this.payId = payId;
    }

    /**
     * 微信付款时间
     * @return pay_time 微信付款时间
     */
    public LocalDateTime getPayTime() {
        return payTime;
    }

    /**
     * 微信付款时间
     * @param payTime 微信付款时间
     */
    public void setPayTime(LocalDateTime payTime) {
        this.payTime = payTime;
    }

    /**
     * 微信转账批次单号
     * @return batch_id 微信转账批次单号
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * 微信转账批次单号
     * @param batchId 微信转账批次单号
     */
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    /**
     * 转账时间
     * @return batch_time 转账时间
     */
    public LocalDateTime getBatchTime() {
        return batchTime;
    }

    /**
     * 转账时间
     * @param batchTime 转账时间
     */
    public void setBatchTime(LocalDateTime batchTime) {
        this.batchTime = batchTime;
    }

    /**
     * 发货编号
     * @return ship_sn 发货编号
     */
    public String getShipSn() {
        return shipSn;
    }

    /**
     * 发货编号
     * @param shipSn 发货编号
     */
    public void setShipSn(String shipSn) {
        this.shipSn = shipSn;
    }

    /**
     * 发货快递公司
     * @return ship_channel 发货快递公司
     */
    public String getShipChannel() {
        return shipChannel;
    }

    /**
     * 发货快递公司
     * @param shipChannel 发货快递公司
     */
    public void setShipChannel(String shipChannel) {
        this.shipChannel = shipChannel;
    }

    /**
     * 发货开始时间
     * @return ship_time 发货开始时间
     */
    public LocalDateTime getShipTime() {
        return shipTime;
    }

    /**
     * 发货开始时间
     * @param shipTime 发货开始时间
     */
    public void setShipTime(LocalDateTime shipTime) {
        this.shipTime = shipTime;
    }

    /**
     * 实际退款金额，（有可能退款金额小于实际支付金额）
     * @return refund_amount 实际退款金额，（有可能退款金额小于实际支付金额）
     */
    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    /**
     * 实际退款金额，（有可能退款金额小于实际支付金额）
     * @param refundAmount 实际退款金额，（有可能退款金额小于实际支付金额）
     */
    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    /**
     * 退款方式
     * @return refund_type 退款方式
     */
    public String getRefundType() {
        return refundType;
    }

    /**
     * 退款方式
     * @param refundType 退款方式
     */
    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    /**
     * 退款备注
     * @return refund_content 退款备注
     */
    public String getRefundContent() {
        return refundContent;
    }

    /**
     * 退款备注
     * @param refundContent 退款备注
     */
    public void setRefundContent(String refundContent) {
        this.refundContent = refundContent;
    }

    /**
     * 退款时间
     * @return refund_time 退款时间
     */
    public LocalDateTime getRefundTime() {
        return refundTime;
    }

    /**
     * 退款时间
     * @param refundTime 退款时间
     */
    public void setRefundTime(LocalDateTime refundTime) {
        this.refundTime = refundTime;
    }

    /**
     * 用户确认收货时间
     * @return confirm_time 用户确认收货时间
     */
    public LocalDateTime getConfirmTime() {
        return confirmTime;
    }

    /**
     * 用户确认收货时间
     * @param confirmTime 用户确认收货时间
     */
    public void setConfirmTime(LocalDateTime confirmTime) {
        this.confirmTime = confirmTime;
    }

    /**
     * 待评价订单商品数量
     * @return comments 待评价订单商品数量
     */
    public Short getComments() {
        return comments;
    }

    /**
     * 待评价订单商品数量
     * @param comments 待评价订单商品数量
     */
    public void setComments(Short comments) {
        this.comments = comments;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 订单表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 订单表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", orderSn=").append(orderSn);
        sb.append(", brandId=").append(brandId);
        sb.append(", orderStatus=").append(orderStatus);
        sb.append(", aftersaleStatus=").append(aftersaleStatus);
        sb.append(", consignee=").append(consignee);
        sb.append(", mobile=").append(mobile);
        sb.append(", address=").append(address);
        sb.append(", message=").append(message);
        sb.append(", goodsPrice=").append(goodsPrice);
        sb.append(", freightPrice=").append(freightPrice);
        sb.append(", couponPrice=").append(couponPrice);
        sb.append(", integralPrice=").append(integralPrice);
        sb.append(", grouponPrice=").append(grouponPrice);
        sb.append(", orderPrice=").append(orderPrice);
        sb.append(", actualPrice=").append(actualPrice);
        sb.append(", payId=").append(payId);
        sb.append(", payTime=").append(payTime);
        sb.append(", batchId=").append(batchId);
        sb.append(", batchTime=").append(batchTime);
        sb.append(", shipSn=").append(shipSn);
        sb.append(", shipChannel=").append(shipChannel);
        sb.append(", shipTime=").append(shipTime);
        sb.append(", refundAmount=").append(refundAmount);
        sb.append(", refundType=").append(refundType);
        sb.append(", refundContent=").append(refundContent);
        sb.append(", refundTime=").append(refundTime);
        sb.append(", confirmTime=").append(confirmTime);
        sb.append(", comments=").append(comments);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 订单表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallOrder other = (LitemallOrder) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getOrderSn() == null ? other.getOrderSn() == null : this.getOrderSn().equals(other.getOrderSn()))
            && (this.getBrandId() == null ? other.getBrandId() == null : this.getBrandId().equals(other.getBrandId()))
            && (this.getOrderStatus() == null ? other.getOrderStatus() == null : this.getOrderStatus().equals(other.getOrderStatus()))
            && (this.getAftersaleStatus() == null ? other.getAftersaleStatus() == null : this.getAftersaleStatus().equals(other.getAftersaleStatus()))
            && (this.getConsignee() == null ? other.getConsignee() == null : this.getConsignee().equals(other.getConsignee()))
            && (this.getMobile() == null ? other.getMobile() == null : this.getMobile().equals(other.getMobile()))
            && (this.getAddress() == null ? other.getAddress() == null : this.getAddress().equals(other.getAddress()))
            && (this.getMessage() == null ? other.getMessage() == null : this.getMessage().equals(other.getMessage()))
            && (this.getGoodsPrice() == null ? other.getGoodsPrice() == null : this.getGoodsPrice().equals(other.getGoodsPrice()))
            && (this.getFreightPrice() == null ? other.getFreightPrice() == null : this.getFreightPrice().equals(other.getFreightPrice()))
            && (this.getCouponPrice() == null ? other.getCouponPrice() == null : this.getCouponPrice().equals(other.getCouponPrice()))
            && (this.getIntegralPrice() == null ? other.getIntegralPrice() == null : this.getIntegralPrice().equals(other.getIntegralPrice()))
            && (this.getGrouponPrice() == null ? other.getGrouponPrice() == null : this.getGrouponPrice().equals(other.getGrouponPrice()))
            && (this.getOrderPrice() == null ? other.getOrderPrice() == null : this.getOrderPrice().equals(other.getOrderPrice()))
            && (this.getActualPrice() == null ? other.getActualPrice() == null : this.getActualPrice().equals(other.getActualPrice()))
            && (this.getPayId() == null ? other.getPayId() == null : this.getPayId().equals(other.getPayId()))
            && (this.getPayTime() == null ? other.getPayTime() == null : this.getPayTime().equals(other.getPayTime()))
            && (this.getBatchId() == null ? other.getBatchId() == null : this.getBatchId().equals(other.getBatchId()))
            && (this.getBatchTime() == null ? other.getBatchTime() == null : this.getBatchTime().equals(other.getBatchTime()))
            && (this.getShipSn() == null ? other.getShipSn() == null : this.getShipSn().equals(other.getShipSn()))
            && (this.getShipChannel() == null ? other.getShipChannel() == null : this.getShipChannel().equals(other.getShipChannel()))
            && (this.getShipTime() == null ? other.getShipTime() == null : this.getShipTime().equals(other.getShipTime()))
            && (this.getRefundAmount() == null ? other.getRefundAmount() == null : this.getRefundAmount().equals(other.getRefundAmount()))
            && (this.getRefundType() == null ? other.getRefundType() == null : this.getRefundType().equals(other.getRefundType()))
            && (this.getRefundContent() == null ? other.getRefundContent() == null : this.getRefundContent().equals(other.getRefundContent()))
            && (this.getRefundTime() == null ? other.getRefundTime() == null : this.getRefundTime().equals(other.getRefundTime()))
            && (this.getConfirmTime() == null ? other.getConfirmTime() == null : this.getConfirmTime().equals(other.getConfirmTime()))
            && (this.getComments() == null ? other.getComments() == null : this.getComments().equals(other.getComments()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * 订单表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getOrderSn() == null) ? 0 : getOrderSn().hashCode());
        result = prime * result + ((getBrandId() == null) ? 0 : getBrandId().hashCode());
        result = prime * result + ((getOrderStatus() == null) ? 0 : getOrderStatus().hashCode());
        result = prime * result + ((getAftersaleStatus() == null) ? 0 : getAftersaleStatus().hashCode());
        result = prime * result + ((getConsignee() == null) ? 0 : getConsignee().hashCode());
        result = prime * result + ((getMobile() == null) ? 0 : getMobile().hashCode());
        result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
        result = prime * result + ((getMessage() == null) ? 0 : getMessage().hashCode());
        result = prime * result + ((getGoodsPrice() == null) ? 0 : getGoodsPrice().hashCode());
        result = prime * result + ((getFreightPrice() == null) ? 0 : getFreightPrice().hashCode());
        result = prime * result + ((getCouponPrice() == null) ? 0 : getCouponPrice().hashCode());
        result = prime * result + ((getIntegralPrice() == null) ? 0 : getIntegralPrice().hashCode());
        result = prime * result + ((getGrouponPrice() == null) ? 0 : getGrouponPrice().hashCode());
        result = prime * result + ((getOrderPrice() == null) ? 0 : getOrderPrice().hashCode());
        result = prime * result + ((getActualPrice() == null) ? 0 : getActualPrice().hashCode());
        result = prime * result + ((getPayId() == null) ? 0 : getPayId().hashCode());
        result = prime * result + ((getPayTime() == null) ? 0 : getPayTime().hashCode());
        result = prime * result + ((getBatchId() == null) ? 0 : getBatchId().hashCode());
        result = prime * result + ((getBatchTime() == null) ? 0 : getBatchTime().hashCode());
        result = prime * result + ((getShipSn() == null) ? 0 : getShipSn().hashCode());
        result = prime * result + ((getShipChannel() == null) ? 0 : getShipChannel().hashCode());
        result = prime * result + ((getShipTime() == null) ? 0 : getShipTime().hashCode());
        result = prime * result + ((getRefundAmount() == null) ? 0 : getRefundAmount().hashCode());
        result = prime * result + ((getRefundType() == null) ? 0 : getRefundType().hashCode());
        result = prime * result + ((getRefundContent() == null) ? 0 : getRefundContent().hashCode());
        result = prime * result + ((getRefundTime() == null) ? 0 : getRefundTime().hashCode());
        result = prime * result + ((getConfirmTime() == null) ? 0 : getConfirmTime().hashCode());
        result = prime * result + ((getComments() == null) ? 0 : getComments().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_order
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_order
         */
        private final Boolean value;

        /**
         * litemall_order
         */
        private final String name;

        /**
         * 订单表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 订单表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 订单表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 订单表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 订单表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 订单表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_order
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        userId("user_id", "userId", "INTEGER", false),
        orderSn("order_sn", "orderSn", "VARCHAR", false),
        brandId("brand_id", "brandId", "INTEGER", false),
        orderStatus("order_status", "orderStatus", "SMALLINT", false),
        aftersaleStatus("aftersale_status", "aftersaleStatus", "SMALLINT", false),
        consignee("consignee", "consignee", "VARCHAR", false),
        mobile("mobile", "mobile", "VARCHAR", false),
        address("address", "address", "VARCHAR", false),
        message("message", "message", "VARCHAR", false),
        goodsPrice("goods_price", "goodsPrice", "DECIMAL", false),
        freightPrice("freight_price", "freightPrice", "DECIMAL", false),
        couponPrice("coupon_price", "couponPrice", "DECIMAL", false),
        integralPrice("integral_price", "integralPrice", "DECIMAL", false),
        grouponPrice("groupon_price", "grouponPrice", "DECIMAL", false),
        orderPrice("order_price", "orderPrice", "DECIMAL", false),
        actualPrice("actual_price", "actualPrice", "DECIMAL", false),
        payId("pay_id", "payId", "VARCHAR", false),
        payTime("pay_time", "payTime", "TIMESTAMP", false),
        batchId("batch_id", "batchId", "VARCHAR", false),
        batchTime("batch_time", "batchTime", "TIMESTAMP", false),
        shipSn("ship_sn", "shipSn", "VARCHAR", false),
        shipChannel("ship_channel", "shipChannel", "VARCHAR", false),
        shipTime("ship_time", "shipTime", "TIMESTAMP", false),
        refundAmount("refund_amount", "refundAmount", "DECIMAL", false),
        refundType("refund_type", "refundType", "VARCHAR", false),
        refundContent("refund_content", "refundContent", "VARCHAR", false),
        refundTime("refund_time", "refundTime", "TIMESTAMP", false),
        confirmTime("confirm_time", "confirmTime", "TIMESTAMP", false),
        comments("comments", "comments", "SMALLINT", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_order
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_order
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_order
         */
        private final String column;

        /**
         * litemall_order
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_order
         */
        private final String javaProperty;

        /**
         * litemall_order
         */
        private final String jdbcType;

        /**
         * 订单表
         */
        public String value() {
            return this.column;
        }

        /**
         * 订单表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 订单表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 订单表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 订单表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 订单表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 订单表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 订单表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 订单表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 订单表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 订单表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}