package org.ysling.litemall.db.constant;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import org.ysling.litemall.db.domain.LitemallAftersale;

import java.io.Serializable;

public class AftersaleConstant implements Serializable {
    /**可申请*/
    public static final Short STATUS_INIT = 0;
    /**用户已申请*/
    public static final Short STATUS_REQUEST = 1;
    /**管理员审核通过*/
    public static final Short STATUS_RECEPT = 2;
    /**管理员退款成功*/
    public static final Short STATUS_REFUND = 3;
    /**管理员审核拒绝*/
    public static final Short STATUS_REJECT = 4;
    /**用户已取消*/
    public static final Short STATUS_CANCEL = 5;

    /**未收货退款*/
    public static final Short TYPE_GOODS_MISS = 0;
    /**已收货（无需退货）退款*/
    public static final Short TYPE_GOODS_NEEDLESS = 1;
    /**退货退款*/
    public static final Short TYPE_GOODS_REQUIRED = 2;

    public static String statusText(LitemallAftersale aftersale) {
        Short status = aftersale.getStatus();

        if (status.equals(STATUS_INIT)) {
            return "可申请";
        }
        if (status.equals(STATUS_REQUEST)) {
            return "已申请";
        }
        if (status.equals(STATUS_RECEPT)) {
            return "审核通过";
        }
        if (status.equals(STATUS_REFUND)) {
            return "退款成功";
        }
        if (status.equals(STATUS_REJECT)) {
            return "审核拒绝";
        }
        if (status.equals(STATUS_CANCEL)) {
            return "用户取消";
        }
        throw new IllegalStateException("grouponStatus不支持");
    }
}
