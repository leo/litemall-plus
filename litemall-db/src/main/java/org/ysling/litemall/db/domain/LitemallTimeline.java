package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_timeline
 * Database Table Remarks : 
 *   动态表
 * @author ysling
 */
public class LitemallTimeline implements Serializable {
    /**
     * litemall_timeline
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_timeline
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 动态表ID
     */
    private Integer id;

    /**
     * 用户表的用户ID
     */
    private Integer userId;

    /**
     * 发表内容
     */
    private String content;

    /**
     * 图片地址列表，采用JSON数组格式
     */
    private String[] picUrls;

    /**
     * 点赞量
     */
    private Long thumbUp;

    /**
     * 访问量
     */
    private Long lookNumber;

    /**
     * 是否管理员，0不是，1是
     */
    private Boolean isAdmin;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_timeline
     */
    private static final long serialVersionUID = 1L;

    /**
     * 动态表ID
     * @return id 动态表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 动态表ID
     * @param id 动态表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 用户表的用户ID
     * @return user_id 用户表的用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 用户表的用户ID
     * @param userId 用户表的用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 发表内容
     * @return content 发表内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 发表内容
     * @param content 发表内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 图片地址列表，采用JSON数组格式
     * @return pic_urls 图片地址列表，采用JSON数组格式
     */
    public String[] getPicUrls() {
        return picUrls;
    }

    /**
     * 图片地址列表，采用JSON数组格式
     * @param picUrls 图片地址列表，采用JSON数组格式
     */
    public void setPicUrls(String[] picUrls) {
        this.picUrls = picUrls;
    }

    /**
     * 点赞量
     * @return thumb_up 点赞量
     */
    public Long getThumbUp() {
        return thumbUp;
    }

    /**
     * 点赞量
     * @param thumbUp 点赞量
     */
    public void setThumbUp(Long thumbUp) {
        this.thumbUp = thumbUp;
    }

    /**
     * 访问量
     * @return look_number 访问量
     */
    public Long getLookNumber() {
        return lookNumber;
    }

    /**
     * 访问量
     * @param lookNumber 访问量
     */
    public void setLookNumber(Long lookNumber) {
        this.lookNumber = lookNumber;
    }

    /**
     * 是否管理员，0不是，1是
     * @return is_admin 是否管理员，0不是，1是
     */
    public Boolean getIsAdmin() {
        return isAdmin;
    }

    /**
     * 是否管理员，0不是，1是
     * @param isAdmin 是否管理员，0不是，1是
     */
    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 动态表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 动态表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", content=").append(content);
        sb.append(", picUrls=").append(picUrls);
        sb.append(", thumbUp=").append(thumbUp);
        sb.append(", lookNumber=").append(lookNumber);
        sb.append(", isAdmin=").append(isAdmin);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 动态表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallTimeline other = (LitemallTimeline) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
            && (Arrays.equals(this.getPicUrls(), other.getPicUrls()))
            && (this.getThumbUp() == null ? other.getThumbUp() == null : this.getThumbUp().equals(other.getThumbUp()))
            && (this.getLookNumber() == null ? other.getLookNumber() == null : this.getLookNumber().equals(other.getLookNumber()))
            && (this.getIsAdmin() == null ? other.getIsAdmin() == null : this.getIsAdmin().equals(other.getIsAdmin()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * 动态表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + (Arrays.hashCode(getPicUrls()));
        result = prime * result + ((getThumbUp() == null) ? 0 : getThumbUp().hashCode());
        result = prime * result + ((getLookNumber() == null) ? 0 : getLookNumber().hashCode());
        result = prime * result + ((getIsAdmin() == null) ? 0 : getIsAdmin().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_timeline
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_timeline
         */
        private final Boolean value;

        /**
         * litemall_timeline
         */
        private final String name;

        /**
         * 动态表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 动态表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 动态表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 动态表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 动态表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 动态表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_timeline
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        userId("user_id", "userId", "INTEGER", false),
        content("content", "content", "VARCHAR", false),
        picUrls("pic_urls", "picUrls", "VARCHAR", false),
        thumbUp("thumb_up", "thumbUp", "BIGINT", false),
        lookNumber("look_number", "lookNumber", "BIGINT", false),
        isAdmin("is_admin", "isAdmin", "BIT", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_timeline
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_timeline
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_timeline
         */
        private final String column;

        /**
         * litemall_timeline
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_timeline
         */
        private final String javaProperty;

        /**
         * litemall_timeline
         */
        private final String jdbcType;

        /**
         * 动态表
         */
        public String value() {
            return this.column;
        }

        /**
         * 动态表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 动态表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 动态表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 动态表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 动态表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 动态表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 动态表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 动态表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 动态表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 动态表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}