package org.ysling.litemall.db.constant;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import org.ysling.litemall.db.domain.LitemallGroupon;

import java.io.Serializable;

public class GrouponConstant implements Serializable {
    /**团购规则正常*/
    public static final Short RULE_STATUS_ON = 0;
    /**团购规则到期下线*/
    public static final Short RULE_STATUS_DOWN_EXPIRE = 1;
    /**团购规则提前下线*/
    public static final Short RULE_STATUS_DOWN_ADMIN = 2;


    /**待开团（未支付）*/
    public static final Short STATUS_NONE = 0;
    /**团购中（已支付）*/
    public static final Short STATUS_ON = 1;
    /**团购失败（待退款）*/
    public static final Short STATUS_FAIL = 2;
    /**团购成功（待发货）*/
    public static final Short STATUS_SUCCEED = 3;
    /**团购取消（未支付）*/
    public static final Short STATUS_CANCEL = 4;

    public static String GrouponStatusText(LitemallGroupon groupon) {
        Short status = groupon.getStatus();

        if (status.equals(STATUS_CANCEL)) {
            return "取消团购";
        }
        if (status.equals(STATUS_NONE)) {
            return "待开团";
        }
        if (status.equals(STATUS_ON)) {
            return "团购中";
        }
        if (status.equals(STATUS_FAIL)) {
            return "团购失败";
        }
        if (status.equals(STATUS_SUCCEED)) {
            return "团购成功";
        }
        throw new IllegalStateException("grouponStatus不支持");
    }
}
