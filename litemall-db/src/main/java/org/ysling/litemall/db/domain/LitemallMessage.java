package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_message
 * Database Table Remarks : 
 *   webSocket消息
 * @author ysling
 */
public class LitemallMessage implements Serializable {
    /**
     * litemall_message
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_message
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * socket消息ID
     */
    private Integer id;

    /**
     * 发送方头像图片
     */
    private String avatar;

    /**
     * 发送方昵称
     */
    private String nickname;

    /**
     * 发送方用户ID
     */
    private Integer sendUserId;

    /**
     * 接收方用户ID
     */
    private Integer receiveUserId;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 图片地址列表，采用JSON数组格式
     */
    private String[] picUrls;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_message
     */
    private static final long serialVersionUID = 1L;

    /**
     * socket消息ID
     * @return id socket消息ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * socket消息ID
     * @param id socket消息ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 发送方头像图片
     * @return avatar 发送方头像图片
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * 发送方头像图片
     * @param avatar 发送方头像图片
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * 发送方昵称
     * @return nickname 发送方昵称
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * 发送方昵称
     * @param nickname 发送方昵称
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * 发送方用户ID
     * @return send_user_id 发送方用户ID
     */
    public Integer getSendUserId() {
        return sendUserId;
    }

    /**
     * 发送方用户ID
     * @param sendUserId 发送方用户ID
     */
    public void setSendUserId(Integer sendUserId) {
        this.sendUserId = sendUserId;
    }

    /**
     * 接收方用户ID
     * @return receive_user_id 接收方用户ID
     */
    public Integer getReceiveUserId() {
        return receiveUserId;
    }

    /**
     * 接收方用户ID
     * @param receiveUserId 接收方用户ID
     */
    public void setReceiveUserId(Integer receiveUserId) {
        this.receiveUserId = receiveUserId;
    }

    /**
     * 消息内容
     * @return content 消息内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 消息内容
     * @param content 消息内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 图片地址列表，采用JSON数组格式
     * @return pic_urls 图片地址列表，采用JSON数组格式
     */
    public String[] getPicUrls() {
        return picUrls;
    }

    /**
     * 图片地址列表，采用JSON数组格式
     * @param picUrls 图片地址列表，采用JSON数组格式
     */
    public void setPicUrls(String[] picUrls) {
        this.picUrls = picUrls;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * webSocket消息
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * webSocket消息
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", avatar=").append(avatar);
        sb.append(", nickname=").append(nickname);
        sb.append(", sendUserId=").append(sendUserId);
        sb.append(", receiveUserId=").append(receiveUserId);
        sb.append(", content=").append(content);
        sb.append(", picUrls=").append(picUrls);
        sb.append(", addTime=").append(addTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * webSocket消息
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallMessage other = (LitemallMessage) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getAvatar() == null ? other.getAvatar() == null : this.getAvatar().equals(other.getAvatar()))
            && (this.getNickname() == null ? other.getNickname() == null : this.getNickname().equals(other.getNickname()))
            && (this.getSendUserId() == null ? other.getSendUserId() == null : this.getSendUserId().equals(other.getSendUserId()))
            && (this.getReceiveUserId() == null ? other.getReceiveUserId() == null : this.getReceiveUserId().equals(other.getReceiveUserId()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
            && (Arrays.equals(this.getPicUrls(), other.getPicUrls()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * webSocket消息
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAvatar() == null) ? 0 : getAvatar().hashCode());
        result = prime * result + ((getNickname() == null) ? 0 : getNickname().hashCode());
        result = prime * result + ((getSendUserId() == null) ? 0 : getSendUserId().hashCode());
        result = prime * result + ((getReceiveUserId() == null) ? 0 : getReceiveUserId().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + (Arrays.hashCode(getPicUrls()));
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_message
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_message
         */
        private final Boolean value;

        /**
         * litemall_message
         */
        private final String name;

        /**
         * webSocket消息
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * webSocket消息
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * webSocket消息
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * webSocket消息
         */
        public String getName() {
            return this.name;
        }

        /**
         * webSocket消息
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * webSocket消息
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_message
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        avatar("avatar", "avatar", "VARCHAR", false),
        nickname("nickname", "nickname", "VARCHAR", false),
        sendUserId("send_user_id", "sendUserId", "INTEGER", false),
        receiveUserId("receive_user_id", "receiveUserId", "INTEGER", false),
        content("content", "content", "VARCHAR", false),
        picUrls("pic_urls", "picUrls", "VARCHAR", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_message
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_message
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_message
         */
        private final String column;

        /**
         * litemall_message
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_message
         */
        private final String javaProperty;

        /**
         * litemall_message
         */
        private final String jdbcType;

        /**
         * webSocket消息
         */
        public String value() {
            return this.column;
        }

        /**
         * webSocket消息
         */
        public String getValue() {
            return this.column;
        }

        /**
         * webSocket消息
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * webSocket消息
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * webSocket消息
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * webSocket消息
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * webSocket消息
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * webSocket消息
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * webSocket消息
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * webSocket消息
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * webSocket消息
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}