package org.ysling.litemall.db.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Database Table : litemall_reward
 * Database Table Remarks : 
 *   赏金任务参与表
 * @author ysling
 */
public class LitemallReward implements Serializable {
    /**
     * litemall_reward
     */
    public static final Boolean IS_DELETED = Deleted.IS_DELETED.value();

    /**
     * litemall_reward
     */
    public static final Boolean NOT_DELETED = Deleted.NOT_DELETED.value();

    /**
     * 赏金任务参与表ID
     */
    private Integer id;

    /**
     * 赏金任务的ID
     */
    private Integer taskId;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 订单ID
     */
    private Integer orderId;

    /**
     * 如果是分享用户，则reward_id是0；如果是活动参与用户，则reward_id是活动ID
     */
    private Integer rewardId;

    /**
     * 奖励金额
     */
    private BigDecimal award;

    /**
     * 赏金分享图片地址
     */
    private String shareUrl;

    /**
     * 分享用户ID
     */
    private Integer creatorUserId;

    /**
     * 赏金任务状态，0待加入，1加入成功，2取消加入
     */
    private Short status;

    /**
     * 微信转账批次单号
     */
    private String batchId;

    /**
     * 创建时间
     */
    private LocalDateTime addTime;

    /**
     * 转账时间
     */
    private LocalDateTime batchTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    /**
     * 租户ID，用于分割多个租户
     */
    private Integer tenantId;

    /**
     * 更新版本号
     */
    private Integer version;

    /**
     * litemall_reward
     */
    private static final long serialVersionUID = 1L;

    /**
     * 赏金任务参与表ID
     * @return id 赏金任务参与表ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 赏金任务参与表ID
     * @param id 赏金任务参与表ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 赏金任务的ID
     * @return task_id 赏金任务的ID
     */
    public Integer getTaskId() {
        return taskId;
    }

    /**
     * 赏金任务的ID
     * @param taskId 赏金任务的ID
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    /**
     * 用户ID
     * @return user_id 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 用户ID
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 订单ID
     * @return order_id 订单ID
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 订单ID
     * @param orderId 订单ID
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 如果是分享用户，则reward_id是0；如果是活动参与用户，则reward_id是活动ID
     * @return reward_id 如果是分享用户，则reward_id是0；如果是活动参与用户，则reward_id是活动ID
     */
    public Integer getRewardId() {
        return rewardId;
    }

    /**
     * 如果是分享用户，则reward_id是0；如果是活动参与用户，则reward_id是活动ID
     * @param rewardId 如果是分享用户，则reward_id是0；如果是活动参与用户，则reward_id是活动ID
     */
    public void setRewardId(Integer rewardId) {
        this.rewardId = rewardId;
    }

    /**
     * 奖励金额
     * @return award 奖励金额
     */
    public BigDecimal getAward() {
        return award;
    }

    /**
     * 奖励金额
     * @param award 奖励金额
     */
    public void setAward(BigDecimal award) {
        this.award = award;
    }

    /**
     * 赏金分享图片地址
     * @return share_url 赏金分享图片地址
     */
    public String getShareUrl() {
        return shareUrl;
    }

    /**
     * 赏金分享图片地址
     * @param shareUrl 赏金分享图片地址
     */
    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    /**
     * 分享用户ID
     * @return creator_user_id 分享用户ID
     */
    public Integer getCreatorUserId() {
        return creatorUserId;
    }

    /**
     * 分享用户ID
     * @param creatorUserId 分享用户ID
     */
    public void setCreatorUserId(Integer creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    /**
     * 赏金任务状态，0待加入，1加入成功，2取消加入
     * @return status 赏金任务状态，0待加入，1加入成功，2取消加入
     */
    public Short getStatus() {
        return status;
    }

    /**
     * 赏金任务状态，0待加入，1加入成功，2取消加入
     * @param status 赏金任务状态，0待加入，1加入成功，2取消加入
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * 微信转账批次单号
     * @return batch_id 微信转账批次单号
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * 微信转账批次单号
     * @param batchId 微信转账批次单号
     */
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    /**
     * 创建时间
     * @return add_time 创建时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 创建时间
     * @param addTime 创建时间
     */
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 转账时间
     * @return batch_time 转账时间
     */
    public LocalDateTime getBatchTime() {
        return batchTime;
    }

    /**
     * 转账时间
     * @param batchTime 转账时间
     */
    public void setBatchTime(LocalDateTime batchTime) {
        this.batchTime = batchTime;
    }

    /**
     * 更新时间
     * @return update_time 更新时间
     */
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 赏金任务参与表
     */
    public void andLogicalDeleted(boolean deleted) {
        setDeleted(deleted ? Deleted.IS_DELETED.value() : Deleted.NOT_DELETED.value());
    }

    /**
     * 逻辑删除
     * @return deleted 逻辑删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 逻辑删除
     * @param deleted 逻辑删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * 租户ID，用于分割多个租户
     * @return tenant_id 租户ID，用于分割多个租户
     */
    public Integer getTenantId() {
        return tenantId;
    }

    /**
     * 租户ID，用于分割多个租户
     * @param tenantId 租户ID，用于分割多个租户
     */
    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 更新版本号
     * @return version 更新版本号
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * 更新版本号
     * @param version 更新版本号
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * 赏金任务参与表
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", id=").append(id);
        sb.append(", taskId=").append(taskId);
        sb.append(", userId=").append(userId);
        sb.append(", orderId=").append(orderId);
        sb.append(", rewardId=").append(rewardId);
        sb.append(", award=").append(award);
        sb.append(", shareUrl=").append(shareUrl);
        sb.append(", creatorUserId=").append(creatorUserId);
        sb.append(", status=").append(status);
        sb.append(", batchId=").append(batchId);
        sb.append(", addTime=").append(addTime);
        sb.append(", batchTime=").append(batchTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", deleted=").append(deleted);
        sb.append(", tenantId=").append(tenantId);
        sb.append(", version=").append(version);
        sb.append("]");
        return sb.toString();
    }

    /**
     * 赏金任务参与表
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LitemallReward other = (LitemallReward) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTaskId() == null ? other.getTaskId() == null : this.getTaskId().equals(other.getTaskId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getRewardId() == null ? other.getRewardId() == null : this.getRewardId().equals(other.getRewardId()))
            && (this.getAward() == null ? other.getAward() == null : this.getAward().equals(other.getAward()))
            && (this.getShareUrl() == null ? other.getShareUrl() == null : this.getShareUrl().equals(other.getShareUrl()))
            && (this.getCreatorUserId() == null ? other.getCreatorUserId() == null : this.getCreatorUserId().equals(other.getCreatorUserId()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getBatchId() == null ? other.getBatchId() == null : this.getBatchId().equals(other.getBatchId()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getBatchTime() == null ? other.getBatchTime() == null : this.getBatchTime().equals(other.getBatchTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getDeleted() == null ? other.getDeleted() == null : this.getDeleted().equals(other.getDeleted()))
            && (this.getTenantId() == null ? other.getTenantId() == null : this.getTenantId().equals(other.getTenantId()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    /**
     * 赏金任务参与表
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTaskId() == null) ? 0 : getTaskId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getRewardId() == null) ? 0 : getRewardId().hashCode());
        result = prime * result + ((getAward() == null) ? 0 : getAward().hashCode());
        result = prime * result + ((getShareUrl() == null) ? 0 : getShareUrl().hashCode());
        result = prime * result + ((getCreatorUserId() == null) ? 0 : getCreatorUserId().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getBatchId() == null) ? 0 : getBatchId().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getBatchTime() == null) ? 0 : getBatchTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getDeleted() == null) ? 0 : getDeleted().hashCode());
        result = prime * result + ((getTenantId() == null) ? 0 : getTenantId().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    /**
     * litemall_reward
     */
    public enum Deleted {
        NOT_DELETED(new Boolean("0"), "未删除"),
        IS_DELETED(new Boolean("1"), "已删除");

        /**
         * litemall_reward
         */
        private final Boolean value;

        /**
         * litemall_reward
         */
        private final String name;

        /**
         * 赏金任务参与表
         */
        Deleted(Boolean value, String name) {
            this.value = value;
            this.name = name;
        }

        /**
         * 赏金任务参与表
         */
        public Boolean getValue() {
            return this.value;
        }

        /**
         * 赏金任务参与表
         */
        public Boolean value() {
            return this.value;
        }

        /**
         * 赏金任务参与表
         */
        public String getName() {
            return this.name;
        }

        /**
         * 赏金任务参与表
         */
        public static Deleted parseValue(Boolean value) {
            if (value != null) {
                for (Deleted item : values()) {
                    if (item.value.equals(value)) {
                        return item;
                    }
                }
            }
            return null;
        }

        /**
         * 赏金任务参与表
         */
        public static Deleted parseName(String name) {
            if (name != null) {
                for (Deleted item : values()) {
                    if (item.name.equals(name)) {
                        return item;
                    }
                }
            }
            return null;
        }
    }

    /**
     * litemall_reward
     */
    public enum Column {
        id("id", "id", "INTEGER", false),
        taskId("task_id", "taskId", "INTEGER", false),
        userId("user_id", "userId", "INTEGER", false),
        orderId("order_id", "orderId", "INTEGER", false),
        rewardId("reward_id", "rewardId", "INTEGER", false),
        award("award", "award", "DECIMAL", false),
        shareUrl("share_url", "shareUrl", "VARCHAR", false),
        creatorUserId("creator_user_id", "creatorUserId", "INTEGER", false),
        status("status", "status", "SMALLINT", true),
        batchId("batch_id", "batchId", "VARCHAR", false),
        addTime("add_time", "addTime", "TIMESTAMP", false),
        batchTime("batch_time", "batchTime", "TIMESTAMP", false),
        updateTime("update_time", "updateTime", "TIMESTAMP", false),
        deleted("deleted", "deleted", "BIT", false),
        tenantId("tenant_id", "tenantId", "INTEGER", false),
        version("version", "version", "INTEGER", false);

        /**
         * litemall_reward
         */
        private static final String BEGINNING_DELIMITER = "`";

        /**
         * litemall_reward
         */
        private static final String ENDING_DELIMITER = "`";

        /**
         * litemall_reward
         */
        private final String column;

        /**
         * litemall_reward
         */
        private final boolean isColumnNameDelimited;

        /**
         * litemall_reward
         */
        private final String javaProperty;

        /**
         * litemall_reward
         */
        private final String jdbcType;

        /**
         * 赏金任务参与表
         */
        public String value() {
            return this.column;
        }

        /**
         * 赏金任务参与表
         */
        public String getValue() {
            return this.column;
        }

        /**
         * 赏金任务参与表
         */
        public String getJavaProperty() {
            return this.javaProperty;
        }

        /**
         * 赏金任务参与表
         */
        public String getJdbcType() {
            return this.jdbcType;
        }

        /**
         * 赏金任务参与表
         */
        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        /**
         * 赏金任务参与表
         */
        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        /**
         * 赏金任务参与表
         */
        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        /**
         * 赏金任务参与表
         */
        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        /**
         * 赏金任务参与表
         */
        public static Column[] all() {
            return Column.values();
        }

        /**
         * 赏金任务参与表
         */
        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        /**
         * 赏金任务参与表
         */
        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}