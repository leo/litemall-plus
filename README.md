# litemall-plus介绍

* 对【[litemall](https://gitee.com/linlinjava/litemall) 】的全面优化，原litemall主要面向单体商户（B2C模式）
* 经改良后litemall-plus则是（平台管理员->小程序店铺->用户）（B2C2C模式）。
* 连锁平台请进群资讯群主

#### 1.微信小程序的ui界面的优化，
* 其主要依赖于[colorui](http://docs.xzeu.com/#/info/%E5%BF%AB%E9%80%9F%E5%BC%80%E5%A7%8B/%E5%BF%AB%E9%80%9F%E5%B8%83%E7%BD%B2) ，目前小程序前端ui优化基本完成，大家可自行扫码阅览
* 微信小程序商城演示
* <img src="./doc/pics/wxxcx.jpg" width="344" alt="小程序商城">
#### 2.微信小程序功能新增
* 1.动态发布功能，2.动态列表功能，
* 3.管理员登陆功能，4.商家小程序端发货及退款功能，
* 5.消息订阅功能（发货，退款，新订单），
* 6.个人中心滑动抽屉功能，右划
* 7.购物车规格选择功能，
* 8.多商户功能，个人店铺添加，个人店铺修改
* 9.店铺商品上传（上传后需通过审核才能上线），店铺商品修改，店铺商品删除
* 10.店铺订单查询，店铺订单通知
* 11.订单拆分完成，按付款商品生成订单
* 12.用户确认收货后将订单付款金额发送到卖家零钱
* 13.添加redis缓存，添加全局事务管理
* 15.添加赏金（变相分销，通过分享商品获取奖励，奖励从分享进入的销售订单中扣除，发给分享者）
* 16.添加小程序内部聊天功能
* 17.添加小程序订单物流查询：快递鸟，微信物流查询插件

#### 3.微信小程序功能优化
* 1.评论优化，2.团购优化，3.专题优化，4.订单状态优化，5.优惠券优化

#### 4.微信小程序功能待开发
* 3.添加小程序我的赏金查看
* 5.添加我的发布的日常查看

## 项目文档

* [API](./doc/api.md)
* [数据库](./doc/database.md)
* [常见问题FAQ](./doc/FAQ.md)
* [最佳后台设计](./doc/best-admin.md)
* [1. 系统架构](./doc/project.md)
* [2. 基础系统](./doc/platform.md)
* [3. 小商场](./doc/wxmall.md)
* [4. 管理后台](./doc/admin.md)

## 项目实例

### 1.管理后台实例

![](./doc/pics/admin-dashboard.png)

1. 浏览器打开，输入以下网址: [https://litemall-plus.ysling.site](https://litemall-plus.ysling.site)
2. 平台管理员  用户名：`927069313@qq.com`，管理员密码：`admin123`
3. 租户管理员  用户名：`2024511631@qq.com`，管理员密码：`admin123`
> 注意：此实例只是测试管理后台。

## 项目架构
![](./doc/pics/project-structure.png)

Spring Boot后端 + Vue管理员前端 + 微信小程序用户前端 + Vue用户移动端

## 技术栈

> 1. Spring Boot
> 2. Vue
> 3. 微信小程序

![](doc/pics/technology-stack.png)

## 快速启动

1. 配置最小开发环境：
    * [MySQL](https://dev.mysql.com/downloads/mysql/)
    * [JDK1.8或以上](http://www.oracle.com/technetwork/java/javase/overview/index.html)
    * [Maven](https://maven.apache.org/download.cgi)
    * [Nodejs](https://nodejs.org/en/download/)
    * [微信开发者工具](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)
    
2. 数据库直接导入litemall-db/sql下的数据库文件
    * litemall-plus.sql

3. 启动小商场和管理后台的后端服务

    打开命令行，输入以下命令
    ```bash
    cd litemall-plus
    mvn install
    mvn clean package
    java -Dfile.encoding=UTF-8 -jar litemall-all/target/litemall-all-0.1.0-exec.jar
    ```
    
4. 启动管理后台前端

    打开命令行，输入以下命令
    ```bash
    npm config set registry http://registry.npm.taobao.org
    npm install -g cnpm --registry=https://registry.npm.taobao.org
    cd litemall-plus/litemall-admin
    cnpm install
    cnpm run dev
    ```
    此时，浏览器打开，输入网址`http://localhost:9527`, 此时进入管理后台登录页面。
    
5. 启动小商城前端
   
   这里存在两套小商场前端litemall-wx和renard-wx，开发者可以分别导入和测试：
   这里如果没有可前往[litemall](https://gitee.com/linlinjava/litemall) 自行下载
   1. 微信开发工具导入litemall-wx项目;
   2. 项目配置，启用“不校验合法域名、web-view（业务域名）、TLS 版本以及 HTTPS 证书”
   3. 点击“编译”，即可在微信开发工具预览效果；
   4. 也可以点击“预览”，然后手机扫描登录（但是手机需开启调试功能）。
      
   注意：
   > 这里只是最简启动方式，而小商场的微信登录、微信支付等功能需开发者设置才能运行，
   > 更详细方案请参考[文档](./doc/project.md)。
        
## 警告

> 1. 本项目仅用于学习练习

## 致谢

本项目基于或参考以下项目：
1. [https://gitee.com/linlinjava/litemall](https://gitee.com/linlinjava/litemall)

    项目介绍：Spring Boot后端 + Vue管理员前端 + 微信小程序用户前端 + Vue用户移动端

2. [colorui](http://docs.xzeu.com/#/info/%E5%BF%AB%E9%80%9F%E5%BC%80%E5%A7%8B/%E5%BF%AB%E9%80%9F%E5%B8%83%E7%BD%B2)

   项目介绍：ColorUI是一个css库！！！在你引入样式后可以根据class来调用组件。

   项目参考：微信小程序前端设计。


## 推荐

1. [order-meal](https://gitee.com/JoyAtMeeting/order-meal) 微信扫码点餐小程序

## 问题

![](doc/pics/qq1.jpg)

 * 开发者有问题或者好的建议可以用Issues反馈交流，请给出详细信息
 * 在开发交流群中应讨论开发、业务和合作问题
 * 如果真的需要QQ群里提问，请在提问前先完成以下过程：
    * 请仔细阅读本项目文档，特别是是[**FAQ**](./doc/FAQ.md)，查看能否解决；
    * 请阅读[提问的智慧](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)；
    * 请百度或谷歌相关技术；
    * 请查看相关技术的官方文档，例如微信小程序的官方文档；
    * 请提问前尽可能做一些DEBUG或者思考分析，然后提问时给出详细的错误相关信息以及个人对问题的理解。

## License

[MulanPSL-2.0](https://gitee.com/JoyAtMeeting/litemall-plus/blob/master/LICENSE)
Copyright (c) 2022-present ysling