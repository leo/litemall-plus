package org.ysling.litemall.admin.web;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.

import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.ysling.litemall.admin.annotation.RequiresPermissionsDesc;
import org.ysling.litemall.core.utils.JacksonUtil;
import org.ysling.litemall.core.utils.ResponseUtil;
import org.ysling.litemall.core.validator.Order;
import org.ysling.litemall.core.validator.Sort;
import org.ysling.litemall.db.domain.LitemallQrcode;
import org.ysling.litemall.db.service.LitemallQrcodeService;
import org.ysling.litemall.core.utils.QRCodeUtil;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.ysling.litemall.admin.util.AdminResponseCode;

/**
 * 二维码配置
 */
@RestController
@RequestMapping("/admin/qrcode")
@Validated
public class AdminQrcodeController {

    @Resource
    private LitemallQrcodeService qrcodeService;

    @RequiresPermissions("admin:qrcode:list")
    @RequiresPermissionsDesc(menu = {"配置管理", "二维码配置"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
    	List<LitemallQrcode> qrcodeList = qrcodeService.queryList(page, limit, sort, order);
        return ResponseUtil.okList(qrcodeList);
    }
    

    @RequiresPermissions("admin:qrcode:create")
    @RequiresPermissionsDesc(menu = {"配置管理", "二维码配置"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody String body) {
        String tableNo = JacksonUtil.parseString(body, "tableNo");
        String httpTcp = JacksonUtil.parseString(body, "httpTcp");
        Integer imgSize = JacksonUtil.parseInteger(body, "imgSize");

        if (Objects.isNull(tableNo) || Objects.isNull(httpTcp) || Objects.isNull(imgSize)) {
            return ResponseUtil.badArgument();
        }

        List<LitemallQrcode> qrcodeList = qrcodeService.findQrcode(tableNo);
        if (qrcodeList.size() > 0){
            return ResponseUtil.fail(AdminResponseCode.ADMIN_NAME_EXIST, "桌号已经存在");
        }

        if (imgSize < 50){
            imgSize = 50;
        }else if(imgSize > 1000){
            imgSize = 1000;
        }
        byte[] picBlob = QRCodeUtil.createPicBlob(httpTcp, tableNo, imgSize);
        if (picBlob == null){
            return ResponseUtil.fail(AdminResponseCode.ADMIN_NAME_EXIST, "二维码生成失败");
        }

        LitemallQrcode qrcode = new LitemallQrcode();
		qrcode.setTableNo(tableNo);
		qrcode.setPicBlob(picBlob);
		qrcode.setHttpTcp(httpTcp);
        qrcode.setImgSize(imgSize);
        qrcodeService.add(qrcode);
        return ResponseUtil.ok(qrcode);
    }

    @RequiresPermissions("admin:qrcode:delete")
    @RequiresPermissionsDesc(menu = {"配置管理", "二维码配置"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallQrcode qrcode) {
        if (qrcode == null){
            return ResponseUtil.badArgument();
        }
        qrcodeService.deleteById(qrcode.getId());
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:qrcode:update")
    @RequiresPermissionsDesc(menu = {"配置管理", "二维码配置"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody LitemallQrcode qrcode) {
        if (qrcode == null){
            return ResponseUtil.badArgument();
        }

        Integer imgSize = qrcode.getImgSize();
        String tableNo = qrcode.getTableNo();
        String httpTcp = qrcode.getHttpTcp();
        if (Objects.isNull(tableNo) || Objects.isNull(httpTcp) || imgSize == null) {
            return ResponseUtil.badArgument();
        }

        if (imgSize < 50){
            imgSize = 50;
        }else if(imgSize > 1000){
            imgSize = 1000;
        }
        byte[] picBlob = QRCodeUtil.createPicBlob(httpTcp, tableNo, imgSize);
        if (picBlob == null){
            return ResponseUtil.fail(AdminResponseCode.ADMIN_NAME_EXIST, "二维码修改失败");
        }
        qrcode.setImgSize(imgSize);
        qrcode.setPicBlob(picBlob);
        qrcodeService.updateBlobById(qrcode);
        return ResponseUtil.ok(qrcode);
    }
}
