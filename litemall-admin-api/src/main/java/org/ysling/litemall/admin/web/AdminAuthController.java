package org.ysling.litemall.admin.web;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.subject.Subject;
import org.ysling.litemall.admin.service.SystemPermissionsService;
import org.ysling.litemall.core.service.ActionLogService;
import org.ysling.litemall.core.utils.captcha.CaptchaCodeManager;
import org.ysling.litemall.core.notify.NotifyService;
import org.ysling.litemall.core.utils.*;
import org.ysling.litemall.db.domain.LitemallAdmin;
import org.ysling.litemall.db.service.LitemallAdminService;
import org.ysling.litemall.db.service.LitemallPermissionService;
import org.ysling.litemall.db.service.LitemallRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.ysling.litemall.admin.util.AdminResponseCode;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.*;

@RestController
@RequestMapping("/admin/auth")
@Validated
public class AdminAuthController {
    private final Log logger = LogFactory.getLog(AdminAuthController.class);

    @Autowired
    private ActionLogService logHelper;
    @Autowired
    private NotifyService notifyService;
    @Autowired
    private LitemallAdminService adminService;
    @Autowired
    private LitemallRoleService roleService;
    @Autowired
    private LitemallPermissionService permissionService;
    @Autowired
    private SystemPermissionsService systemPermissionsService;


    /**
     * 登陆验证码
     * @param username
     * @return
     */
    @PostMapping("/captcha")
    public Object loginCaptcha(@NotNull String username) {
        if (!RegexUtil.isUsername(username)) {
            return ResponseUtil.fail(AdminResponseCode.ADMIN_INVALID_KAPTCHA_REQUIRED, "用户名校验失败");
        }

        String code = CharUtil.getRandomNum(6);
        if (!CaptchaCodeManager.addToCache(username, code)) {
            return ResponseUtil.fail(AdminResponseCode.ADMIN_INVALID_KAPTCHA_REQUIRED, "验证码未超时，不能发送");
        }

        // TODO 发送验证码到邮箱,上线放开下面三行
//        StringBuilder inform = new StringBuilder("如发送错误请您忽略！");
//        notifyService.notifyMail("登录验证码："+code, inform.toString() , username);
//        return ResponseUtil.ok("验证码发送成功，请注意查收");

        return ResponseUtil.ok("测试登录验证码为："+code);
    }

    /**
     * 管理员登陆
     * @param body
     * @param request
     * @return
     */
    @PostMapping("/login")
    public Object login(@RequestBody String body, HttpServletRequest request) {
        String username = JacksonUtil.parseString(body, "username");
        String password = JacksonUtil.parseString(body, "password");
        String code = JacksonUtil.parseString(body, "code");
        
        if (Objects.isNull(username) || Objects.isNull(password)) {
            return ResponseUtil.badArgument();
        }

        //判断验证码是否正确
        if (!CaptchaCodeManager.isCachedCaptcha(username,code)) {
            return ResponseUtil.fail(AdminResponseCode.ADMIN_INVALID_KAPTCHA_REQUIRED, "验证码错误");
        }
        
        //登录验证主体
        Subject currentUser = SecurityUtils.getSubject();
        //判断用户是否登陆
        if(currentUser.isAuthenticated()) currentUser.logout();

        try {
            currentUser.login(new UsernamePasswordToken(username, password));
        } catch (UnknownAccountException uae) {
            logHelper.logAuthFail("登录", "用户帐号或密码不正确");
            return ResponseUtil.fail(AdminResponseCode.ADMIN_INVALID_ACCOUNT, "用户帐号或密码不正确");
        } catch (LockedAccountException lae) {
            logHelper.logAuthFail("登录", "用户帐号已锁定不可用");
            return ResponseUtil.fail(AdminResponseCode.ADMIN_INVALID_ACCOUNT, "用户帐号已锁定不可用");
        } catch (AuthenticationException ae) {
            logHelper.logAuthFail("登录", "认证失败");
            return ResponseUtil.fail(AdminResponseCode.ADMIN_INVALID_ACCOUNT, "认证失败");
        }

        currentUser = SecurityUtils.getSubject();
        LitemallAdmin admin = (LitemallAdmin) currentUser.getPrincipal();
        admin.setLastLoginIp(IpUtil.getIpAddr(request));
        admin.setLastLoginTime(LocalDateTime.now());
        adminService.updateSelective(admin);

        logHelper.logAuthSucceed("登录");

        // userInfo
        Map<String, Object> adminInfo = new HashMap<String, Object>();
        adminInfo.put("nickName", admin.getUsername());
        adminInfo.put("avatar", admin.getAvatar());

        Map<Object, Object> result = new HashMap<Object, Object>();
        result.put("token", currentUser.getSession().getId());
        result.put("tenantId", admin.getTenantId());
        result.put("adminInfo", adminInfo);
        return ResponseUtil.ok(result);
    }

    /**
     * 退出登陆
     * @return
     */
    @RequiresAuthentication
    @PostMapping("/logout")
    public Object logout() {
        Subject currentUser = SecurityUtils.getSubject();

        logHelper.logAuthSucceed("退出登录");
        currentUser.logout();
        return ResponseUtil.ok();
    }


    @RequiresAuthentication
    @GetMapping("/info")
    public Object info() {
        Subject currentUser = SecurityUtils.getSubject();
        LitemallAdmin admin = (LitemallAdmin) currentUser.getPrincipal();

        Map<String, Object> data = new HashMap<>();
        data.put("name", admin.getUsername());
        data.put("avatar", admin.getAvatar());

        Integer[] roleIds = admin.getRoleIds();
        Set<String> roles = roleService.queryByIds(roleIds);
        Set<String> permissions = permissionService.queryByRoleIds(roleIds);
        data.put("roles", roles);

        // 这里需要转换perms结构，因为对于前端而已API形式的权限更容易理解
        data.put("perms", systemPermissionsService.toApi(permissions));
        return ResponseUtil.ok(data);
    }

    @GetMapping("/401")
    public Object page401() {
        return ResponseUtil.unlogin();
    }

    @GetMapping("/index")
    public Object pageIndex() {
        return ResponseUtil.ok();
    }

    @GetMapping("/403")
    public Object page403() {
        return ResponseUtil.unauthz();
    }
}
