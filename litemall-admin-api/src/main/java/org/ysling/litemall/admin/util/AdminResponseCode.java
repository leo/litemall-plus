package org.ysling.litemall.admin.util;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
public class AdminResponseCode {
    public static final Integer ADMIN_INVALID_NAME = 601;
    public static final Integer ADMIN_INVALID_PASSWORD = 602;
    public static final Integer ADMIN_NAME_EXIST = 602;
    public static final Integer ADMIN_ALTER_NOT_ALLOWED = 603;
    public static final Integer ADMIN_DELETE_NOT_ALLOWED = 604;
    public static final Integer ADMIN_INVALID_ACCOUNT = 605;
    public static final Integer ADMIN_INVALID_KAPTCHA = 606;
    public static final Integer ADMIN_INVALID_KAPTCHA_REQUIRED = 607;
    public static final Integer GOODS_UPDATE_NOT_ALLOWED = 610;
    public static final Integer GOODS_NAME_EXIST = 611;
    public static final Integer ORDER_CONFIRM_NOT_ALLOWED = 620;
    public static final Integer ORDER_REFUND_FAILED = 621;
    public static final Integer ORDER_REPLY_EXIST = 622;
    public static final Integer ORDER_DELETE_FAILED = 623;
    public static final Integer ORDER_PAY_FAILED = 624;
    public static final Integer USER_INVALID_NAME = 630;
    public static final Integer USER_INVALID_PASSWORD = 631;
    public static final Integer USER_INVALID_MOBILE = 632;
    public static final Integer USER_NAME_EXIST = 633;
    public static final Integer USER_MOBILE_EXIST = 634;
    public static final Integer ROLE_NAME_EXIST = 640;
    public static final Integer ROLE_SUPER_SUPERMISSION = 641;
    public static final Integer ROLE_USER_EXIST = 642;
    public static final Integer GROUPON_GOODS_UNKNOWN = 650;
    public static final Integer GROUPON_GOODS_EXISTED = 651;
    public static final Integer GROUPON_GOODS_OFFLINE = 652;
    public static final Integer NOTICE_UPDATE_NOT_ALLOWED = 660;
    public static final Integer AFTERSALE_NOT_ALLOWED = 670;

}
