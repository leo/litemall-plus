package org.ysling.litemall.admin.web;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.ysling.litemall.admin.annotation.RequiresPermissionsDesc;
import org.ysling.litemall.core.utils.ResponseUtil;
import org.ysling.litemall.db.domain.LitemallAdmin;
import org.ysling.litemall.db.domain.LitemallTenant;
import org.ysling.litemall.db.service.LitemallAdminService;
import org.ysling.litemall.db.service.LitemallTenantService;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin/tenant")
@Validated
public class AdminTenantController {

    @Resource
    private LitemallTenantService tenantService;
    @Autowired
    private LitemallAdminService adminService;

    @RequiresPermissions("admin:tenant:list")
    @RequiresPermissionsDesc(menu = {"系统管理", "租户管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        return ResponseUtil.okList(tenantService.queryList(page, limit));
    }

    @RequiresPermissions("admin:tenant:all")
    @RequiresPermissionsDesc(menu = {"系统管理", "租户管理"}, button = "查询所有")
    @GetMapping("/all")
    public Object listAll() {
        return ResponseUtil.okList(tenantService.queryList());
    }

    @RequiresPermissions("admin:tenant:create")
    @RequiresPermissionsDesc(menu = {"系统管理", "租户管理"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody LitemallTenant tenant) {
        if (tenant == null || tenant.getAddress() == null){
            return ResponseUtil.badArgument();
        }
        tenantService.add(tenant);
        return ResponseUtil.ok(tenant);
    }

    @RequiresPermissions("admin:tenant:delete")
    @RequiresPermissionsDesc(menu = {"系统管理", "租户管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallTenant tenant) {
        if (tenant == null || tenant.getId() == null){
            return ResponseUtil.badArgument();
        }
        //删除租户
        tenantService.deleteById(tenant.getId());
        //删除租户下的所有管理员
        List<LitemallAdmin> adminList = adminService.findByTenantId(tenant.getId());
        for (LitemallAdmin admin :adminList) {
            adminService.deleteById(admin.getId());
        }
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:tenant:update")
    @RequiresPermissionsDesc(menu = {"系统管理", "租户管理"}, button = "修改")
    @PostMapping("/update")
    public Object update(@RequestBody LitemallTenant tenant) {
        if (tenant == null || tenant.getAddress() == null){
            return ResponseUtil.badArgument();
        }
        if (tenantService.updateVersionSelective(tenant) == 0){
            return ResponseUtil.updatedDateExpired();
        }
        return ResponseUtil.ok(tenant);
    }
}
