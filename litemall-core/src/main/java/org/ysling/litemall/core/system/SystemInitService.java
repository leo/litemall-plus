package org.ysling.litemall.core.system;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import org.ysling.litemall.core.utils.SystemInfoPrinter;
import org.ysling.litemall.db.service.LitemallSystemConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 系统启动服务，用于设置系统配置信息、检查系统状态及打印系统信息
 */
@Component
class SystemInitService {

    @Autowired
    private Environment environment;
    @Autowired
    private LitemallSystemConfigService systemConfigService;

    private final static Map<String, String> DEFAULT_CONFIGS = new HashMap<>();

    @PostConstruct
    private void init() {
        initConfigs();
        SystemInfoPrinter.printInfo("Litemall-plus 初始化信息", getSystemInfo());
    }

    static {
        // 小程序相关配置默认值
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_WX_INDEX_NEW, "20");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_WX_INDEX_HOT, "20");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_WX_INDEX_BRAND, "4");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_WX_INDEX_TOPIC, "20");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_WX_INDEX_CATLOG_LIST, "20");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_WX_INDEX_CATLOG_GOODS, "20");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_WX_SHARE, "false");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_WX_PAY, "true");
        // 运费相关配置默认值
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_EXPRESS_FREIGHT_VALUE, "0");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_EXPRESS_FREIGHT_MIN, "0");
        // 订单相关配置默认值
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_ORDER_UNPAID, "30");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_ORDER_UNCONFIRM, "7");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_ORDER_COMMENT, "7");
        // 商城相关配置默认值
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_MALL_NAME, "天天云游市场");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_MALL_ADDRESS, "贵阳");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_MALL_LATITUDE, "31.201900");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_MALL_LONGITUDE, "121.587839");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_MALL_PHONE, "185-8567-5204");
        DEFAULT_CONFIGS.put(SystemConfig.LITEMALL_MALL_QQ, "927069313");
    }

    private void initConfigs() {
        // 1. 读取数据库全部配置信息
        Map<String, String> configs = systemConfigService.queryAll();
        // 2. 分析DEFAULT_CONFIGS
        for (Map.Entry<String, String> entry : DEFAULT_CONFIGS.entrySet()) {
            if (configs.containsKey(entry.getKey())) {
                continue;
            }
            configs.put(entry.getKey(), entry.getValue());
            systemConfigService.addConfig(entry.getKey(), entry.getValue());
        }
        SystemConfig.setConfigs(configs);
    }

    private Map<String, String> getSystemInfo() {
        Map<String, String> infos = new LinkedHashMap<>();
        infos.put(SystemInfoPrinter.CREATE_PART_COPPER + 0, "系统信息");
        // 测试获取application-db.yml配置信息
        infos.put("服务器端口-----------", environment.getProperty("server.port"));
        infos.put("数据库USER-----------", environment.getProperty("spring.datasource.druid.username"));
        infos.put("数据库地址-----------", environment.getProperty("spring.datasource.druid.url"));
        infos.put("调试级别-------------", environment.getProperty("logging.level.org.ysling.litemall.wx"));

        // 测试获取application-core.yml配置信息
        infos.put(SystemInfoPrinter.CREATE_PART_COPPER + 1, "模块状态");
        infos.put("邮件----------------", environment.getProperty("litemall.notify.mail.enable"));
        infos.put("短信----------------", environment.getProperty("litemall.notify.sms.enable"));
        infos.put("模版消息-------------", environment.getProperty("litemall.notify.wx.enable"));
        infos.put("快递信息-------------", environment.getProperty("litemall.express.enable"));
        infos.put("快递鸟ID-------------", environment.getProperty("litemall.express.appId"));
        infos.put("对象存储-------------", environment.getProperty("litemall.storage.active"));
        infos.put("本地对象存储路径-------", environment.getProperty("litemall.storage.local.storagePath"));
        infos.put("本地对象访问地址-------", environment.getProperty("litemall.storage.local.address"));
        infos.put("本地对象访问端口-------", environment.getProperty("litemall.storage.local.port"));

        // 微信相关信息
        infos.put(SystemInfoPrinter.CREATE_PART_COPPER + 2, "微信相关");
        infos.put("微信APP KEY---------", environment.getProperty("litemall.wx.app-id"));
        infos.put("微信APP-SECRET------", environment.getProperty("litemall.wx.app-secret"));
        infos.put("微信支付MCH-ID-------", environment.getProperty("litemall.wx.mch-id"));
        infos.put("微信支付MCH-KEY------", environment.getProperty("litemall.wx.mch-key"));
        infos.put("微信支付通知地址-------", environment.getProperty("litemall.wx.notify-url"));

        //测试获取System表配置信息
        infos.put(SystemInfoPrinter.CREATE_PART_COPPER + 3, "系统设置");
        infos.put("自动创建朋友圈分享图---", Boolean.toString(SystemConfig.isAutoCreateShareImage()));
        infos.put("微信支付------------", Boolean.toString(SystemConfig.isAutoPay()));
        infos.put("商场名称------------", SystemConfig.getMallName());
        infos.put("商场地址------------", SystemConfig.getMallAddress());
        infos.put("商场经度------------", SystemConfig.getMallLatitude());
        infos.put("商场纬度------------", SystemConfig.getMallLongitude());
        infos.put("商场电话------------", SystemConfig.getMallPhone());
        infos.put("商场QQ-------------", SystemConfig.getMallQQ());

        infos.put("首页显示记录数：NEW,HOT,BRAND,TOPIC,CatlogList,CatlogMore",
                    SystemConfig.getNewLimit() + "," +
                    SystemConfig.getHotLimit() + "," +
                    SystemConfig.getBrandLimit() + "," +
                    SystemConfig.getTopicLimit() + "," +
                    SystemConfig.getCatlogListLimit() + "," +
                    SystemConfig.getCatlogMoreLimit());

        //设置http请求超时时间10秒
        System.setProperty("sun.net.client.defaultConnectTimeout", String.valueOf(10000));// （单位：毫秒）
        System.setProperty("sun.net.client.defaultReadTimeout", String.valueOf(10000)); // （单位：毫秒）
        return infos;
    }
}
