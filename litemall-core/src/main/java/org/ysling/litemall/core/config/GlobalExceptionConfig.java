package org.ysling.litemall.core.config;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.ysling.litemall.core.utils.ResponseUtil;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.util.Set;

/**
 * 全局异常配置类
 */
@ControllerAdvice
@Order
public class GlobalExceptionConfig {

    private Log logger = LogFactory.getLog(GlobalExceptionConfig.class);

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public Object seriousHandler(Exception e) {
        logger.error(e.getMessage(), e);
        return ResponseUtil.serious();
    }

    @ResponseBody
    @ExceptionHandler(RuntimeException.class)
    public Object runtimeHandler(RuntimeException e) {
        logger.error(e.getMessage(), e);
        return ResponseUtil.runtime(e.getMessage());
    }


    @ResponseBody
    @ExceptionHandler(NullPointerException.class)
    public Object nullPointerHandler(NullPointerException e) {
        logger.error(e.getMessage(), e);
        return ResponseUtil.runtime("信息获取失败");
    }

    @ResponseBody
    @ExceptionHandler(IllegalArgumentException.class)
    public Object badArgumentHandler(IllegalArgumentException e) {
        logger.error(e.getMessage(), e);
        return ResponseUtil.badArgumentValue();
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public Object badArgumentHandler(MethodArgumentTypeMismatchException e) {
        logger.error(e.getMessage(), e);
        return ResponseUtil.badArgumentValue();
    }


    @ResponseBody
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Object badArgumentHandler(MissingServletRequestParameterException e) {
        logger.error(e.getMessage(), e);
        return ResponseUtil.badArgumentValue();
    }


    @ResponseBody
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Object badArgumentHandler(HttpMessageNotReadableException e) {
        logger.error(e.getMessage(), e);
        return ResponseUtil.badArgumentValue();
    }


    @ResponseBody
    @ExceptionHandler(ValidationException.class)
    public Object badArgumentHandler(ValidationException e) {
        if (e instanceof ConstraintViolationException) {
            ConstraintViolationException exs = (ConstraintViolationException) e;
            Set<ConstraintViolation<?>> violations = exs.getConstraintViolations();
            for (ConstraintViolation<?> item : violations) {
                String message = ((PathImpl) item.getPropertyPath()).getLeafNode().getName() + item.getMessage();
                logger.error(message, e);
                return ResponseUtil.fail(402, message);
            }
        }
        logger.error(e.getMessage(), e);
        return ResponseUtil.badArgumentValue();
    }

}
