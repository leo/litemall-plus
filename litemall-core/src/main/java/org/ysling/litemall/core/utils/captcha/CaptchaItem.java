package org.ysling.litemall.core.utils.captcha;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 验证码实体类，用于缓存验证码发送
 */
public class CaptchaItem implements Serializable {

    /**账号*/
    private String account;
    /**验证码*/
    private String code;
    /**过期时间*/
    private LocalDateTime expireTime;
    /**重发时间*/
    private LocalDateTime retryTime;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(LocalDateTime expireTime) {
        this.expireTime = expireTime;
    }

    public LocalDateTime getRetryTime() {
        return retryTime;
    }

    public void setRetryTime(LocalDateTime retryTime) {
        this.retryTime = retryTime;
    }
}