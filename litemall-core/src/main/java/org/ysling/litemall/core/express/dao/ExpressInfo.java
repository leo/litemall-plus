package org.ysling.litemall.core.express.dao;
/**
 *  Copyright (c) [ysling] [927069313@qq.com]
 *  [litemall-plus] is licensed under Mulan PSL v2.
 *  You can use this software according to the terms and conditions of the Mulan PSL v2.
 *  You may obtain a copy of Mulan PSL v2 at:
 *              http://license.coscl.org.cn/MulanPSL2
 *  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 *  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *  See the Mulan PSL v2 for more details.
 */

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * 物流详情
 */
public class ExpressInfo {

    /**用户ID*/
    @JsonProperty("EBusinessID")
    private String EBusinessID;

    /**订单编号*/
    @JsonProperty("OrderCode")
    private String OrderCode;

    /**快递公司编码*/
    @JsonProperty("ShipperCode")
    private String ShipperCode;

    /**物流公司名称*/
    private String ShipperName;

    /**物流运单号*/
    @JsonProperty("LogisticCode")
    private String LogisticCode;

    /**成功与否*/
    @JsonProperty("Success")
    private boolean Success;

    /**失败原因*/
    @JsonProperty("Reason")
    private String Reason;

    /**物流状态：2-在途中,3-签收,4-问题件*/
    @JsonProperty("State")
    private String State;

    /**用户ID*/
    @JsonProperty("Traces")
    private List<Traces> Traces;

    public String getEBusinessID() {
        return EBusinessID;
    }

    public void setEBusinessID(String EBusinessID) {
        this.EBusinessID = EBusinessID;
    }

    public String getOrderCode() {
        return OrderCode;
    }

    public void setOrderCode(String orderCode) {
        OrderCode = orderCode;
    }

    public String getShipperCode() {
        return ShipperCode;
    }

    public void setShipperCode(String shipperCode) {
        ShipperCode = shipperCode;
    }

    public String getShipperName() {
        return ShipperName;
    }

    public void setShipperName(String shipperName) {
        ShipperName = shipperName;
    }

    public String getLogisticCode() {
        return LogisticCode;
    }

    public void setLogisticCode(String logisticCode) {
        LogisticCode = logisticCode;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean success) {
        Success = success;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public List<Traces> getTraces() {
        return Traces;
    }

    public void setTraces(List<Traces> traces) {
        Traces = traces;
    }
}