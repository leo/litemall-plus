package org.ysling.litemall.core.config;
/**
 *  Copyright (c) [ysling] [927069313@qq.com]
 *  [litemall-plus] is licensed under Mulan PSL v2.
 *  You can use this software according to the terms and conditions of the Mulan PSL v2.
 *  You may obtain a copy of Mulan PSL v2 at:
 *              http://license.coscl.org.cn/MulanPSL2
 *  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 *  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *  See the Mulan PSL v2 for more details.
 */
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.ysling.litemall.core.utils.GlobalWebUtil;
import org.ysling.litemall.core.utils.IpUtil;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

/**
 * 接口切面日志
 */
@Configuration
public class LogAspectConfig {

    private final Log logger = LogFactory.getLog(LogAspectConfig.class);

    /**
     * 配置切入点表达式
     */
    private static final String POINTCUT_EXPRESSION = "execution(public * org.ysling.litemall.*.web.*.*(..))";

    /**
     * 日志的切面
     * @return
     */
    @Bean
    public DefaultPointcutAdvisor defaultPointcutAdvisor() {
        //添加切点
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression(POINTCUT_EXPRESSION);
        //环绕通知方法
        MethodInterceptor interceptor = new MethodInterceptor() {
            @Nullable
            @Override
            public Object invoke(@Nonnull MethodInvocation invocation) throws Throwable {
                long start = System.currentTimeMillis();

                //获取当前请求
                HttpServletRequest request = GlobalWebUtil.getRequest();
                if (request == null) return invocation.proceed();

                //执行时间
                Object result = invocation.proceed();
                long end = System.currentTimeMillis();

                logger.info("=====================================Method  start==================================");
                logger.info("请求地址:" + request.getRequestURI());
                logger.info("用户IP:" + IpUtil.getIpAddr(request));
                logger.info("执行时间: " + (end - start) + " ms!");
                logger.info("=====================================Method  End====================================");
                return result;
            }
        };
        // 配置增强类advisor
        DefaultPointcutAdvisor advisor = new DefaultPointcutAdvisor();
        advisor.setPointcut(pointcut);
        advisor.setAdvice(interceptor);
        return advisor;
    }
}