package org.ysling.litemall.core.tasks.impl;
/**
 *  Copyright (c) [ysling] [927069313@qq.com]
 *  [litemall-plus] is licensed under Mulan PSL v2.
 *  You can use this software according to the terms and conditions of the Mulan PSL v2.
 *  You may obtain a copy of Mulan PSL v2 at:
 *              http://license.coscl.org.cn/MulanPSL2
 *  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 *  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *  See the Mulan PSL v2 for more details.
 */
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ysling.litemall.core.service.ActionLogService;
import org.ysling.litemall.core.service.OrderCoreService;
import org.ysling.litemall.core.tasks.service.Task;
import org.ysling.litemall.core.utils.BeanUtil;
import org.ysling.litemall.db.constant.GrouponConstant;
import org.ysling.litemall.db.constant.OrderConstant;
import org.ysling.litemall.db.domain.LitemallGoods;
import org.ysling.litemall.db.domain.LitemallGroupon;
import org.ysling.litemall.db.domain.LitemallGrouponRules;
import org.ysling.litemall.db.domain.LitemallOrder;
import org.ysling.litemall.db.service.LitemallGoodsService;
import org.ysling.litemall.db.service.LitemallGrouponRulesService;
import org.ysling.litemall.db.service.LitemallGrouponService;
import org.ysling.litemall.db.service.LitemallOrderService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 团购到期延时队列
 */
public class GrouponRuleExpiredTask extends Task {

    private final Log logger = LogFactory.getLog(GrouponRuleExpiredTask.class);
    private final Integer grouponRuleId;

    public GrouponRuleExpiredTask(Integer grouponRuleId, long delayInMilliseconds){
        super("GrouponRuleExpiredTask-" + grouponRuleId, delayInMilliseconds);
        this.grouponRuleId = grouponRuleId;
    }

    public GrouponRuleExpiredTask(Integer grouponRuleId){
        super("GrouponRuleExpiredTask-" + grouponRuleId, 0);
        this.grouponRuleId = grouponRuleId;
    }

    @Override
    public void run() {
        logger.info(String.format("系统处理延时任务 -> [开始-团购规则过期] [任务ID-%s]" , this.grouponRuleId));

        ActionLogService logService = BeanUtil.getBean(ActionLogService.class);
        OrderCoreService orderCoreService = BeanUtil.getBean(OrderCoreService.class);
        LitemallOrderService orderService = BeanUtil.getBean(LitemallOrderService.class);
        LitemallGoodsService goodsService = BeanUtil.getBean(LitemallGoodsService.class);
        LitemallGrouponService grouponService = BeanUtil.getBean(LitemallGrouponService.class);
        LitemallGrouponRulesService grouponRulesService = BeanUtil.getBean(LitemallGrouponRulesService.class);

        logService.logOrderSucceed("系统处理延时任务",String.format("[开始-团购规则过期] [任务ID-%s]" , this.grouponRuleId));

        //查找团购规则
        LitemallGrouponRules grouponRules = grouponRulesService.findById(grouponRuleId);
        if (grouponRules == null) return;
        if (!grouponRules.getStatus().equals(GrouponConstant.RULE_STATUS_ON)) return;
        //查找团购商品
        LitemallGoods goods = goodsService.findById(grouponRules.getGoodsId());
        if (goods == null) return;
        //设置商品取消团购
        goods.setIsGroupon(false);
        if (goodsService.updateVersionSelective(goods) == 0){
            throw new RuntimeException("网络繁忙，请刷新重试");
        }

        //判断是否到达过期时间
        if (grouponRules.getExpireTime().isBefore(LocalDateTime.now())){
            grouponRules.setStatus(GrouponConstant.RULE_STATUS_DOWN_EXPIRE);
        } else {
            grouponRules.setStatus(GrouponConstant.RULE_STATUS_DOWN_ADMIN);
        }
        // 团购活动取消
        if (grouponRulesService.updateVersionSelective(grouponRules) == 0) {
            throw new RuntimeException("网络繁忙，请刷新重试");
        }

        // 用户团购订单处理
        List<LitemallGroupon> grouponList = grouponService.queryByRuleId(grouponRuleId);
        for(LitemallGroupon groupon : grouponList){
            LitemallOrder order = orderService.findById(groupon.getOrderId());
            if(groupon.getStatus().equals(GrouponConstant.STATUS_NONE)){
                //团购未付款或订单金额为零，设置团购取消
                groupon.setStatus(GrouponConstant.STATUS_CANCEL);
                if (grouponService.updateVersionSelective(groupon) == 0) {
                    throw new RuntimeException("网络繁忙，请刷新重试");
                }
                //设置订单取消（系统）
                order.setOrderStatus(OrderConstant.STATUS_AUTO_CANCEL);
                if (orderService.updateVersionSelective(order) == 0) {
                    throw new RuntimeException("网络繁忙，请刷新重试");
                }
            } else if(groupon.getStatus().equals(GrouponConstant.STATUS_ON)){
                // 如果团购进行中，团购设置团购失败等待退款状态
                groupon.setStatus(GrouponConstant.STATUS_FAIL);
                if (grouponService.updateVersionSelective(groupon) == 0) {
                    throw new RuntimeException("网络繁忙，请刷新重试");
                }
                //支付金额为零
                if (order.getActualPrice().compareTo(BigDecimal.ZERO) <= 0){
                    //团购订单申请退款
                    order.setOrderStatus(OrderConstant.STATUS_AUTO_CANCEL);
                    // 返还订单
                    orderCoreService.orderRelease(order);
                } else {
                    //团购订单申请退款
                    order.setOrderStatus(OrderConstant.STATUS_REFUND);
                }
                //更新订单
                if (orderService.updateVersionSelective(order) == 0) {
                    throw new RuntimeException("网络繁忙，请刷新重试");
                }
            }
        }

        logService.logOrderSucceed("系统处理延时任务",String.format("[结束-团购规则过期] [任务ID-%s]" , this.grouponRuleId));
        logger.info(String.format("系统处理延时任务 -> [结束-团购规则过期] [任务ID-%s]" , this.grouponRuleId));
    }
}
