package org.ysling.litemall.core.utils;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;

public class SystemInfoPrinter {

    private static final Log logger = LogFactory.getLog(SystemInfoPrinter.class);
    public static final String CREATE_PART_COPPER = "XOXOXOXOX";

    private static int maxSize = 0;

    public static void printInfo(String title, Map<String, String> infos) {
        setMaxSize(infos);

        printHeader(title);

        for (Map.Entry<String, String> entry : infos.entrySet()) {
            printLine(entry.getKey(), entry.getValue());
        }

        printEnd(title);
    }

    private static void setMaxSize(Map<String, String> infos) {
        for (Map.Entry<String, String> entry : infos.entrySet()) {
            if (entry.getValue() == null)
                continue;

            int size = entry.getKey().length() + entry.getValue().length();

            if (size > maxSize)
                maxSize = size;
        }

        maxSize = maxSize + 30;
    }

    private static void printHeader(String title) {
        logger.info("====================" + title + "-开始" + "====================");
    }

    private static void printEnd(String title) {
        logger.info("  ");
        logger.info("====================" + title + "-结束" + "====================");
        logger.info("  ");
    }

    private static String getLineCopper() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < maxSize; i++) {
            sb.append("=");
        }

        return sb.toString();
    }

    private static void printLine(String head, String line) {
        if (line == null)
            return;

        if (head.startsWith(CREATE_PART_COPPER)) {
            logger.info("");
            logger.info("    [[  " + line + "  ]]");
            logger.info("");
        } else {
            logger.info(head + "->  " + line);
        }
    }
}
