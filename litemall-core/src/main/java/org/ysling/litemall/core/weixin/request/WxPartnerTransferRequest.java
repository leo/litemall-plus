package org.ysling.litemall.core.weixin.request;

import com.github.binarywang.wxpay.bean.marketing.transfer.PartnerTransferRequest;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WxPartnerTransferRequest extends PartnerTransferRequest implements Serializable{

    @SerializedName("appid")
    private String appid;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }
}
