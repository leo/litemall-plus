package org.ysling.litemall.core.tasks.impl;
/**
 *  Copyright (c) [ysling] [927069313@qq.com]
 *  [litemall-plus] is licensed under Mulan PSL v2.
 *  You can use this software according to the terms and conditions of the Mulan PSL v2.
 *  You may obtain a copy of Mulan PSL v2 at:
 *              http://license.coscl.org.cn/MulanPSL2
 *  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 *  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *  See the Mulan PSL v2 for more details.
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ysling.litemall.core.service.ActionLogService;
import org.ysling.litemall.core.tasks.service.Task;
import org.ysling.litemall.core.utils.BeanUtil;
import org.ysling.litemall.db.constant.CouponUserConstant;
import org.ysling.litemall.db.domain.LitemallCouponUser;
import org.ysling.litemall.db.service.LitemallCouponUserService;

import java.util.Objects;

/**
 * 用户优惠券过期队列
 */
public class CouponUserExpiredTask extends Task {

    private final Log logger = LogFactory.getLog(CouponUserExpiredTask.class);
    private final Integer couponUserId;

    public CouponUserExpiredTask(Integer couponUserId, long delayInMilliseconds){
        super("CouponExpiredTask-" + couponUserId, delayInMilliseconds);
        this.couponUserId = couponUserId;
    }

    @Override
    public void run() {
        logger.info(String.format("系统处理延时任务 -> [开始-用户优惠券过期] [任务ID-%s]" , this.couponUserId));

        ActionLogService logService = BeanUtil.getBean(ActionLogService.class);
        LitemallCouponUserService couponUserService = BeanUtil.getBean(LitemallCouponUserService.class);

        LitemallCouponUser couponUser = couponUserService.findById(this.couponUserId);
        if(couponUser == null || !Objects.equals(CouponUserConstant.STATUS_USABLE , couponUser.getStatus())) return;

        couponUser.setStatus(CouponUserConstant.STATUS_EXPIRED);
        if (couponUserService.updateVersionSelective(couponUser) == 0) {
            throw new RuntimeException("用户优惠券过期设置失败");
        }

        logger.info(String.format("系统处理延时任务 -> [结束-用户优惠券过期] [任务ID-%s]" , this.couponUserId));
        logService.logOrderSucceed("系统处理延时任务",String.format("[结束-用户优惠券过期] [任务ID-%s]" , this.couponUserId));
    }
}
