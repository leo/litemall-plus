package org.ysling.litemall.core.tasks.impl;
/**
 *  Copyright (c) [ysling] [927069313@qq.com]
 *  [litemall-plus] is licensed under Mulan PSL v2.
 *  You can use this software according to the terms and conditions of the Mulan PSL v2.
 *  You may obtain a copy of Mulan PSL v2 at:
 *              http://license.coscl.org.cn/MulanPSL2
 *  THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 *  MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *  See the Mulan PSL v2 for more details.
 */
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ysling.litemall.core.service.ActionLogService;
import org.ysling.litemall.core.system.SystemConfig;
import org.ysling.litemall.core.tasks.service.Task;
import org.ysling.litemall.core.utils.BeanUtil;
import org.ysling.litemall.db.constant.OrderConstant;
import org.ysling.litemall.db.domain.*;
import org.ysling.litemall.db.service.*;

import java.util.List;

/**
 * 订单评论超时
 */
public class OrderCommentTask extends Task {

    private final Log logger = LogFactory.getLog(OrderCommentTask.class);
    private final Integer orderId;

    public OrderCommentTask(Integer orderId, long delayInMilliseconds){
        super("OrderCommentTask-" + orderId, delayInMilliseconds);
        this.orderId = orderId;
    }

    public OrderCommentTask(Integer orderId){
        super("OrderCommentTask-" + orderId, SystemConfig.getOrderComment() * 24 * 60 * 60 * 1000);
        this.orderId = orderId;
    }

    @Override
    public void run() {
        logger.info(String.format("系统处理延时任务 -> [开始-订单评论超时] [任务ID-%s]" , this.orderId));

        ActionLogService logService = BeanUtil.getBean(ActionLogService.class);
        LitemallOrderService orderService = BeanUtil.getBean(LitemallOrderService.class);
        LitemallOrderGoodsService orderGoodsService = BeanUtil.getBean(LitemallOrderGoodsService.class);

        logService.logOrderSucceed("系统处理延时任务",String.format("[开始-订单评论超时] [任务ID-%s]" , this.orderId));
        LitemallOrder order = orderService.findById(this.orderId);
        if(order == null || !OrderConstant.isConfirmStatus(order)) return;

        order.setComments((short) 0);
        order.setOrderStatus(OrderConstant.STATUS_COMMENT_OVERTIME);
        if (orderService.updateVersionSelective(order) == 0){
            throw new RuntimeException("网络繁忙，请刷新重试");
        }

        //设置订单商品禁止评论
        List<LitemallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(order.getId());
        for (LitemallOrderGoods orderGoods : orderGoodsList) {
            orderGoods.setComment(-1);
            if (orderGoodsService.updateVersionSelective(orderGoods) == 0){
                throw new RuntimeException("商品评论更新失败");
            }
        }

        logger.info(String.format("系统处理延时任务 -> [结束-订单评论超时] [任务ID-%s]" , this.orderId));
        logService.logOrderSucceed("系统处理延时任务",String.format("[结束-订单评论超时] [任务ID-%s]" , this.orderId));
    }
}
