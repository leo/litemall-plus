package org.ysling.litemall.core.service;
// Copyright (c) [ysling] [927069313@qq.com]
// [litemall-plus] is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of the Mulan PSL v2.
// You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.ysling.litemall.db.dtomain.GoodsAllinone;
import org.ysling.litemall.core.tasks.impl.GrouponRuleExpiredTask;
import org.ysling.litemall.core.tasks.service.TaskService;
import org.ysling.litemall.core.utils.ResponseUtil;
import org.ysling.litemall.db.domain.*;
import org.ysling.litemall.db.service.*;
import org.ysling.litemall.db.constant.GrouponConstant;


import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Service
public class GoodsCoreService {
    private final Log logger = LogFactory.getLog(GoodsCoreService.class);

    @Autowired
    private LitemallGoodsService goodsService;
    @Autowired
    private LitemallGoodsSpecificationService specificationService;
    @Autowired
    private LitemallGoodsAttributeService attributeService;
    @Autowired
    private LitemallGoodsProductService productService;
    @Autowired
    private LitemallCategoryService categoryService;
    @Autowired
    private LitemallGrouponRulesService rulesService;
    @Autowired
    private LitemallRewardTaskService rewardTaskService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private LitemallBrandService brandService;
    @Autowired
    private LitemallCartService cartService;
    @Autowired
    private QCodeService qCodeService;

    /**
     * 商品信息校验
     * @param goodsAllinone
     * @return
     */
    private Object validate(GoodsAllinone goodsAllinone) {
        LitemallGoods goods = goodsAllinone.getGoods();
        String name = goods.getName();
        String unit = goods.getUnit();
        String brief = goods.getBrief();
        String picUrl = goods.getPicUrl();
        String goodsSn = goods.getGoodsSn();
        if (!ObjectUtils.allNotNull(name, goodsSn,unit,brief,picUrl)) {
            return ResponseUtil.fail("商品信息错误");
        }

        String[] gallery = goods.getGallery();
        if (gallery == null || gallery.length <= 0){
            return ResponseUtil.fail("商品轮播图错误");
        }

        // 品牌商必须设置，如果设置则需要验证品牌商存在
        Integer brandId = goods.getBrandId();
        if (brandId != null && brandId != 0) {
            LitemallBrand brand = brandService.findById(brandId);
            if (brand == null) {
                return ResponseUtil.fail("店铺信息错误");
            }
            if (!brand.getStatus().equals((byte)0)){
                return ResponseUtil.fail("店铺已禁用");
            }
        }else {
            return ResponseUtil.fail("店铺信息错误");
        }

        // 分类必须设置，如果设置则需要验证分类存在
        Integer categoryId = goods.getCategoryId();
        if (categoryId != null && categoryId != 0) {
            if (categoryService.findById(categoryId) == null) {
                return ResponseUtil.fail("分类信息错误");
            }
        }else {
            return ResponseUtil.fail("分类信息错误");
        }

        //如果有团购验证团购
        if (goods.getIsGroupon() != null && goods.getIsGroupon()){
            LitemallGrouponRules grouponRules = goodsAllinone.getGrouponRules();
            BigDecimal discount = grouponRules.getDiscount();
            LocalDateTime expireTime = grouponRules.getExpireTime();
            Integer discountMember = grouponRules.getDiscountMember();
            if (!ObjectUtils.allNotNull(expireTime, discount, discountMember)){
                return ResponseUtil.fail("团购信息错误");
            }
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime expire = grouponRules.getExpireTime();
            if(expire.isBefore(now)){
                return ResponseUtil.fail(889,"团购日期设置错误");
            }
        }

        //商品规格设置
        LitemallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        for (LitemallGoodsSpecification specification : specifications) {
            String value = specification.getValue();
            String spec = specification.getSpecification();
            String specificationPicUrl = specification.getPicUrl();
            if (!ObjectUtils.allNotNull(spec, value ,specificationPicUrl)) {
                return ResponseUtil.fail("商品规格信息不完整");
            }
        }

        //商品参数设置
        LitemallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        for (LitemallGoodsAttribute attribute : attributes) {
            String attr = attribute.getAttribute();
            String value = attribute.getValue();
            if (!ObjectUtils.allNotNull(attr, value)) {
                return ResponseUtil.fail("商品参数信息不完整");
            }
        }

        //商品货品设置
        LitemallGoodsProduct[] products = goodsAllinone.getProducts();
        if (products.length <= 0) return ResponseUtil.fail("货品信息至少一条");
        for (LitemallGoodsProduct product : products) {
            Integer number = product.getNumber();
            if (number == null || number <= 0) {
                return ResponseUtil.fail("货品库存不正确");
            }

            BigDecimal price = product.getPrice();
            if (price == null || price.intValue() < 1 || price.intValue() > 1000) {
                return ResponseUtil.fail("货品销售价格不正确1~1000元");
            }

            String[] productSpecifications = product.getSpecifications();
            if (productSpecifications.length <= 0) {
                return ResponseUtil.fail("货品规格至少有一个");
            }
        }

        return null;
    }


    /**
     * 商品添加
     * @param goodsAllinone
     * @return
     */
    public Object goodsCreate(GoodsAllinone goodsAllinone) {
        Object error = validate(goodsAllinone);
        if (error != null) {
            return error;
        }

        LitemallGoods goods = goodsAllinone.getGoods();
        LitemallGrouponRules grouponRules = goodsAllinone.getGrouponRules();
        LitemallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        LitemallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        LitemallGoodsProduct[] products = goodsAllinone.getProducts();

        if (goodsService.checkExistByName(goods.getName())) {
            throw new RuntimeException("商品名已经存在");
        }

        // 商品表里面有一个字段retailPrice记录当前商品的最低价
        BigDecimal retailPrice = new BigDecimal(Integer.MAX_VALUE);
        for (LitemallGoodsProduct product : products) {
            BigDecimal productPrice = product.getPrice();
            if(retailPrice.compareTo(productPrice) == 1){
                retailPrice = productPrice;
            }
        }

        // 添加商品基本信息
        goods.setRetailPrice(retailPrice);
        goodsService.add(goods);

        //将生成的分享图片地址写入数据库
        String url = qCodeService.createGoodShareImage(goods.getId().toString(), goods.getPicUrl(), goods.getName());
        goods.setShareUrl(url);
        goodsService.updateById(goods);

        //判断是否团购
        if (goods.getIsGroupon() != null && goods.getIsGroupon()){
            //添加团购规则信息
            grouponRules.setGoodsId(goods.getId());
            grouponRules.setGoodsName(goods.getName());
            grouponRules.setPicUrl(goods.getPicUrl());
            grouponRules.setStatus(GrouponConstant.RULE_STATUS_ON);
            rulesService.createRules(grouponRules);
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime expire = grouponRules.getExpireTime();
            long delay = ChronoUnit.MILLIS.between(now, expire);
            // 团购过期任务
            taskService.addTask(new GrouponRuleExpiredTask(grouponRules.getId(), delay));
        }

        // 商品规格表litemall_goods_specification
        for (LitemallGoodsSpecification specification : specifications) {
            specification.setGoodsId(goods.getId());
            specificationService.add(specification);
        }

        // 商品参数表litemall_goods_attribute
        for (LitemallGoodsAttribute attribute : attributes) {
            attribute.setGoodsId(goods.getId());
            attributeService.add(attribute);
        }

        // 商品货品表litemall_product
        for (LitemallGoodsProduct product : products) {
            product.setGoodsId(goods.getId());
            productService.add(product);
        }
        return ResponseUtil.ok();
    }

    /**
     * 商品更新
     * @param goodsAllinone
     * @return
     */
    public Object goodsUpdate(GoodsAllinone goodsAllinone) {
        Object error = validate(goodsAllinone);
        if (error != null) {
            return error;
        }

        LitemallGoods goods = goodsAllinone.getGoods();
        LitemallGrouponRules grouponRules = goodsAllinone.getGrouponRules();
        LitemallGoodsAttribute[] attributes = goodsAllinone.getAttributes();
        LitemallGoodsSpecification[] specifications = goodsAllinone.getSpecifications();
        LitemallGoodsProduct[] products = goodsAllinone.getProducts();

        //将生成的分享图片地址写入数据库
        if (!StringUtils.hasText(goods.getShareUrl())){
            String url = qCodeService.createGoodShareImage(goods.getId().toString(), goods.getPicUrl(), goods.getName());
            goods.setShareUrl(url);
        }

        // 商品表里面有一个字段retailPrice记录当前商品的最低价
        BigDecimal retailPrice = new BigDecimal(Integer.MAX_VALUE);
        for (LitemallGoodsProduct product : products) {
            BigDecimal productPrice = product.getPrice();
            if(retailPrice.compareTo(productPrice) == 1){
                retailPrice = productPrice;
            }
        }

        // 商品基本信息
        goods.setRetailPrice(retailPrice);
        if (goodsService.updateVersionSelective(goods) == 0){
            throw new RuntimeException("系统繁忙");
        }

        if (goods.getIsGroupon() != null && goods.getIsGroupon()){
            //判断是否设置过团购
            LitemallGrouponRules rulesServiceByGid = rulesService.findByGid(goods.getId());
            if (rulesServiceByGid != null) grouponRules = rulesServiceByGid;
            //设置商品更新信息
            grouponRules.setGoodsId(goods.getId());
            grouponRules.setPicUrl(goods.getPicUrl());
            grouponRules.setGoodsName(goods.getName());
            grouponRules.setStatus(GrouponConstant.RULE_STATUS_ON);
            if (grouponRules.getId() != null){
                if (rulesService.updateVersionSelective(grouponRules) == 0) {
                    throw new RuntimeException("更新数据失败");
                }
            }else {
                //添加团购信息
                if (rulesService.createRules(grouponRules) == 0) {
                    throw new RuntimeException("更新数据失败");
                }
                LocalDateTime now = LocalDateTime.now();
                LocalDateTime expire = grouponRules.getExpireTime();
                long delay = ChronoUnit.MILLIS.between(now, expire);
                // 团购过期任务
                taskService.addTask(new GrouponRuleExpiredTask(grouponRules.getId(), delay));
            }
        } else {
            if (grouponRules != null && rulesService.countByGoodsId(goods.getId()) > 0){
                //删除团购超时任务
                taskService.removeTask(new GrouponRuleExpiredTask(grouponRules.getId()));
                //设置团购立马过期
                GrouponRuleExpiredTask expiredTask = new GrouponRuleExpiredTask(grouponRules.getId());
                expiredTask.run();
            }
        }

        LitemallRewardTask rewardTask = rewardTaskService.findByGid(goods.getId());
        if (rewardTask != null) {
            rewardTask.setPicUrl(goods.getPicUrl());
            rewardTask.setGoodsName(goods.getName());
            rewardTask.setGoodsPrice(goods.getRetailPrice());
            if (rewardTaskService.updateVersionSelective(rewardTask) == 0){
                throw new RuntimeException("赏金更新失效");
            }
        }

        // 商品规格
        for (LitemallGoodsSpecification specification : specifications) {
            // 目前只支持更新规格表的图片字段
            specification.setSpecification(null);
            specification.setValue(null);
            if (specificationService.updateVersionSelective(specification) == 0){
                throw new RuntimeException("系统繁忙");
            }
        }

        // 商品货品
        for (LitemallGoodsProduct product : products) {
            if (productService.updateVersionSelective(product) == 0){
                throw new RuntimeException("系统繁忙");
            }
        }

        // 商品参数
        for (LitemallGoodsAttribute attribute : attributes) {
            if (attributeService.updateVersionSelective(attribute) == 0){
                throw new RuntimeException("系统繁忙");
            }
        }

        // 这里需要注意的是购物车有些字段是拷贝商品的一些字段，因此需要及时更新
        // 目前这些字段是goods_sn, goods_name, price, pic_url
        for (LitemallGoodsProduct product : products) {
            cartService.updateProduct(product.getId(), goods.getGoodsSn(), goods.getName(), product.getPrice(), product.getUrl());
        }

        return ResponseUtil.ok();
    }

    /**
     * 商品删除
     * @param goods
     * @return
     */
    public Object goodsDelete(LitemallGoods goods) {
        Integer gid = goods.getId();
        LitemallGoods litemallGoods = goodsService.findById(gid);
        if (gid == null || litemallGoods == null) {
            return ResponseUtil.badArgument();
        }

        if (litemallGoods.getIsGroupon() != null && litemallGoods.getIsGroupon()){
            LitemallGrouponRules grouponRules = rulesService.findByGid(gid);
            //删除团购超时任务
            taskService.removeTask(new GrouponRuleExpiredTask(grouponRules.getId()));
            //设置团购立马过期
            GrouponRuleExpiredTask expiredTask = new GrouponRuleExpiredTask(grouponRules.getId());
            expiredTask.run();
        }

        //删除赏金
        LitemallRewardTask rewardTask = rewardTaskService.findByGid(gid);
        if (rewardTask != null) rewardTaskService.deleteByGid(gid);

        //删除商品信息
        goodsService.deleteById(gid);
        specificationService.deleteByGid(gid);
        attributeService.deleteByGid(gid);
        productService.deleteByGid(gid);
        return ResponseUtil.ok();
    }

}